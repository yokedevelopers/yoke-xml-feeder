<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */

/**
 * Handles plugin action hooks
 *
 * @since      1.0.0
 * @package    YXML
 * @subpackage YXML/includes
 */

class YXML_Actions {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->init_hooks();
	}

	public function init_hooks() {
        // add_action( 'yxml_init', array($this, 'actions_init'), 100);
		add_action( 'init', array($this, 'actions_init'), 10);
	}

    public function actions_init($request = array()) {

        $force_nonce = null;

    	if(defined('YXML_REQUEST_ACTION') && YXML_REQUEST_ACTION) return;

    	if(empty($request)):

	        if(!empty($_POST)):
	        	$request = $_POST;
	        elseif( !empty($_GET)):
	        	$action_can_get = (is_admin() && !defined('DOING_AJAX') ? true : false);
	        	$action_can_get = apply_filters('yxml/filter/action_can_get', $action_can_get, $_GET );
	        	if( $action_can_get ):
	        		$request = $_GET;
		        endif;
	        endif;
	    
	    endif;

        if( !isset($request['yxml_action']) ):
            if(!defined('YXML_REQUEST_ACTION') ) define('YXML_REQUEST_ACTION', false);
        	return;
        endif;

        // $response = new YXML_Response( YXML_REQUEST_ACTION );
    	// p(YXML_REQUEST_ACTION);

        $action_prefix = 'yxml/action';

        $action = $request['yxml_action'];

        if($force_nonce):
            
            $response = yxml_response();
            
            $nonce = $this->do_nonce($action, $request);
            
            if(!$nonce->success):
                $nonce->finalise();
            endif;

        endif;

        if(!defined('YXML_REQUEST_ACTION')):
            define('YXML_REQUEST_ACTION', $action);
        endif;
        
        global $yxml_request_args;
        $yxml_request_args = $request;

        $action_arr = explode('/', $action);

        $current_action = '';


        foreach($action_arr as $action_step_key => $action_step):
            
            $current_action .= '/' . $action_step;
            do_action($action_prefix . $current_action, $request);

        endforeach;


        // $response->finalise();
    }

    public function do_nonce($action, $request = array()) {
    	
		$response = yxml_response();

        if( !isset($request['_wpnonce_' . $action]) || !wp_verify_nonce( $request['_wpnonce_' . $action], $action ) ):
        	
        	$response->err('You do not have permission to do this.');
        	$response->finalise();

        	exit;
        endif;

        return $response;
    }
}
