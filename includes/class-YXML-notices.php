<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    YXML
 * @subpackage YXML/includes
 * @author     Your Name <email@example.com>
 */
class YXML_Notices {

		
	private $session_var;
	
	public $notices = array();

	public function __construct( ) {

		$this->session_var = YXML_SESSION_PREFIX . 'notices';
		$this->init_hooks();

	}

	private function init_hooks() {
		
		add_action( 'yxml_before_init', array($this, 'start_session') );
		add_action( 'yxml_before_init', array($this, 'init') );
		add_action( 'admin_notices', array($this, 'show_notices') );
	}

	public function init() {

	}

	public function start_session() {
		if (!session_id()) {
		    session_start();
		}
	}

	public function get_notices( $type = null ) {

		$return = $notices = (isset($_SESSION[$this->session_var]) ? $_SESSION[$this->session_var] : array());

		if($type && $notices && is_array($notices)):
			$return = array();
			foreach ($notices as $notice):
				if(isset($notice['type']) && $notice['type'] == $type) $return[] = $notice;
			endforeach;
		endif;

		return $return;

	}

	public function clear_notices() {
		$_SESSION[$this->session_var] = array();
	}

	public function add_notice($message, $dismissable = false, $type = 'success') {

		$notices = $this->get_notices();

		$notices[] = array(
			'message' 		=> $message,
			'dismissable' 	=> $dismissable,
			'type' 			=> $type
		);

		$_SESSION[$this->session_var] = $notices;
		
	}

	public function show_notices() {

		$notices = $this->get_notices();

		if(!empty($notices)):
			foreach ($notices as $key => $notice):

				$classes = array('notice');
				if($notice['dismissable']) $classes[] = 'is-dismissible';
				$classes[] = 'notice-' . $notice['type'];

				echo sprintf(
						'<div class="%s"><p>%s</p></div>',
						implode(' ', $classes),
						$notice['message']
					);

			endforeach;
		endif;

		$this->clear_notices();

	}

	public function msg($message, $dismissable = false) {
		$this->add_notice($message, $dismissable, 'success');
	}

	public function err($message, $dismissable = false) {
		$this->add_notice($message, $dismissable, 'error');
	}

	public function warn($message, $dismissable = false) {
		$this->add_notice($message, $dismissable, 'warning');
	}

	public function has_errors() {
		$errors = $this->get_notices( 'error' );
		return !empty($errors) ? true : false;
	}

}