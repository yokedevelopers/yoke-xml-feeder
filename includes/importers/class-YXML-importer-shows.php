<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class YXML_Import_Shows extends YXML_Importer {

	public static $id = 'shows';
	public static $endpoint = 'Performance.ashx';
	public static $label = 'Shows';
	
	public function init_hooks() {
		add_filter('yxml_endpoint_args', array($this, 'add_endpoint_args'), 10, 3);
	}

	public function add_endpoint_args( $args = array(), $endpoint, $method ) {

		if($endpoint == $this->get_endpoint()):
			$args = wp_parse_args( $args, array(
				'FromDate' => date('d/m/Y'),
				'ToDate' => date('d/m/Y', strtotime('+ 1 year')),
				'ProfileID' => 'INT',
				'Prefix' => ''
			));
		endif;

		return $args;
	}

	public function do_import( $result, $args = array() ) {
		$updated_rows = 0;
		if($result):
			foreach ($result->Show as $key => $show):

				if( YXML()->shows->insert( $show ) ) $updated_rows++;

			endforeach;
		endif;

		YXML()->notices->add_notice(sprintf(__('<strong>%s Importer</strong>: %s item(s) updated', 'yoke'), self::$label, $updated_rows));
		return $updated_rows;
	}
}
