<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *  Import - Abstract
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */

abstract class YXML_Importer {

	/**
	 * Unique ID for the import - must be set.
	 * @var string
	 */
	public static $id = '';

	/**
	 * API endpoint for the import - must be set.
	 * @var string
	 */
	public static $endpoint = '';

	/**
	 * Import label.
	 * @var string
	 */
	public static $label = '';

	/**
	 * Import description.
	 * @var string
	 */
	public static $description = '';

	/**
	 * @var array
	 */
	public $args = array();

	/**
	 * Required arguments for the get query
	 * @var array
	 */
	public static $required_args = array();

	/**
	 * Default arguments for the get query
	 * @var array
	 */
	public static $default_args = array(
		'results_per_page'		=> 40,
		'time_limit' 			=> 30,
		'max_pages'				=> 0
		);

	/**
	 * Whether to request results in pages
	 * @var boolean
	 */
	public static $paginate = false;

	protected $total_pages = 1;
	protected $total_rows = 1;
	protected $max_pages = 0;
	protected $current_page = 1;
	protected $completed_pages = 0;

	protected $data_complete = false;
	protected $import_complete = false;
	protected $saved_data = array();
	protected $imported_pages = 0;
	protected $imported_rows = 0;

	protected $errors = array();


	public function get_id() {
		return $this::$id;
	}

	public function get_endpoint() {
		return $this::$endpoint;
	}

	abstract protected function do_import( $results, $args = array() );

	/**
	 * Constructor.
	 */

	public function __construct( $args = array() ) {
		
		$this->init_hooks();
		$this->args = wp_parse_args( $args, $this->get_default_args() );
		$this->request_args = wp_parse_args( $args, $this->get_default_args() );

	}

	public function init_hooks() {}
	
	public function get_default_args() {
		return is_array($this::$default_args) ? $this::$default_args : array();
	}

	public function get_arg( $key, $default = null ) {
		return isset($this->args[$key]) ? $this->args[$key] : $default;
	}

	public function get_required_args() {
		return is_array($this::$required_args) ? $this::$required_args : array();
	}

	public function get_request() {

		$api_request = array();
		$request_valid = true;

		if( $this->get_paginate() ):
			
			$results_per_page = $this->get_results_per_page();
			$current_page = $this->get_current_page();

			if(!isset($api_request['pageRows']) ) $api_request['pageRows'] = $results_per_page;
			
			$page_start_row = $results_per_page * ($current_page - 1) + 1;

			$api_request['startRow'] = $page_start_row;
			$api_request['pageRows'] = $results_per_page;

		endif;


		if($this->get_arg('changes_only') && $last_updated = $this->get_arg('last_updated')):
			$api_request['updatedAfter'] = $last_updated;
		endif;


		foreach ($this->get_required_args() as $key => $arg):
			
			if(!isset($api_request[$arg])):
				$this->add_error( sprintf(__('<code>%s</code> is a requried argument', 'yoke'), $arg) );
				$request_valid = false;
			endif;

		endforeach;



		$request_valid = apply_filters( 'yxml_import_request_valid', $request_valid, $api_request, $this );

		if(!$request_valid):

			$this->add_error( sprintf(__('<strong>%s</strong> import could not be completed due to an invalid request', 'yoke'), $this->get_label()));
			do_action( 'yxml_invalid_import_request', $api_request, $this );
			return false;
		endif;

		return apply_filters( 'yxml_import_args_' . $this::$id, $api_request, $this );
	}


	public function get_data_complete() {
		return $this->data_complete;
	}

	public function set_data_complete( $complete = true ) {
		$this->data_complete = $complete;
	}

	public function get_import_complete() {
		return $this->import_complete;
	}

	public function set_import_complete( $complete = true ) {
		$this->import_complete = $complete;
	}

	public final function getting_data() {
		
		$errors = array();

		if($this->get_data_complete()):
			return false;
		endif;

		$api_request = $this->get_request();

		if($api_request === false) return false;

		$api_result = $this->get_data( $api_request );
		
		$this->set_api_result($api_result);
		
		if(!$api_result):
			
			$this->add_error( sprintf(__('Request returned no results', 'yoke'), $this->get_label()) );

		elseif(is_object($api_result) && method_exists($api_result, 'is_AP21_error') && $api_result->is_AP21_error() ):

			$this->add_error( sprintf(__('%s (%s)', 'yoke'), $api_result->get_error(), $api_result->is_AP21_error()) );

		endif;

// var_dump($api_result);
// die();

		if($this->has_errors()):
			$this->set_data_complete();
			return false;
		endif;

		if($this->get_total_pages() && $this->get_current_page() >= $this->get_total_pages()):
			$this->set_data_complete();
		endif;
		
		$this->set_completed_pages( $this->get_current_page() );
		$this->set_current_page( $this->get_current_page()+1 );


		return true;
	}

	public final function importing_data() {
		$saved_data = $this->get_saved_data();
		$n_pages = count($saved_data);

		$imported_pages 	 	= $this->get_imported_pages();
		$total_imported_rows	= $this->get_imported_rows();

		if($this->get_import_complete()):
			return false;
		elseif(!$n_pages):
			$this->add_error( __('No data found to import', 'yoke') );
			return false;
		elseif($imported_pages >= $n_pages):
			$this->set_import_complete();
			return false;
		elseif( !isset($saved_data[$imported_pages]) ):
			$this->add_error( __('No data found to import', 'yoke') );
			return false;			
		endif;

		$current_page_data = $saved_data[$imported_pages];

		$this->set_api_result($current_page_data);
		// var_dump($current_page_data);
		$total_rows = $this->get_total_rows();
		$total_pages = $this->get_total_pages();
		$results_per_page = $this->get_results_per_page();

		$imported_rows = $this->do_import( apply_filters('yxml_import_data', $current_page_data, $this ) );

		if($imported_rows === false):			
			$this->add_error( __('Unable to import rows', 'yoke') );
			return false;
		elseif(is_numeric($imported_rows)):
			$total_imported_rows += $imported_rows;
		endif;

		if($this->has_errors()):
			$this->set_import_complete();
			return false;
		endif;
		
		$imported_pages = 
			$total_imported_rows >= $total_rows ?
				$total_pages : ($total_imported_rows ? floor($total_imported_rows/$results_per_page) : 0);

		// $this->set_imported_pages( $imported_pages );
		$this->set_imported_pages( $imported_pages );
		// $this->set_imported_rows( $total_imported_rows );
		$this->set_imported_rows( $total_imported_rows );

		return true;

	}

	public function get_data( $args = array() ) {
		return YXML()->con->get( apply_filters('yxml_get_import_data_endpoint', $this->get_endpoint(), $this->get_id() ), $args );
	}

	public function get_saved_data() {
		return $this->saved_data;
	}
	
	public function set_saved_data( $data = array() ) {
		$this->saved_data = $data;
	}

	public function set_api_result( $result ) {
		
		$this->current_result = $result;
		
		if(!$result) return;

		if($this->get_paginate()):
	
			$n_results = (int) $result->attributes()->TotalRows ?: 0;
			$page_start_row = (int) $result->attributes()->PageStartRow ?: 1;

			$results_per_page = $this->get_results_per_page();

			// $current_page = $page_start_row > 1 ?  $results_per_page / ($page_start_row - 1) : 1;
			$total_rows = $n_results;
			$total_pages = $n_results && $results_per_page ? ceil($n_results/$results_per_page) : 1;

			$this->set_total_pages($total_pages);
			$this->set_total_rows($total_rows);
			// $this->set_current_page($current_page);
			// var_dump($current_page);
		endif;

	}

	public function get_api_result() {
		return $this->current_result;
	}

	public function get_label() {
		return (!empty($this::$label) ? $this::$label : $this::$id );
	}

	public function get_paginate() {
		return $this::$paginate;
	}

	public function get_results_per_page() {
		return $this->args['results_per_page'];
	}

	public function get_total_pages() {
		$total_pages = $this->total_pages ?: 1;
		return $this->max_pages && $this->max_pages < $total_pages ? $this->max_pages : $total_pages;
	}

	public function set_total_pages( $n ) {
		$this->total_pages = $this->get_paginate() ? $n : 1;
	}
	public function get_total_rows() {
		return $this->total_rows ?: 0;
	}

	public function set_total_rows( $n ) {
		$this->total_rows = $this->get_paginate() ? $n : 1;
	}

	public function get_current_page() {
		$current_page = $this->current_page ?: 1;
		return absint($current_page);
	}

	public function set_current_page( $n ) {
		$current_page = $this->get_paginate() ? $n : 1;
		$this->current_page = $current_page;
	}

	public function set_completed_pages( $n ) {
		$this->completed_pages = absint($n);
	}

	public function get_completed_pages() {
		return absint($this->completed_pages) ?: 0;
	}
	
	public function set_imported_pages( $n ) {
		$this->imported_pages = absint($n);
	}

	public function get_imported_pages() {
		return absint($this->imported_pages) ?: 0;
	}
	
	public function set_imported_rows( $n ) {
		$this->imported_rows = absint($n);
	}

	public function get_imported_rows() {
		return absint($this->imported_rows) ?: 0;
	}
	
	public function add_error( $err ) {
		$errors = $this->get_errors();
		$errors[] = $err;
		$this->errors = $errors;
	}

	public function get_errors() {
		return is_array( $this->errors ) ? $this->errors : array();
	}

	public function has_errors() {
		return count($this->get_errors()) ?: false;
	}

	public final function run() {


		// $error_msg = null;
		// $error_reason = null;



		var_dump($args);
		var_dump(__FUNCTION__);
		die();


		// $this->set_option('started', current_time( 'mysql' ) );


		$completed_pages = 0;

		$max_pages = 10;
		$current_page = 40; //No of pages to skip 	//$this->get_current_page();
		
		$total_pages = null; //By default until set by first result

		$complete = false;
		$success = false;
		$total_imported_rows = 0;

		// while (!$complete && (is_null($total_pages) || $current_page < $total_pages) && (!$max_pages || $completed_pages < $max_pages)):

			if($this->time_limit):
				set_time_limit( $this->time_limit );
			endif;

			$total_pages = 1;
			// $current_page = $this->get_current_page();
			
			if($this->get_paginate()):
				$page_start_row = $results_per_page * $current_page + 1;
				$args['startRow'] = $page_start_row;
			endif;

			$result = $this->get_data( $args );

			$this->set_api_result($result);

			if(!$result):				
				
				$error_reason = sprintf(__('Request returned no results', 'yoke'), $this->get_label());
				$complete = true;

			elseif(is_object($result) && method_exists($result, 'is_AP21_error') && $result->is_AP21_error() ):

				$error_reason = sprintf(__('%s (%s)', 'yoke'), $result->get_error(), $result->is_AP21_error());
				$complete = true;

			else:

				$imported_rows = $this->do_import( $result, $args );

				if($imported_rows === false):
					
					$complete = true;

				else:
					
					if(is_numeric($imported_rows)) $total_imported_rows += $imported_rows;

					$success = true;

					if($this->get_paginate()):
						$total_pages = $this->get_total_pages();
					else:
						$complete = true;
					endif;

				endif;

			endif;

			$current_page++;
			$completed_pages++;
			// $this->set_current_page($current_page);
			
			// var_dump('$current_page: ' 		. $current_page);
			// var_dump('$total_pages: ' 		. $total_pages);

		// endwhile;
		
		// $this->set_current_page(0);

		if($success):
			YXML()->notices->msg( sprintf(__('<strong>%s</strong> import successful. %s item(s) added or updated.', 'yoke'), $this->get_label(), $total_imported_rows));
		else:
			$error_msg = !empty($error_msg) ? $error_msg : sprintf(__('<strong>%s</strong> import failed%s', 'yoke'), $this->get_label(), (!empty($error_reason) ? ': ' . $error_reason : ''));
			YXML()->notices->err( $error_msg );
		endif;

		$this->set_option( 'completed', current_time( 'mysql' ) );
 		$this->set_option( 'success', $success );

 		if($success):
 			$last_started = $this->get_option('started');
			$this->set_option( 'success_completed', current_time( 'mysql' ) );
			$this->set_option( 'success_started', $last_started );
		endif;

		do_action( 'yxml_after_import_' . $this->get_id(), $this, $success, $result, $args);

	}
}
