<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *  Put Load - Abstract
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


abstract class YXML_API_PutLoad {

	public $raw_args = null;
	public $call = null;
	
	public $request_type = 'PUT';

	protected $output = null;

	public function __construct( $args = array() ) {

		$this->raw_args = $args;
		$xml_object_name = $this->get_xml_object_name();

		$this->output = new SimpleXMLElement('<' . $xml_object_name . '></' . $xml_object_name . '>');
	}

	public function get_xml_object_name() {
		return 'data';
	}

	public function before_submit(&$endpoint, &$xml_output) {}

	public function get_xml() {
		$xml_output = $this->output->asXML();
		return $xml_output;
	}

	public function get_request_type() {
		return $this->request_type;
	}

	public abstract function get_endpoint();

	public function clean_key_str( $key ) {

		$key_arr = explode(':', $key);
		$key = $key_arr[0];

		$key = strip_tags($key);
		$key = preg_replace("/[^A-Za-z0-9_ ]/", '', $key);

		return $key;
	}


	public function filter_item_name( $item_name, $key ) {
		return $item_name;
	}

	public function process_field_callbacks( $field, $field_key = null ) {

		$value = null;

		if(is_array($field)):

			$cb = isset($field['cb']) ? $field['cb'] : null;
			
			if(!$cb):

				$value = array();
				foreach ($field as $subkey => $subfield) $value[$subkey] = $this->process_field_callbacks( $subfield, $subkey );

			else:

				$args = array();

				if(is_array($cb)):
					if(isset($cb['args'])):
						$args = $cb['args'];
						unset($cb['args']);
					endif;			
				endif;

				if($cb && is_callable($cb)):
					$value = call_user_func($cb, $args);
				endif;
			endif;

		else:
			$value = $field;
		endif;

		return $value;
	}

	public function work_node( &$node, $value ) {
		return $node;
	}

	public function set_output_field( $key, $value ) {
		
		$key = !is_array($key) ? array($key) : $key;

		foreach ($key as &$key_str) $key_str = $this->clean_key_str($key_str);
		
		if( is_array($value) || is_object($value) ):

			$value = (array) $value;

			foreach ($value as $subkey => $subvalue):
				
				$current_node = $this->output;

				$new_key = $key;
				$new_key[] = $subkey;

				if(!$this->is_asso($value)):

					foreach ($new_key as $i => $key_el):

						if($i+1 == count($new_key)):
							$current_node->addChild( apply_filters('yxml_xml_output_item_name_' . $this->get_xml_object_name(), 'item', end($key) ), $subvalue);
						else:
							if(!isset( $current_node->$key_el)) $current_node->addChild("$key_el");
							$current_node = $current_node->$key_el;
							if($i == count($new_key)):
								$current_node->addAttribute('type', 'array');
							endif;
						endif;

					endforeach;

				else:
					$this->set_output_field( $new_key, $subvalue );
				endif;			


			endforeach;

		else:

			$current_node = $this->output;

			foreach ($key as $i => $key_el):

				if($i+1 == count($key)):

					if(is_null($current_node)):
						continue;
					endif;

					$current_node->addChild("$key_el", $value);

				else:
					
					if(!isset( $current_node->$key_el)) $current_node->addChild("$key_el");
					$current_node = $current_node->$key_el;

				endif;

			endforeach;

		endif;

	}

	public function is_asso($a) {
		foreach(array_keys($a) as $key) if (!is_int($key)) return true;
		return false;
	}


	public function submit( $args = array(), $return_obj = false ) {

		add_filter('yxml_use_local_data', '__return_false');

		$xml_output = $this->get_xml();
		$endpoint = $this->get_endpoint();

		$this->before_submit($endpoint, $xml_output);
// p($xml_output);
// p($this);
// die();
		if(!$endpoint || empty($endpoint)) return false;

		$result = false;
		$call = false;

		$request_type = $this->get_request_type();
		$request_type_func = strtolower($request_type);

		if(method_exists(YXML()->con, $request_type_func)):

			$call = YXML()->con->$request_type_func( $endpoint, $xml_output, $args );
			$this->set_call_object( $call );

			if(method_exists($call, 'get_result')):
				$result = $call->get_result();
			
				$this->on_submit_response();
			endif;
		endif;

		return $return_obj ? $call : $result;
	}

	private function set_call_object( $call ) {
		$this->call = $call;
	}

	public function get_call_object( ) {
		return $this->call;
	}

	public function get_call_result( ) {
		$call = $this->get_call_object();
		return $call ? $call->get_result() : null; 
	}

	public function on_submit_response( ) {}
}
