<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *  Put Load - Abstract
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Cron {

	public static $cron_task_prefix = 'yxml/action/cron_';

	public function __construct() {

		$this->init_hooks();
	}

	public function init_hooks() {


		add_filter( 'cron_schedules', array($this, 'add_cron_intervals'), 10, 1 );
		// add_action( 'yxml/action/cron_all', array($this, 'log_cron_job'), 1, 2 );

// var_dump(get_option( YXML_OPTION_PREFIX . 'cron_scheduled'));
		if( !get_option( YXML_OPTION_PREFIX . 'cron_scheduled') ):
			add_action( 'yxml_init', array($this, 'schedule_yxml_cron_tasks'), 110 );
			update_option( YXML_OPTION_PREFIX . 'cron_scheduled', true);

		endif;

		add_action( 'yxml_deactivation', array($this, 'clear_yxml_cron_tasks'), 10 );

		// add_action('yxml_init', array($this, 'test_cron'), 300);

		/*if(isset($_GET['view_cron'])):
			$this->print_tasks();
			die();
		endif;*/

		$this->add_cron_actions();

		// p($this->yxml_cron_tasks());
		// die();
	}
	
	public function print_tasks() {

		echo '<pre>';
		// print_r(_get_cron_array());
		echo date('Y/m/d H:i:s', time()) . "\tcurrent time\n-----------------------------------------------\n";

		foreach(_get_cron_array() as $time => $tasks):
			foreach ($tasks as $hook => $details):

				foreach ($details as $detail_key => $task):
					$args = $task['args'];
					$next_scheduled_timestamp = wp_next_scheduled( $hook, $args );
					echo date('Y/m/d H:i:s', $next_scheduled_timestamp) . "\t" . $hook . "\n";
				endforeach;

			endforeach;
		endforeach;

		echo '</pre>';
	}

	public function test_cron() {
		// do_action('yxml/action/cron_monthly', array( 'schedule' => 'monthly' ) );
		/*
		
		YXML()->log->print_log();

		die();
		// wp_clear_scheduled_hook( 'yxml/action/import_trigger', array('import_id' => 54426) ); //This worked

		// $action = 'yxml/action/import_trigger';
		// wp_unschedule_event(1506995406, $action );
		// $wp_next_scheduled = wp_next_scheduled( $action );
		// $wp_get_schedule = wp_get_schedule( $action );
		// var_dump($wp_next_scheduled);
		// var_dump($wp_get_schedule);
		if(defined('DISABLE_WP_CRON')) var_dump(DISABLE_WP_CRON);

		$this->clear_yxml_cron_tasks();
		$this->schedule_yxml_cron_tasks();
		// do_action( 'yxml_activation' );
		// do_action( 'yxml_deactivation' );
				echo date('H:i:s d/m/Y', time()) . ' <br>';
		foreach(_get_cron_array() as $time => $tasks):
			// echo date('H:i:s d/m/Y');
			foreach ($tasks as $hook => $details):

				foreach ($details as $detail_key => $task):
					$args = $task['args'];
					$next_scheduled_timestamp = wp_next_scheduled( $hook, $args );
					// var_dump(vars)
					echo date('H:i:s d-m-Y', $next_scheduled_timestamp) . ' ' . $hook . ' <br>';
				endforeach;

			endforeach;
		endforeach;
		p(_get_cron_array());
		die();
		*/
	}

	public function add_cron_actions() {
		foreach ($this->yxml_cron_tasks() as $schedule => $hook):
			add_action( $hook, function( $args ) use($hook) { 
				do_action('yxml/action/cron_all', $args, $hook);
			});
		endforeach;
	}

	public function log_cron_job( $args, $original_hook = 'undefined' ) {

			YXML()->log->log('TRIGGER CRON: ' . $original_hook);
		// $tasks = $this->yxml_cron_tasks();

		// if( isset($tasks[$original_hook]) ):
		// endif;

	}

	public function get_schedules() {
		
		$schedules = wp_get_schedules();

		$allowed_schedules = array_merge(
			array(
				'hourly',
				'twicedaily',
				'daily',
				'weekly',
				'monthly'
			),
			array_keys($this->add_cron_intervals())
		);

		$schedules = array_filter($schedules, function($v, $k) use($allowed_schedules) {
			return in_array($k, $allowed_schedules, true);
		}, ARRAY_FILTER_USE_BOTH);
 		
 		uasort($schedules, function ($a, $b) {
		    return $a['interval'] - $b['interval'];
		});

		return $schedules;
	}

	public function yxml_cron_tasks() {

		$schedules = $this->get_schedules();
		$tasks = array();

		foreach($schedules as $schedule_key => $detail):
			$tasks[ $schedule_key ] = self::$cron_task_prefix . $schedule_key;
		endforeach;

		return $tasks;
		
	}


	public function schedule_yxml_cron_tasks() {

		foreach ($this->yxml_cron_tasks() as $schedule_key => $hook):
			$args = array( 'schedule' => $schedule_key );
			if(!wp_next_scheduled( $hook, $args )):
				$this->schedule_task( time(), $schedule_key, $hook, $args );
			endif;
		endforeach;
		
	}

	public function clear_yxml_cron_tasks() {


		foreach ($this->yxml_cron_tasks() as $schedule_key => $hook):
			$args = array( 'schedule' => $schedule_key );
			if(wp_next_scheduled( $hook, $args )):
				wp_clear_scheduled_hook( $hook, array( 'schedule' => $schedule_key ) );
			endif;
		endforeach;
		
		delete_option( YXML_OPTION_PREFIX . 'cron_scheduled');

	}

	public function add_cron_intervals( $intervals = array() ) {

	    $intervals['minute'] = array(
	        'interval' => MINUTE_IN_SECONDS,
	        'display'  => esc_html__( 'Every Minute' ),
	    );

	    $intervals['five_minutes'] = array(
	        'interval' => 5*MINUTE_IN_SECONDS,
	        'display'  => esc_html__( 'Every 5 Minutes' ),
	    );
	 
	    return $intervals;
	}

	public static function get_schedule_interval_from_hook( $hook ) {
		return str_replace(self::$cron_task_prefix, '', $hook);
	}
	public function get_interval_options() {
 		
 		$options = array();
		
		$schedules = $this->get_schedules();	

		foreach($schedules as $schedule_key => $detail):
			$options[$schedule_key] = $detail['display'];
		endforeach;

		return $options; 	
	}

	public function schedule_task($timestamp, $recurrence, $action, $args = array()) {
		// var_dump($timestamp, $recurrence, $action, $args);
// die();
		$timestamp += MINUTE_IN_SECONDS * 10;

		echo __FUNCTION__ . ' ' . $action . ' ' .  date('H:i:s d/m/Y', $timestamp) . ' <br>';

		$scheduled = wp_schedule_event($timestamp, $recurrence, $action, $args);
		// var_dump($scheduled);
		// die();
		 // wp_schedule_event(time(), 'hourly', 'my_schedule_hook', $args);
	}

	public function clear_task( $action, $args = array() ) {
		wp_clear_scheduled_hook( $action, $args );
	}
}
