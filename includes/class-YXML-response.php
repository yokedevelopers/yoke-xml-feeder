<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Response {

    function __construct($global_name = null) {

        global $yxml_responses;

        $this->success = true;
        $this->messages = array();
        $this->errors = array();
        $this->fragments = array();
        $this->redirect = null;
        $this->print = null;
        $this->html = null;

        if(!is_null($global_name)):
            
            if(!isset($yxml_responses['global_name'])):
                $yxml_responses['global_name'] = $this;
            endif;

            return $yxml_responses['global_name'];

        endif;

        return $this;
    }

    public function __toString() {

        $o_print = $this->print;
        $this->prnt('', false);

        $this->prnt('success:'      .   $this->success);
        $this->prnt('messages:'     .   $this->messages);
        $this->prnt('errors:'       .   $this->errors);
        $this->prnt('fragments:'    .   $this->fragments);
        $this->prnt('redirect:'     .   $this->redirect);
        $this->prnt('print:'        .   $o_print);

        $output = $this->print;

        $this->prnt($o_print, false);
        
        return $output;
    }

    public function err($msg = null, $data = null) {
        
        $item = array();
        
        if(!empty($msg)):
            $item['message'] = $msg; 
            $item['data'] = $data; 
            $this->errors[] = $item;
            $this->success = false;
        endif;
        
    }
    
    public function msg($msg = null, $data = null) {
     
        $item = array();
        
        if(!empty($msg)):
            $item['message'] = $msg; 
            $item['data'] = $data; 
            $this->messages[] = $item;
            // $this->success = false;
        endif;
    }

    public function frag($selector = null, $html = null) {
     
        if(!empty($selector) && !empty($html)):
            $item[$selector]; 
            $this->fragments[$selector] = $html;
        endif;
    }

    public function add_redirect($url = null, $msg = null) {

        $redirect = array();
        
        if($url):
            $redirect['url'] = $url;

            if($msg):
                $redirect['message'] = $msg;
            endif;
        endif;

        $this->redirect = (!empty($redirect) ? $redirect : null);
    }

    public function merge( YXML_Response $that = null, $props = array()) {

        if( !is_a($that, get_class($this) ) ) return;
        
        $props = (!is_array($props) ? array($props) : $props);

        $mergables = array(
            'messages',
            'errors',
            'fragments',
        );

        foreach ($mergables as $mergable):
            if(!empty($props) && !in_array($mergable, $props)) continue;

            $new_val = array_merge(
                    (is_array($this->$mergable) ? $this->$mergable : array()),
                    (is_array($that->$mergable) ? $that->$mergable : array())
                );

            $this->$mergable = $new_val;
        endforeach;

        $this->success = (!empty($this->errors) ? false : $this->success); 
        $this->redirect = (empty($props) && !empty($that->redirect) ? $that->redirect : $this->redirect);

        return;
    }

    public function prnt($var = null, $add = true) {
        
        ob_start();
            echo '<pre style="margin-top: 50px; margin-left: 250px;">';
                print_r($var);
            echo '</pre>';
            echo '<hr>';
            $output = ob_get_contents();
        ob_end_clean();

        $this->print = ($add ? $this->print . $output : $output);
        return $this->print;

    }

    public function finalise($and_die = true) {

        do_action( 'yxml/action/before_response_finalise', $this, $and_die);

        $and_die = apply_filters('yxml/filter/response_finalise_die', $and_die );

        if(defined('DOING_AJAX') && DOING_AJAX && $and_die):
            die(json_encode($this));
        else:
        
            if(!empty($this->redirect)):
                // die($this->redirect['url']);
                wp_redirect( $this->redirect['url'] );
                exit;
            endif;

            if(!empty($this->html)) echo $this->html;

            return $this;

        endif;
    }
}

function yxml_response( $global_name = null ) {
    return new YXML_Response( $global_name );    
}

function p($var) {

    $resp = yxml_response();
    $resp->prnt($var);
    echo $resp->print;
    
}

?>