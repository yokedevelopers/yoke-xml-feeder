<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class YXML_Persons {

	/**
	 * @since    1.0.0
	 */

	protected $current_person = null;

	public function __construct() {

		$this->init_hooks();

	}

	private function init_hooks() {
		// add_action('yxml_init', array($this, 'init'), 10);
		add_action('yxml_init', function(){
			// $person = YXML()->con->get('Persons/62239');
			// $order = YXML()->con->get('Persons/62239/Orders/17263');
			// $person = YXML()->con->get('Persons/62238');
			// $order = YXML()->con->get('Persons/62238/Orders/17262');
			// p($person);
			// p($order);
			// die();
		}, 10);
	}


	public function init() {

		$this->set_current_person();

	}

	public function set_current_person( $person = null ) {
		
		$user_id = get_current_user_id();
		$person = new YXML_Person( $user_id );
		
		$this->current_person = $person;

		return $person;

	}

	public function get_current_person( $set = false ) {

		if(isset($this->current_person) && is_a($this->current_person, 'YXML_Person')):
			return $this->current_person;
		elseif($set):
			return $this->set_current_person();
		endif;
		 
		return false;
	}

	public function create_person() {
		$person_package = new YXML_Person_Package();
	}

	public function get_current_person_api_id() {

		$person = $this->get_current_person( true );

		if( $person ):
			$person->set_from_api();
			return $person->get_api_id();
		endif;

		return null;

	}

	public function get_person_api_id_by_order( $wc_order ) {
		
		$api_person_id = null;
		
		$person = new YXML_Person( 
			array( 
				'user_email' 		=> $wc_order->get_billing_email(),
				'user_firstname'	=> $wc_order->get_billing_first_name(), 
				'user_lastname'		=> $wc_order->get_billing_last_name(),
				'billing_details'	=> array(
					'AddressLine1' 					=> $wc_order->get_billing_address_1(),
					'AddressLine2' 					=> $wc_order->get_billing_address_2(),
					'City' 							=> $wc_order->get_billing_city(),
					'State' 						=> $wc_order->get_billing_state(),
					'Postcode' 						=> $wc_order->get_billing_postcode(),
					'Country' 						=> $wc_order->get_billing_country(),
				)
			)
		);
		if( $person ):
			$person->set_from_api( true );
			$api_person_id = $person->get_api_id();
		endif;

		return $api_person_id;
	}
	public function get_person_api_id_by_email( $email ) {

		$person = new YXML_Person( array( 'user_email' => $email ) );

		if( $person ):
			$person->set_from_api();
			return $person->get_api_id();
		endif;

		return null;
	}
}
