<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Attributes extends YXML_Data {

	protected $extra_data = array(

		);

	public function __construct() {

		$this->init_hooks();

		parent::__construct();
		
	}

	private function init_hooks() {

	 	add_filter('get_terms_orderby', array($this, 'size_terms_orderby'), 20, 3);
	 	add_filter('woocommerce_attribute_orderby', array($this, 'size_terms_get_orderby'), 20, 2);

		foreach ($this->get_attribute_taxonomies() as $attr_name => $attr_args):
			$taxonomy =  wc_attribute_taxonomy_name( $attr_name );
			add_filter('woocommerce_taxonomy_args_' . $taxonomy, function( $args ) use ($attr_args) {
				return wp_parse_args( $attr_args, $args );
			}, 20, 1);
		endforeach;
	}



	public function get_attribute_taxonomies( $keys_only = false ) {
		$attrs = array(
			'colour' => array(
					'hierarchical' => true
				),
			'size' => array(
					'hierarchical' => true
				)
			);

		return $keys_only ? array_keys($attrs) : $attrs;
	}

	public function get_term_from_api_item( $type, $item, $args = array(), $create = true ) {

		$attrs = $this->get_attribute_taxonomies( true );

		if(!in_array($type, $attrs)) return false; 

		$taxonomy = wc_attribute_taxonomy_name( $type );

		$term = $this->{"get_{$type}_term_from_api_item"}( $item, $args, $create );

		$term = $this->filter_term_result( $term, $taxonomy );

		return $term;
	}

	public function filter_term_result( $term, $taxonomy = null ) {

		if(is_wp_error( $term )):
			
			$term_id = $term->get_error_data('term_exists');
			$term_id = absint($term_id);

			if($term_id):
				$term =	get_term( $term_id, $taxonomy, 'ARRAY_A' );
			else:
				return false;
			endif;

		endif;

		if(!$term || empty($term) || is_wp_error( $term )):
			return false;
		else:
		
			$term = is_object($term) ? (array) $term : $term;
			
			if( (!isset($term['slug']) || !isset($term['name'])) && isset($term['term_id'])):

				$term_id = absint($term['term_id']);
				$term =	get_term( $term_id, $taxonomy, 'ARRAY_A' );

				if(is_wp_error($term)) return false;

			endif;
		endif;

		$term = (array) $term;

		return $term;
	}

	private function get_colour_term_from_api_item( $item, $args = array(), $create = true ) {
		
		$taxonomy = wc_attribute_taxonomy_name( 'colour' );
		$term = false;

		$cid = (string) $item->Id; //Specific to this product

		$name = $this->clean_term_value( $item->Name ); //Maybe specific to this product
		$code = $this->clean_term_value( $item->Code ); //Maybe specific to this product
		
		$global_code = $this->clean_term_value( $item->TypeCode );
		$global_name = $this->clean_term_value( $item->TypeName );

		if(empty($global_code) && empty($name)) return $term;

		$slug = sprintf('%s-%s-%s', $global_code, $code, $name);
		$slug = sanitize_title( $slug );
		$parent_slug = sprintf('type-code-%s', $global_code);
		$parent_slug = sanitize_title( $parent_slug );

		$parent_name = $global_code; 

		if(empty($slug) || empty($parent_slug)) return $term;

		$name = !empty($name) ? $name : 'None';

		$term = $this->get_term_child( $taxonomy, $name, $slug, $parent_name, $parent_slug, $create );

		return $term;
	}

	private function get_size_term_from_api_item( $item, $args = array(), $create = true ) {

		$taxonomy = wc_attribute_taxonomy_name( 'size' );
		$term = false;

		$name = $this->clean_term_value( $item->SizeCode );
		$sequence = absint( $item->GlobalSequence >= $item->Sequence ? $item->GlobalSequence : $item->Sequence );
		$range = $this->clean_term_value( isset($args['size_range']) ? $args['size_range'] : null );

		if(empty($range) && empty($name)) return $term;

		$slug = sprintf('srange-%s-%s-s-%s', $range, str_pad($sequence, 3, '0', STR_PAD_LEFT), $name);
		$slug = sanitize_title( $slug );
		$parent_slug = sprintf('srange-%s', $range);
		$parent_slug = sanitize_title( $parent_slug );
		
		$parent_name = $range; 

		if(empty($slug) || empty($parent_slug)) return $term;

		$term = $this->get_term_child( $taxonomy, $name, $slug, $parent_name, $parent_slug, $create );

		return $term;
	}

	public function get_one_size_term() {

		$taxonomy = wc_attribute_taxonomy_name( 'size' );

		$name = __('One Size', 'yoke');
		$slug = 'global-one-size-one-size';
		$parent_name = __('One Size', 'yoke');
		$parent_slug = 'global-one-size';

		$term = $this->get_term_child( $taxonomy, $name, $slug, $parent_name, $parent_slug );
		return $this->filter_term_result( $term, $taxonomy );

	}

	private function get_term_child( $taxonomy, $name, $slug, $parent_name, $parent_slug, $create = true ) {

		$term = false;

		$parent_term = term_exists( $parent_slug, $taxonomy, 0);

		if(!$parent_term):
			
			if(!$create) return false;
			
			$parent_term_data = array(
				'name' => $parent_name,
				'slug'	=> $parent_slug,
				'parent' => 0
				);

			$parent_term = wp_insert_term( $parent_name, $taxonomy, $parent_term_data );

		endif;

		if($parent_term && !is_wp_error( $parent_term )):

			$parent_term = (array) $parent_term;
			$parent_id = absint($parent_term['term_id']);

			$term = term_exists( $slug, $taxonomy, $parent_id);

			if(!$term):

				if(!$create) return false;

				$term_data = array(
					'name' => $name,
					'slug' => $slug,
					'parent' => $parent_id
					);

				$term = wp_insert_term( $name, $taxonomy, $term_data );

			endif;

		endif;

		return $term;
	}

	public function clean_term_value( $input = '', $default = null ) {

		$value = (string) $input;
		$value = trim( $value, '-');
		$value = trim( $value );

		return strlen($value) ? $value : $default;
	}

	public function size_terms_orderby( $orderby, $query_vars, $taxonomy = null ) {

		$size_taxonomy = wc_attribute_taxonomy_name('size');

		if(empty($taxonomy)):
			$taxonomy = $query_vars['taxonomy'];
		endif;
		
		if($taxonomy == $size_taxonomy || (is_array($taxonomy) && in_array($size_taxonomy, $taxonomy))):
			$orderby = 't.slug';
		endif;

		return $orderby;
	}

	public function size_terms_get_orderby( $orderby, $name ) {

		if($name == 'size' || $name == wc_attribute_taxonomy_name('size')) $orderby = 'slug';
		
		return $orderby;
	}
}
