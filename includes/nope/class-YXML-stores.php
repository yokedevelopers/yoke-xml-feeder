<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Stores extends YXML_Post_Factory {

	protected $factory_id	 			= 'store';

	protected $db_table_name		 	= 'stores';

	protected $single_class	 		 	= 'YXML_Store';

	protected $post_type	 		 	= 'yxml_store';

	protected $import_object_type	 	= 'store';

}
