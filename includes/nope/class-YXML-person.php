<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Person {

	protected $api_id = null;
	protected $api_id_meta_key = 'api_id';
	protected $api_result = null;

	protected $wp_user;

	protected $data = array();

	public function __construct( $args = null ) {
		
		if($args && is_numeric( $args )):

			$this->wp_user = new WP_User( $args );

		elseif(is_array($args)):

			$this->data = $args;

		endif;

		// parent::__construct($id);

	}

	public function get( $key ) {
		if($this->is_guest()):
			return isset( $this->data[$key] ) ? $this->data[$key] : null;
		else:
			return $this->wp_user->get( $key );
		endif;
	}

	public function get_id() {
		return $this->get('ID');
	}

	public function is_guest() {
		return empty($this->wp_user) ? true : false;
	}

	public function get_email() {
		$email = $this->get('user_email');
		return $email ?: false;
	}

	public function get_firstname( $default = '' ) {
		$firstname = $this->get('user_firstname');
		return $firstname ? $firstname : $default;
	}

	public function get_surname( $default = '' ) {
		$lastname = $this->get('user_lastname');
		return $lastname ? $lastname : '';
	}

	public function get_billing_details() {

		return wp_parse_args( $this->get('billing_details'), array(
			'ContactName'		=> '',
			'AddressLine1'		=> '',
			'AddressLine2'		=> '',
			'City'				=> '',
			'State'				=> '',
			'Postcode'			=> '',
			'Country'			=> ''
		));
	}


	public function get_api_id_meta_key( $include_prefix = true ) {

		$key = $this->api_id_meta_key;
		if($include_prefix && $key) $key =  YXML_DB_PREFIX . $key;
		return $key;

	}

	public function set_api_id( $val = 0 ) {

		$this->api_id = $val;
		
		if($this->is_guest()) return;

		$meta_key = $this->get_api_id_meta_key();
		update_user_meta( $this->get_id(), $meta_key, $val );

	}

	public function delete_api_id() {

		if($this->is_guest()) return;

		$meta_key = $this->get_api_id_meta_key();
		delete_user_meta( $this->get_id(), $meta_key );

	}

	public function get_api_id() {
		
		return $this->api_id;// = $val;

		/*if(!$this->is_guest()):
			$key = $this->get_api_id_meta_key();
			return $this->has_prop($key) ? $this->get($key) : null;
		else:
			return $this->get_guest_api_id();
		endif;*/
	}

	public function get_guest_api_id() {
		return 'GUEST001';
	}

	public function set_from_api( $create = true ) {
		
		if($this->api_result) return true;

		$api_result = $this->get_from_api( $create );

		if(!$api_result) return false;

		$this->api_result = $api_result;

		if(!$this->get_api_id()):
			$this->set_api_id( isset($api_result->Person->Id) ? (string) $api_result->Person->Id : 0  );
		endif;

		return true;

	}

	public function get_from_api( $create = true ) {

		$api_id = $this->get_api_id();
		$email = $this->get_email();

		if(!$email && !$api_id):
			return false;
		endif;
		
		if($api_id):
			$result = YXML()->con->get('Persons/' . $api_id);
		else:
			$result = YXML()->con->get('Persons', array( 'Email' => $email ));
		endif;

		if($result && $result->has_data()):
			return $result;
		elseif( $create ):
			return $this->create_in_api( );
		else:
			return false;
		endif;

	}

	/**
	* @return (string) Person ID 
	**/

	public function create_in_api() {

		$package = new YXML_Person_Package();

		$package->set_output_field('Contacts', 		array('Email' => $this->get_email()));
		$package->set_output_field('Firstname', 	$this->get_firstname('First', 'BLANK'));
		$package->set_output_field('Surname', 		$this->get_surname('Last', 'BLANK'));
		$package->set_output_field('Addresses',		array('Billing' => $this->get_billing_details()));

		$package->submit();

		$call = $package->get_call_object();

		if(is_object($call) && method_exists($call, 'get_return_location')):
		
			$location = $call->get_return_location();
			$api_person_id = self::get_person_api_id_from_location( $location );

		endif;

		$this->set_api_id( $api_person_id );

		return $api_person_id;
	}

	public static function get_person_api_id_from_location( $location = '' ) {
	
		$api_person_id = null;
		if(preg_match('/Persons\/(?<api_person_id>[0-9]+)/', $location, $matches)):
			$api_person_id = $matches['api_person_id'];
		endif;

		return $api_person_id;
		
	}
}

class YXML_Person_Package extends YXML_API_PutLoad {

	public $request_type = 'POST';
	/**
	 * @since    1.0.0
	 */

	public function __construct( $args = array() ) {

		$this->init_hooks();
		parent::__construct( $args );
	}

	public function get_xml_object_name() {
		return 'Person';
	}

	public function get_endpoint() {
		return 'Persons';
	}

	public function init_hooks() {

	}
}
