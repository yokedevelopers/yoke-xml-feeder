<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
* 
*/
class YXML_References {

	private $db_tables = array();

	public function __construct( ) {

		$this->db_tables = array(
			'reference_types' 	=> 'reference_types',
			'references' 		=> 'references'
		);

		$this->init_hooks();

	}

	private function init_hooks() { 

		add_filter( 'yxml_database_config', array($this, 'add_db_config'), 5);
		add_action(	'yxml_init', array($this, 'update_reference_types_options'));

	}

	public function get_table_name( $type ) {
		return (isset($this->db_tables[$type]) ? $this->db_tables[$type] : null);
	}

	private function init() { }

	public function update_reference_types_options( ) {

		if(isset($_POST['action'])
			&& $_POST['action'] == 'yxml_update_reference_types_options'
			&& isset($_POST['reference_types'])
			):
			foreach ($_POST['reference_types'] as $id => $ref_type_settings):
				
				$ref_type_settings['id'] = $id;
				$this->add_reference_type($ref_type_settings);

	 			YXML()->notices->msg( sprintf(__( 'Your options for the <strong>\'%s\'</strong> reference type  have been updated.', 'yoke' ), (isset($ref_type_settings['name']) ? $ref_type_settings['name'] : __('Unknown', 'yoke'))));
			endforeach;
			
			flush_rewrite_rules();
			
			$redirect = (isset($_POST['_redirect']) ? $_POST['_redirect'] : null );

			if($redirect):
				wp_redirect( $redirect );
				exit;
			endif;

			$referer = wp_get_referer();
			if($referer):
				wp_redirect( $referer );
				exit;
			endif;

		endif;
	}

	public function add_db_config( $config ) {

		$config[$this->get_table_name('references')] = array(

			'id'					=> array(
											'type' 			=> 'mediumint(9)',
											'flags'			=> array('NOT NULL'),
											'primary_key'	=> 1
										),
			'code' 					=> array(
											'type' 			=> 'varchar(255)',
											'flags'			=> array('NOT NULL'),
										),
			'name' 					=> array(
											'type' 			=> 'text',
											'default' 		=> '',
											'flags'			=> array('NOT NULL'),
										),
			'reference_type_id'		=> array(
											'import_alias'	=> 'referencetypeid',
											'type'			=> 'mediumint(9)',
											'flags'			=> array('NOT NULL')
										),
			'term_id'				=> array(
											'type' 			=> 'mediumint(9)',
											'default' 		=> null,
											'flags'			=> array('NOT NULL'),
										)
			);

		$config[$this->get_table_name('reference_types')] = array(

			'id'					=> array(
											'type' 			=> 'mediumint(9)',
											'flags'			=> array('NOT NULL'),
											'primary_key'	=> 1
										),
			'code' 					=> array(
											'type' 			=> 'varchar(255)',
											'flags'			=> array('NOT NULL'),
										),
			'name' 					=> array(
											'type' 			=> 'text',
											'default' 		=> '',
											'flags'			=> array('NOT NULL'),
										),
			'sequence'				=> array(
											'type' 			=> 'int(4)',
											'default' 		=> null,
											'flags'			=> array('NOT NULL'),
										),
			'product_reference'		=> array(
											'type' 			=> 'int(1)',
											'default' 		=> 0,
											'flags'			=> array('NOT NULL'),
										),
			'is_tax' 				=> array(
											'type' 			=> 'int(1)',
											'default' 		=> 0,
											'flags'			=> array('NOT NULL'),
										),
			'taxonomy'				=> array(
											'type' 			=> 'varchar(32)',
											'default' 		=> '',
											'flags'			=> array('NOT NULL'),
										),
			'post_types'			=> array(
											'type' 				=> 'longtext',
											'default' 			=> '',
											'flags'				=> array('NOT NULL'),
											'serialize'			=> 1
										),
			'tax_options'			=> array(
											'type' 				=> 'longtext',
											'default' 			=> '',
											'flags'				=> array('NOT NULL'),
											'serialize'			=> 1, 
											// 'filter_in'			=> array($this, ''),
											'filter_out'		=> array( $this, 'filter_tax_options' )
										)
		);

		return $config;
	}

	public function tax_options_fields() {

		$fields = array(

			'public'					=> 	array(
													'type' 		=> 'bool',
													'default'	=> 1,
													'sanitize'	=> null
												),
			'publicly_queryable'		=> 	array(
													'type' 		=> 'bool',
													'default'	=> 1,
													'sanitize'	=> null
												),
			'show_ui'					=> 	array(
													'type' 		=> 'bool',
													'default'	=> 1,
													'sanitize'	=> null
												),
			'show_in_menu'				=> 	array(
													'type' 		=> 'bool',
													'default'	=> 1,
													'sanitize'	=> null
												),
			'show_in_nav_menus'			=> 	array(
													'type' 		=> 'bool',
													'default'	=> null,
													'sanitize'	=> null
												),
			'show_in_rest'				=> 	array(
													'type' 		=> 'bool',
													'default'	=> null,
													'sanitize'	=> null
												),
			'rest_base'					=> 	array(
													'type' 		=> 'text',
													'default'	=> null,
													'sanitize'	=> null
												),
			'rest_controller_class'		=> 	array(
													'type' 		=> 'text',
													'default'	=> null,
													'sanitize'	=> null
												),
			'show_admin_column'			=> 	array(
													'type' 		=> 'bool',
													'default'	=> 0,
													'sanitize'	=> null
												),
			'add_admin_filter'			=> 	array(
													'type' 		=> 'bool',
													'default'	=> 0,
													'sanitize'	=> null
												),
			'description'				=> 	array(
													'type' 		=> 'text',
													'default'	=> null,
													'sanitize'	=> null
												),
			'rewrite'			 		=> 	array(
													'type' 		=> 'text',
													'default'	=> null,
													'sanitize'	=> 'sanitize_title'
												),
			'with_front'		 		=> 	array(
													'type' 		=> 'bool',
													'default'	=> 0,
													'sanitize'	=> null
												),
		);

		return $fields;
	}

	public function add_reference_type( $item ) {
		
		$item = (array) $item;
		
		$updated = YXML()->db->insert( $this->get_table_name('reference_types'), $item );

		return $updated !== false ? true : false;
	}

	public function add_product_reference_type( $item ) {
		
		$item->product_reference = true;
		$item->id = (int) $item->ReferenceTypeId;

		return $this->add_reference_type( $item );
	}

	public function get_reference_types( $args = array() ) {

		$order_by = (isset($args['order_by']) ? $args['order_by'] : 'is_tax' );
		$order = (isset($args['order']) ? $args['order'] : 'DESC' );

		$where = array();

		$where = (isset($args['where']) ? $args['where'] : array() );

		$return = (isset($args['return']) ? $args['return'] : array());

		if(isset($args['id'])):
			$id_arr = array('RELATION' => 'OR');
			$ids = (is_array($args['id']) ? $args['id'] : array($args['id']));
			foreach ($ids as $key => $id):
				$id_arr[] = array('id' => $id);
			endforeach;
			$where[] = $id_arr;
		endif;

		if(isset($args['is_tax'])):
			$where['is_tax'] = $args['is_tax'];
		endif;

		$n_results = (isset($args['n_results']) ? $args['n_results'] : -1);

		$db_results = YXML()->db->get_results( $this->get_table_name('reference_types'), $return, $where, $n_results, $order_by, $order );
		$reference_types = $db_results;

		return $reference_types;
	}

	public function get_reference_type_id_by_reference_id( $ref_id ) {

		$return = array('reference_type_id');
		$where = array('id' => $ref_id );

		$n_results = 1;

		$result = YXML()->db->get_results( $this->get_table_name('references'), $return, $where, $n_results);

		if(!empty($result)):
			return isset($result[0]->reference_type_id) ? $result[0]->reference_type_id : null;
		endif;

		return null;
	}

	public function get_reference_sequence_by_reference_id( $ref_id ) {

		$ref_type_id = $this->get_reference_type_id_by_reference_id( $ref_id );

		$return = array('sequence');
		$where = array('id' => $ref_type_id );

		$n_results = 1;

		$result = YXML()->db->get_results( $this->get_table_name('reference_types'), $return, $where, $n_results);

		if(!empty($result)):
			return isset($result[0]->sequence) ? (int) $result[0]->sequence : null;
		endif;

		return null;
	}

	public function get_reference_by_reference_id( $ref_id, $fields = null ) {

		$return = $fields ?: null;

		$where = array('id' => $ref_id );

		$n_results = 1;

		$result = YXML()->db->get_results( $this->get_table_name('references'), $return, $where, $n_results);

		return !empty($result) && isset($result[0]) ? $result[0] : null;
	}

	public function filter_tax_options( $tax_options = array() ) {

		if(!is_array($tax_options)) $tax_options = array();

		$tax_options_fields = $this->tax_options_fields();

		foreach ($tax_options_fields as $key => $option_settings):
			if(!isset( $tax_options[$key] )):
				$default = (isset($option_settings['default']) ? $option_settings['default'] : null );
				$tax_options[$key] = $default;
			endif;
		endforeach;

		return $tax_options;
	}

	public function add_reference( $item ) {
		
		$item = (array) $item;

		$term =	YXML()->tax->insert_term($item);

		$term_id = (isset($term['term_id']) ? $term['term_id'] : (is_numeric($term) ? $term : null));

		$item['term_id'] = $term_id;

		$updated = YXML()->db->insert( $this->get_table_name('references'), $item );

		return $updated !== false ? true : false;
	}

	public function get_product_references(  ) {

		$reference_types = $this->get_reference_types( array( 
			'where' => array(
					'product_reference' => 1
				),
			'return' => array(
					'id', 'name'
				)
			) );

		$output = array();

		if(!empty($reference_types)):
			foreach ($reference_types as $key => $reference_type):
				$reference_type_id = $reference_type->id;
				$references = YXML()->db->get_results( $this->get_table_name('references'), array('id', 'name'), array('reference_type_id' => $reference_type_id), -1, 'name', 'ASC' );
				$result_row = (array) $reference_type;
				if(!empty($references)):
					foreach ($references as &$reference) $reference = (array) $reference;
					$result_row['references'] = $references;
					// var_dump($result_row);
					$output[] = $result_row;
				endif;

			endforeach;
		endif;

		return $output;

	}

	
	public function get_terms_by_reference_ids( $reference_ids = array() ) {

		// global $wpdb;
		$return = array();

		if(empty($reference_ids)) return array();
		// var_dump($reference_ids);
		
		$references = YXML()->db->get_results( $this->get_table_name('references'), array('id', 'term_id', 'reference_type_id'), array('RELATION' => 'IN','id' => $reference_ids), -1, 'name', 'ASC' );


		if(!empty($references)):
			foreach ($references as $reference):
				$return[ $reference->id ] = array('term_id' => (int) $reference->term_id, 'taxonomy' => YXML()->tax->get_taxonomy_by_reference_type_id( $reference->reference_type_id ));
			endforeach;
		endif;

		return $return;
		/*
		var_dump($references);
		die();

		$db_results = $wpdb->get_results("SELECT wp_terms.term_id, wp_terms.name AS term_name, wp_yxml_references.id AS reference_id, wp_yxml_references.name AS reference_name
		FROM wp_terms
		INNER JOIN wp_yxml_references ON wp_terms.term_id = wp_yxml_references.term_id AND wp_terms.name != wp_yxml_references.name;");

		var_dump($db_results);
		die();
		*/
	}

	// public function get_term_reference_tree( $ref_type_id, $ref_type_id = 0 ) {
	public function get_term_reference_tree( $taxonomies = array() ) {

		if(empty($taxonomies)) return false;

		$tax_reference = YXML()->tax->tax_reference();
				var_dump( $tax_reference);

		$args = array();
		
		$valid = true;

		foreach ($taxonomies as $tax):
			if (false !== $ref_type_id = array_search($tax, $tax_reference)):
				$index = 'level' . (count($args) + 1);
				$args[$index] = $ref_type_id;
			else:
				$valid = false;
			endif;

			if(count($args) >= 5) break;

		endforeach;

		if(!$valid || empty($args)) return false;

		$endpoint = 'ReferenceTree';
		
		$reference_tree = YXML()->con->get('ReferenceTree', $args);
		$reference_tree_ids = array();
		
		if($reference_tree && $reference_tree->has_data() ):
			$reference_tree_ids = $this->get_reference_tree_node( $reference_tree );
		endif;

		return $this->convert_reference_tree_ids_to_terms( $taxonomies, $reference_tree_ids );
	}

	public function get_reference_tree_node( $tree ) {

		$node_ids = array();

		if(!$tree) return $node_ids;

		foreach ($tree->children() as $key => $reference):
			
			$ref_id = (string) $reference->ReferenceId;
			$children = array();
			
			if( property_exists($reference, 'Children') ):
				$children = $this->get_reference_tree_node( $reference->Children );
			endif;

			$node_ids[ $ref_id ] = $children;
		
		endforeach;

		return $node_ids;

	}

	public function convert_reference_tree_ids_to_terms( $taxonomies, $ref_id_tree, $depth = 0 ) {

		$terms = array();

		if(is_array( $ref_id_tree ) && !empty($ref_id_tree)):
			foreach ($ref_id_tree as $ref_id => $child_ref_ids):
				$term_id = YXML()->tax->get_term_id_by_reference_id( $ref_id );
				if(!$term_id || empty($term_id)):
					// echo 'no: ' . $ref_id . '<br>';
				else:
					$terms[$term_id] = $this->convert_reference_tree_ids_to_terms( $taxonomies, $child_ref_ids, $depth+1 );
				endif;
			endforeach;
		endif;

		$taxonomy = isset($taxonomies[$depth]) ? $taxonomies[$depth] : null;

		return $taxonomy ? array($taxonomy => $terms) : $terms;
	}
	

}

