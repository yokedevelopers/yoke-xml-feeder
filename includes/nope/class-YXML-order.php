<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class YXML_Order_Package extends YXML_API_PutLoad {

	public $request_type = 'POST';

	private $wc_order = null;
	public $person_id = null;

	protected $defaults = array();

	public function get_xml_object_name() {
		return 'Order';
	}

	public function before_submit(&$endpoint, &$xml_output) {
		// p($this->output);
		// var_dump($xml_output);
		// die();
	}
		
	public function get_endpoint() {

		$person_id = $this->get_person_id();

		if(!$person_id) return false;

		$endpoint = sprintf('Persons/%s/Orders/', $person_id);

		return $endpoint;
	}

	public function get_order_xml_node() {
		return $this->output;
	}

	public function set_wc_order( $order ) {

		if(!is_a($order, 'WC_Order')):
			if(is_numeric($order)):
				$order = wc_get_order( $order );
			endif;
		endif;

		if(!is_a($order, 'WC_Order')) return false;

		$this->wc_order = $order;

		return true;
	}

	public function get_wc_order() {
		
		$wc_order = $this->wc_order;
		return is_a($wc_order, 'WC_Order') ? $wc_order : false;

	}

	public function create_from_wc_order( $order ) {

		if(!$this->set_wc_order( $order )) return false;

		$config = $this->get_config();

		foreach ($config as $key => $field):
		
			$value = $this->process_field_callbacks( $field, $key );
			$this->set_output_field(array($key), $value);

		endforeach;

		// $this->add_order_timestamp();
		// $this->add_order_number();
		$this->add_order_person();
		$this->add_order_addresses();
		$this->add_order_items();
		$this->add_order_shipping();
		$this->add_order_discounts();
		$this->add_order_payment();

		// p($this->get_wc_order());

		// $result = $this->output->asXML(YXML_ABSPATH . 'includes/xml_things/file.xml');
		// echo '<pre>';
		// print_r($this->output);
		// echo '</pre>';
		$submission = $this->submit( array(), true );
		
		// echo '<pre>';
		// print_r($submission);
		// echo '</pre>';
		// die();
		return $submission;

	}

	public function get_order_tax_correction() {

		
		$total_items_cost = $this->get_total_items_cost();
		$total_shipping_cost = $this->get_total_shipping_cost();
		$total = (float) $this->get_wc_order()->get_total();
		
		$calc_total = $total_items_cost + $total_shipping_cost;

		$dec_places = max( strlen(substr(strrchr($total, "."), 1)) , strlen(substr(strrchr($calc_total, "."), 1)));

		$difference = number_format($calc_total - $total, $dec_places, '.', '');

		if(abs($difference) < 0.01):
			return $difference;
		else:
			return 0;
		endif;

	}

	public function get_total_items_cost() {

		$total_items_cost = 0;

		foreach ($this->output->OrderDetails->children() as $key => $OrderDetail):
			$total_items_cost += (float) $OrderDetail->Value;
		endforeach;

		return $total_items_cost;
	}

	public function get_total_shipping_cost() {

		return isset($this->output->SelectedFreightOption->Value) ? (float) $this->output->SelectedFreightOption->Value : 0;

		return $total_items_cost;
	}

	private function add_order_timestamp() {

		$timestamp = date('Y-m-d\TH:i:s', current_time( 'timestamp' ));
		$this->set_output_field('OrderDateTime', $timestamp );

	}
	
	private function add_order_number() {

		if(!$this->get_wc_order()) return;

		$wc_order_id = $this->get_wc_order()->get_id();

		$order_number_prefix = 'WC';
		$order_number_format = '0000000000000';

		$yxml_order_number = $order_number_prefix . str_pad($wc_order_id, strlen($order_number_format), $order_number_format, STR_PAD_LEFT);

		$this->set_output_field('OrderNumber', $yxml_order_number);

	}

	public function set_person_id( $id ) {
		$this->person_id = $id;
	}

	public function get_person_id() {
		return $this->person_id ? $this->person_id : false;
	}
	
	public function get_order_person() {
		
	}

	private function add_order_person() {

		if(!$this->get_wc_order()) return;

		$person_id = YXML()->persons->get_person_api_id_by_order( $this->get_wc_order() );
		// $person_id = YXML()->persons->get_person_api_id_by_email( $email );

		$this->set_person_id( $person_id );

		$this->set_output_field('PersonId', $person_id);

	}

	private function add_order_addresses() {

		if(!$this->get_wc_order()) return;

		$wc_order = $this->get_wc_order();

		$address_data = array(
				'Billing' 						=> array(
					'AddressLine1' 					=> $wc_order->get_billing_address_1(),
					'AddressLine2' 					=> $wc_order->get_billing_address_2(),
					'City' 							=> $wc_order->get_billing_city(),
					'State' 						=> $wc_order->get_billing_state(),
					'Postcode' 						=> $wc_order->get_billing_postcode(),
					'Country' 						=> $wc_order->get_billing_country(),
												),
				'Delivery' 						=> array( /*  		  		 Delivery is a sub-element of Addresses */
					'ContactName' 					=> $wc_order->get_formatted_shipping_full_name(),
					'AddressLine1' 					=> $wc_order->get_shipping_address_1(),
					'AddressLine2' 					=> $wc_order->get_shipping_address_2(),
					'City' 							=> $wc_order->get_shipping_city(),
					'State' 						=> $wc_order->get_shipping_state(),
					'Postcode' 						=> $wc_order->get_shipping_postcode(),
					'Country' 						=> $wc_order->get_shipping_country(),
												)
			);

		$this->set_output_field('Addresses', $address_data);
		$this->set_output_field('DeliveryInstructions', $wc_order->get_customer_note());

		$contact_data = array( /*  		  		 Contacts consists of Email and Phone numbers. */
				'Email' 						=> $wc_order->get_billing_email(),
				'Phones' 						=> array(
					'Home' 							=>  $wc_order->get_billing_phone()
												)
			);

		$this->set_output_field('Contacts', $contact_data);

	}

	private function add_order_shipping() {

		if(!$this->get_wc_order()) return;

		$order_shipping_data = YXML()->shipping->get_yxml_order_shipping( $this->get_wc_order() );
		
		if(!empty($order_shipping_data)):
			foreach ($order_shipping_data as $key => $value):
				$this->set_output_field( "$key", $value );
			endforeach;
		endif;
	}

	private function add_order_discounts() {
		
		$discount_total = $this->get_wc_order()->get_total_discount( false /*$ex_tax*/);
		$this->set_output_field('TotalDiscount', $discount_total);

	}

	private function add_order_payment() {

		$order_total = $this->get_wc_order()->get_total();
		$payment_total = $order_total;
			
		
		$message = sprintf("Order #%s\n %s \n synced from Wordpress by %s.",
			$this->get_wc_order()->get_id(),
			home_url(),
			YXML_PLUGIN_TITLE
			);

		$tax_correction = $this->get_order_tax_correction();
		
		if($tax_correction != 0):
			$payment_total += $tax_correction;
			$message .= sprintf("\n\n Payment reflects a rounding correction of $%s", $tax_correction);
		endif;

		$payment_data = array(
				array(
					'Origin' 						=> 'CreditCard',
					'Settlement' 					=> $this->wc_order->get_transaction_id(),
					'Amount' 						=> $payment_total,
					'Message'						=> $message

				)
			);
		
		$order_payment_data = apply_filters( 'yxml_order_payment_data', $payment_data, $this->get_wc_order() );

		$order = $this->get_order_xml_node();

		$order_payments = $order->addChild('Payments');
		
		foreach ($order_payment_data as $key => $payment_detail_data):
			$order_payment_details = $order_payments->addChild('PaymentDetail');
			foreach ($payment_detail_data as $payment_detail_data_key => $payment_detail_data_value):
				$order_payment_details->addChild("$payment_detail_data_key", $payment_detail_data_value);
			endforeach;
		endforeach;

	}

	private function add_order_items() {

		$order = $this->get_order_xml_node();

		$order_items = $this->wc_order->get_items();
		$order_details = $order->addChild('OrderDetails');
		
		foreach ($order_items as $item_id => $order_item):
			
			$order_details_item = $order_details->addChild('OrderDetail');

			// $variation_id = $this->wc_order->get_item_meta($item_id, '_variation_id', true);
			$variation_id = wc_get_order_item_meta($item_id, '_variation_id', true);

			$yxml_product = yxml_product($variation_id);
			if(!$yxml_product) continue;
			// $wc_product = wc_product($variation_id);
			// p($wc_product);
			// p($order_item);
			
			$subtotal = $order_item->get_subtotal();
			$subtotal_tax = $order_item->get_subtotal_tax();
			$total = $order_item->get_total();
			$total_tax = $order_item->get_total_tax();

			$quantity = $order_item->get_quantity();

			$line_subtotal = $subtotal + $subtotal_tax;

			$line_total = $total + $total_tax; //number_format( $total + $total_tax, 2, '.', '' );

			$discount = $line_subtotal - $line_total;

			$item_price = $line_total / $quantity;

			$order_item_data = array(
					'SkuId'		=> $yxml_product->get_sku_id(),
					'Quantity'	=>  $quantity,
					'Price'		=>  $item_price,
					'Value'		=>  $line_total,
					// 'Discount'	=>  $discount
				);

			foreach ($order_item_data as $order_item_data_key => $value):
				$order_details_item->addChild("$order_item_data_key", $value);
			endforeach;
		endforeach;
		// die();
	}

	private function get_config() {
		return array();

	}

	private function get_defaults() {}

}
