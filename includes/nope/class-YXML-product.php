<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Product extends YXML_Post {

	protected $object_type	 			= 'product';

	protected $import_object_type	 	= 'Product';
	
	protected $post_type	 		 	= 'product';

	protected $unique_meta_key 		 	= 'product_id';

	protected $unique_import_item_key 	= 'Id';

	protected $default_post_status	 	= 'publish';

	protected $variations 				= array();

	public function get_import_field_map() {
		return array(
			'Name' 				=> array('post' => 'post_title'),
			'Code' 				=> array('meta' => 'product_code'),
		);
	}

	public function before_save() {

		$variable_term = get_term_by( 'slug', 'variable', 'product_type', 'ARRAY_A');
		$this->set_post_term( 'product_type', $variable_term, false );

		$code 			= (string)	 $this->get_api_item_value(	'Code', '' );
		
		$this->set_post_meta('_sku', $code);

		$this->add_reference_terms();
		$this->prepare_variations();

	}

	public function after_save() {

		$post_id = $this->get_id();


		if($post_id):

			
			if(function_exists('wc_delete_product_transients')):
				wc_delete_product_transients( $post_id );
			endif;

			$this->flush_variations();

			foreach($this->get_variations() as $variation_n => $variation):
				
				$variation->set_post_data('post_status', 'publish');
				$variation->set_parent_id( $post_id );
				$variation->save();

			endforeach;


			$wc_product = wc_get_product($post_id);

			if($wc_product && $wc_product->is_type( 'variable' )):

				$price = $wc_product->get_variation_price('min');
				update_post_meta( $post_id, '_price', $price );

			endif;

			update_post_meta( $post_id, '_yxml_updated', date('YmdHi') );

		endif;
	}

	public function flush_variations() {
			
		$post_id = $this->get_id();

		if(!$post_id) return;

		$wc_product = wc_get_product($post_id);

		if($wc_product && $wc_product->is_type( 'variable' )):		

			$saved_variation_ids = $wc_product->get_children();

			$current_variation_ids = array();
			$current_variations = $this->get_variations();

			foreach ($current_variations as $current_variation) $current_variation_ids[] = $current_variation->get_id();

			$inactive_variations = array_filter($saved_variation_ids, function($v) use ($current_variation_ids) {
				return !in_array($v, $current_variation_ids);
			});
			
			foreach ($inactive_variations as $inactive_variation):
				wp_update_post(array(
					'ID' => $inactive_variation,
					'post_status' => 'draft'
				));	
			endforeach;

		endif;


	}

	public function add_reference_terms( ) {

		$tax_reference = YXML()->tax->tax_reference();

		foreach ($tax_reference as $ref_type_id => $taxonomy):
			$this->set_post_term( $taxonomy, 0, false );
		endforeach;

		$references_obj = $this->get_api_item_value('References');
		$references = $references_obj->Reference;

		$tax_input = array();

		if(method_exists($references, 'count') && $references->count()):

			foreach ($references as $key => $reference):

				$term_pair = YXML()->tax->get_term_pair($reference->ReferenceTypeId, $reference->Id);
				
				if(!$term_pair) continue;

				$this->set_post_term( $term_pair['taxonomy'], $term_pair['term_id'], true );

			endforeach;

		endif;

	}

	public function set_default_attribute($attribute, $term) {
		
		$default_attributes = $this->get_post_meta('_default_attributes');
		$term_slug = isset($term['slug']) ? $term['slug'] : null;
		$taxonomy = wc_attribute_taxonomy_name( $attribute );
		
		if(!$term_slug || !$taxonomy) return;

		if(empty($default_attributes)) $default_attributes = array();

		$default_attributes[$taxonomy] = $term_slug;
		$this->set_post_meta( '_default_attributes', $default_attributes );

	}

	public function prepare_variations() {
		
		$item = $this->get_api_item();

		$clrs = $item->Clrs->Clr;

		$variations = array();

		$size_range = (property_exists($item, 'SizeRange') ? trim((string) $item->SizeRange) : null);

		$size_args = array(
			'size_range' => $size_range
		);

		$prices_arr = array();

		if(method_exists($clrs, 'count') && $clrs->count()):

			foreach ($clrs as $key => $item_clr):
				$colour_term = YXML()->attributes->get_term_from_api_item( 'colour', $item_clr );
						
				$sku_arr = array();
				$skus = $item_clr->SKUs->SKU;

				foreach ($skus as $key => $sku):
					set_time_limit(500);

			        $variation = new YXML_Product_Variation( $sku );
			        
					if($colour_term):
			        	$variation->add_colour( $colour_term );
			      	endif;

					$size_term = YXML()->attributes->get_term_from_api_item( 'size', $sku, $size_args );

			      	if($size_term):
			        	$variation->add_size( $size_term );
			        elseif(count($skus) == 1):

						$size_term = YXML()->attributes->get_one_size_term();

			        	if($size_term):
			        		$variation->add_size( $size_term );
			        		$this->set_default_attribute('size', $size_term);
			        	endif;
			        endif;

					$post_id = $this->get_id();
					
					if($post_id):
						// $variation->set_parent_id( $post_id );
					endif;

					$variations[] = $variation;

				endforeach;

			endforeach;
		endif;

		$this->set_variations( $variations );
	}

	public function set_variations( $variations ) {
		
		$this->set_prop( 'variations', $variations );
		
		$product_attributes_data = array();
		$post_terms = array();

		if($variations):			

			foreach($variations as $key => $variation):

				foreach($variation->get_attribute_terms() as $type => $term):
				
					$taxonomy = wc_attribute_taxonomy_name($type);

					if(!isset($post_terms[$taxonomy])) $post_terms[$taxonomy] = array();
					$post_terms[$taxonomy][] = $term;
				
					if(isset($product_attributes_data[$taxonomy])) continue;

					$product_attributes_data[$taxonomy] = array(
						'name'			=> $taxonomy,
			            'value'        => '',
			            'is_visible'   => '1',
			            'is_variation' => '1',
			            'is_taxonomy'  => '1'
						);

				endforeach;

			endforeach;

		endif;

		foreach ($post_terms as $taxonomy => $terms):
			$this->set_post_term( $taxonomy, false );
			foreach ($terms as $key => $term):
				$this->set_post_term( $taxonomy, $term, true );
			endforeach;
		endforeach;

		$this->set_post_meta('_product_attributes', $product_attributes_data);

	}

	public function get_variations() {
		$value = $this->get_prop( 'variations' );
		return $value ?: array();
	}

	public function get_api_id() {
		return $this->get_unique_meta_value();
	}

	public function get_product_notes( $code = false ) {

		$notes = YXML()->con->get('ProductNotes/' . $this->get_api_id() );

		if( $notes && $notes->has_data() && !empty($notes->children()) ):

			if(!$code):
				return $notes;
			else:

				foreach ($notes->children() as $key => $note):
					 if($note->Code == $code):
					 	return $note;
					 	break;
					 endif;
				endforeach;

			endif;
		endif;

		return false;

		/*

			$clr_results = array();

			foreach ($api_result->Clrs->children() as $clr):
				$clr = (array) $clr;
				$clr_id =	(string) $clr['Id'];

				if(empty($clr_id)) continue;

				$clr_results[$clr_id] =  YXML()->con->get('ProductColourNotes/' . $clr_id );
			endforeach;

			$api_result_simple =  YXML()->con->get('ProductsSimple/' . $api_id );
			$api_result_notes = YXML()->con->get('ProductNotes/' . $api_id );

		// p($api_result);
		p($clr_results);
		p($api_result_simple);
		p($api_result_notes);
		die();*/
	}

}

function yxml_product( $id = null ) {

	$post_type = get_post_type($id);
	switch ($post_type):
		case 'product':
			return new YXML_Product( $id );			
			break;
		case 'product_variation':
			return new YXML_Product_Variation( $id );			
			break;
	endswitch;
	
	return false;
}
