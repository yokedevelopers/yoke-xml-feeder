<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class YXML_Cart {

	/**
	 * @since    1.0.0
	 */

	/**
	 * @var object stores data to send to API
	 */	
	protected $package = null;
	protected $response = null;
	protected $stores = null;

	protected $freestock = null;
	protected $empty_cart = false;

	public function __construct() {

		$this->init_package();
		$this->init_hooks();

	}

	public function init_hooks() {
			
		add_action('woocommerce_cart_loaded_from_session', array($this, 'set_response_from_session'), 10);
		// add_action('woocommerce_before_calculate_totals', array($this, 'before_calculate_totals'), 20, 1);
		// add_action('woocommerce_after_calculate_totals', function(){ die('woocommerce_after_calculate_totals'); }, 5, 1);
		// add_action('woocommerce_after_calculate_totals', array($this, 'build_package_from_wc_cart'), 20, 1);
		// add_action('woocommerce_after_calculate_totals', array($this, 'get_freestock'), 30, 1);
		// add_action('woocommerce_after_calculate_totals', array($this, 'get_stores'), 30, 1);

		// add_action('woocommerce_cart_updated', function(){ die('woocommerce_cart_updated'); }, 5, 1);
		add_action('woocommerce_cart_updated', array($this, 'build_package_from_wc_cart'), 20);
		add_action('woocommerce_cart_updated', array($this, 'sumbit_package'), 30);
		// add_action('woocommerce_cart_emptied', array($this, ''), 20, 1);

		// define the woocommerce_available_shipping_methods callback 
	}

	public function init_package() {
		$this->package = new YXML_Cart_Package();
	}

	public function get_package() {
		if(is_null($this->package)) $this->init_package();
		return $this->package;
	}

	public function set_response( $response = null ) {
		$this->response = $response;
		// p($response);
	}
	
	public function get_response( $response = null ) {
		return $this->response;
	}

	public function set_response_session( $response = null ) {

		$formatted = method_exists($response, 'has_data') && $response->has_data() ?  serialize($response->asXML()) : array();
		
		WC()->session->set( 'yxml_cart', $formatted );
	}

	public function set_response_from_session() {
		$session_response = $this->get_api_cart_session();

		$this->set_response( $session_response );
	}


	public function get_api_cart_session() {
		$raw = WC()->session->get( 'yxml_cart', null );
		$xml = maybe_unserialize( $raw );
		return YXML_API_Call::parse_xml($xml);
	}

	public function before_calculate_totals( $wc_cart ) {
	}

	public function build_package_from_wc_cart( $wc_cart = null ) {

		if(empty($wc_cart)) $wc_cart = WC()->cart;
		
		if(!$wc_cart->get_cart_contents_count()):
			$this->empty_cart = true;
			return;
		endif;

		foreach ($wc_cart->get_cart() as $key => $cart_item):

			$qty 			= $cart_item['quantity'];
			$variation_id 	= $cart_item['variation_id'];

			$yxml_product_variation 	= yxml_product($variation_id);
			$cart_item_data = array(
					'SkuId'		=> $yxml_product_variation->get_sku_id(),
					'Quantity'	=> $qty,
				);
			// var_dump($cart_item_data);
			$this->get_package()->add_product( $cart_item_data );
			// var_dump($this->get_package());

		endforeach;
// die();
		$this->get_package()->set_output_field('PricesIncludeTax', 'false');
// var_dump($this->get_package());
		
		// $this->get_package()->set_freight();
	}

	public function get_freestock( $wc_cart = null ) {
		
		if( !is_null($this->freestock) ) return $this->freestock;

		if(is_null($wc_cart)) $wc_cart = WC()->cart;

		$results = array();

		foreach ($wc_cart->get_cart() as $key => $cart_item):

			$product_id 	= $cart_item['product_id'];
			$variation_id 	= $cart_item['variation_id'];

			if(isset($results[$variation_id])) continue;
 
			$yxml_product = yxml_product($product_id);
			$yxml_product_variation = yxml_product($variation_id);
				
			$sku = $yxml_product_variation->get_sku_id();
			$style_id = $yxml_product->get_api_id();

			$freestock_results = YXML()->con->get('Freestock/sku/' . $sku );

			$results[$key] = $freestock_results && $freestock_results->has_data() ? $freestock_results : false;

		endforeach;

		$this->set_freestock( $results );

		if(!is_ajax()):
			// p($results);
		endif;

		return $results;
	}

	public function get_stores() {

		if(!is_null($this->stores)) return $this->stores;

		$freestock = $this->get_freestock();

		if(!$freestock || empty($freestock)) return false;

		$stores = array();
		$stores_by_item = array();

		foreach ($freestock as $cart_item_key => $stores):
		
			$item_stores = array();

			if( !method_exists($stores, 'children') ) continue;

			foreach ($stores->children() as $store):

				$store_id = (string) $store['StoreId'];
				$store_name = (string) $store['Name'];

				$item_stores[ $store_id ] = $store_name;

			endforeach;

			$stores_by_item[] = $item_stores;
		
		endforeach;

		if( count($stores_by_item) > 1 ):
			$stores = array_intersect_assoc(...$stores_by_item);
		elseif(!empty($stores_by_item)):
			$stores = $stores_by_item[0];
		endif;

		$stores = !empty($stores) ? $stores : false;
			
		if(is_array($stores)):
			$stores = array_filter($stores, function($v, $k){
				return !empty($k);
			}, ARRAY_FILTER_USE_BOTH);
		endif;

		$this->stores = $stores;

		return $stores;
	}

	public function has_freight_option( $freight_id ) {
		
		return $this->get_freight_option( $freight_id ) ? true : false;

	}

	public function get_freight_option( $freight_id ) {
		
		$response = $this->get_response();
		if($response && isset($response->FreightOptions)):
			foreach ($response->FreightOptions->children() as $freight_type):
				if($freight_type->Id == $freight_id) return $freight_type;
			endforeach;
		endif;

		return false;
	}


	public function set_freestock( $results ) {
		$this->freestock = $results;
	}


	public function sumbit_package() {

		// $cart = WC()->cart;
		if($this->empty_cart):
			$response = false;
		else:
			// var_dump($cart);
			// $this->build_package_from_wc_cart( $cart );
			$response = $this->get_package()->submit();
			// p($response);
		endif;
		
		$this->set_response( $response );
		$this->set_response_session( $response );
		
		if(!is_ajax()):
			// p($this->get_response());
		endif;
		return $response;
	}
}
