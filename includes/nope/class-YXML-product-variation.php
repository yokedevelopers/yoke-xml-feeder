<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class YXML_Product_Variation extends YXML_Post {

	protected $object_type	 			= 'product_variation';

	protected $import_object_type	 	= 'SKU';
	
	protected $post_type	 		 	= 'product_variation';

	protected $unique_meta_key 		 	= 'product_sku_id';

	protected $unique_import_item_key 	= 'Id';

	protected $default_post_status 		= 'publish';

	protected $attribute_terms = array();

	public function get_import_field_map() {
		return array();
	}
	
	public function set_data_from_api() {
		
		$barcode 			= (string)	 $this->get_api_item_value(	'Barcode', '' );
		$price_original	 	= (float) 	 $this->get_api_item_value(	'OriginalPrice' );
		$price_retail		= (float) 	 $this->get_api_item_value(	'RetailPrice' );
		$price			 	= (float) 	 $this->get_api_item_value(	'Price' );
		$stock_amt			= (int) 	 $this->get_api_item_value(	'FreeStock', 0 );
		$next_available		= (int) 	 $this->get_api_item_value(	'NextAvailable', '' );
		
		$this->set_post_meta('_sku',		 			$barcode);
		$this->set_post_meta('_regular_price', 			$price_original);
		$this->set_post_meta('_price', 					$price);
		$this->set_post_meta('_sale_price', 			$price < $price_original ? $price : null);
		$this->set_post_meta('_manage_stock',			'yes');
		$this->set_post_meta('_backorders',				'no');
		$this->set_post_meta('_stock', 					$stock_amt);
		// $this->set_post_meta('_stock_status',			$stock_amt || !empty($next_available) ? 'instock' : 'outofstock');
		$this->set_post_meta('_stock_status',			$stock_amt ? 'instock' : 'outofstock');
		// $this->set_post_meta('_stock_status',			'instock');
		$this->set_post_meta('_variation_description',	'');

	}

	public function before_save() {
		
		$this->set_data_from_api();

		$global_sequence	= $this->get_api_item_value( 'GlobalSequence', 0 ) ?: $this->get_id();
		$global_sequence 	= str_pad($global_sequence, 3, '0', STR_PAD_LEFT);

		$id 				= $this->get_id();
		$parent_id			= $this->get_parent_id();
		$post_type 			= $this->get_post_type();

		$this->set_post_data('post_parent', $parent_id);

		$this->set_post_data('post_title', sprintf('Variation %s of product %s', $global_sequence, $parent_id));
		$this->set_post_data('post_name', sprintf('product-%s-variation-%s', $parent_id, $global_sequence));
		$this->set_post_data('guid', sprintf('%s/?%s=product-%s-variation-%s', home_url(), $post_type, $parent_id, $global_sequence));

		// if(isset($this->changes['post_data'])) var_dump($this->changes['post_data']);
		// if(isset($this->data['post_data'])) var_dump($this->data['post_data']);
	}


	public function after_save() {

		$post_id = $this->get_id();

		if(function_exists('wc_delete_product_transients')):
			wc_delete_product_transients( $post_id );
		endif;
	}

	public function set_parent_id( $id ) {
		
		$id = absint($id);
		$this->set_post_data('post_parent', absint($id));
	}

	public function get_parent_id() {
		return $this->get_post_data('post_parent');
	}

	public function get_sku_id() {
		return $this->get_unique_meta_value();
	}

	public function set_attribute_term( $type, $term = null ) {

		$taxonomy = wc_attribute_taxonomy_name( $type );
		$term = YXML()->attributes->filter_term_result( $term, $taxonomy );
		
		if($term):
			$this->attribute_terms[ $type ] = $term;
			$this->set_post_meta('attribute_' . $taxonomy, $term['slug']);
		else:
			unset($this->attribute_terms[ $type ]);
			$this->set_post_meta('attribute_' . $taxonomy, null);
		endif;

	}

	public function get_attribute_terms( $names_only = true ) {

		$terms = array_filter($this->attribute_terms, function( $term ){ return $term && !empty($term); });

		return $terms;
	}

	public function add_colour( $term ) {
		$this->set_attribute_term( 'colour', $term );
	}

	public function add_size( $term ) {
		$this->set_attribute_term( 'size', $term );
	}

	public function get_attributes() {
		return $this->attribute_terms;
	}

}
