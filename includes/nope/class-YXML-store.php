<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}




class YXML_Store extends YXML_Post {

	protected $object_type	 			= 'store';

	protected $import_object_type	 	= 'Store';
	
	protected $post_type	 		 	= 'yxml_store';

	protected $unique_meta_key 		 	= 'store_id';

	protected $unique_import_item_key 	= 'StoreId';

	public function get_import_field_map() {

		return array(
				// 'StoreId' 		=> '4183',
				'Code' 			=> array('meta' => 'code'),
				'StoreNo' 		=> array('meta' => 'store_no'),
				'PhoneNum' 		=> array('meta' => 'phone'),
				'Name' 			=> array('post' => 'post_title'),
				'Address1' 		=> array('meta' => 'address_1'),
				'Address2' 		=> array('meta' => 'address_2'),
				'City' 			=> array('meta' => 'city'),
				'State' 		=> array('meta' => 'state'),
				'Postcode' 		=> array('meta' => 'postcode'),
				'Country' 		=> array('meta' => 'country'),
				'Email'			=> array('meta' => 'email')
			);
		
	}


}