<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



function yxml_product_images() {

	/*if(is_admin() && current_user_can('shop_manager' )):
		if(isset($_GET['yxml_product_images'])):
			new Temp_Imgs;
		endif;
	endif;*/
}

/**
* 
*/
class YXML_Image_Import {

	private $i = 0;
	private $i_lim = 0;
	
	function __construct() {

		$this->define_constants();
		$this->init_hooks();

		
	}

	public function init_hooks() {
		
	}

	public function define_constants() {

		$upload_dir = wp_upload_dir();

		define('TEMP_IMG_DIR', $upload_dir['basedir'] . '/YOKE_IMAGES_FOR_DOBSONS' );
		define('TEMP_IMG_DIR_URL', $upload_dir['baseurl'] . '/YOKE_IMAGES_FOR_DOBSONS' );

		define('A21_IMG_DIR', $upload_dir['basedir'] . '/a21-imported-media' );
		define('A21_IMG_DIR_URL', $upload_dir['baseurl'] . '/a21-imported-media' );
	}

	public function add_i() {
		$this->i++;
		if($this->i_lim && $this->i >= $this->i_lim):
			die($this->i . ' iterations completed');
		endif;
	}
	public function init() {

		// $located = $this->build_results();
		// $results = $this->clean_results($located['results']);

		$file_data = file_get_contents(YXML_LOCAL_DATA_PATH . '/product_imgs_impot.json');
		$results = json_decode($file_data);
		
		$this->process($results);
		// $this->display($located['results']);
	}

	public function clean_results( $results ) {

		$clean = array();

		foreach ($results as $post_id => $files):

			$images = array_filter($files, array($this, 'is_image_file'));

			if(empty( $images )) continue;

			$img_paths = array();

			foreach ($images as $img_i => $image):

				$image_url = str_replace(TEMP_IMG_DIR, TEMP_IMG_DIR_URL, $image);
				$new_file_name = str_replace(TEMP_IMG_DIR, '', $image);
				$new_file_name = str_replace(' ', '', $new_file_name);
				$new_file_name = str_replace('/', '-', trim($new_file_name, '/'));
				$new_path = A21_IMG_DIR . '/' . $new_file_name;
				$new_url = A21_IMG_DIR_URL . '/' . $new_file_name;

				// $new_url = str_replace(TEMP_IMG_DIR, A21_IMG_DIR_URL, $image);
				$img_paths[] = array(
					'path' => $image,
					'url' => $image_url,
					'new_path' => $new_path,
					'new_url' => $new_url
				);

			endforeach;
			
			// p($img_paths);

			$clean[$post_id] = $img_paths;

		endforeach;
		
		return $clean;	
	}
	
	public function process( $results, $tasks = array('copy', 'attach', 'display') ) {


		// die(json_encode($results));
		set_time_limit(1800);
		
		$replace_images = false;

		foreach ($results as $post_id => $images):
			// if($post_id != 6933) continue;


			$_thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
			$_product_image_gallery = get_post_meta( $post_id, '_product_image_gallery', true );
			
			p('post_id: '. $post_id);
			// if(empty($_product_image_gallery) && empty($_thumbnail_id) ) continue;
			// var_dump('$_thumbnail_id: ', $_thumbnail_id);
			// var_dump('$_product_image_gallery: ', $_product_image_gallery);
			// continue;
			$new_thumbnail_id = null;
			$new_product_image_gallery = array();

			if(!$replace_images && ( !empty($_product_image_gallery) && !empty($_thumbnail_id) ) ) continue;

			foreach ($images as $img_i => $image_paths):
				
				$image_paths = (array) $image_paths;
				
				if(in_array('copy', $tasks) && !file_exists($image_paths['new_path'])):
					if(copy($image_paths['path'],$image_paths['new_path'])):
						p('copied: ' . $image_paths['path'] . ' to: ' . $image_paths['new_path']);
					endif;
				endif;

				if(in_array('attach', $tasks) && file_exists($image_paths['new_path'])):

					// $filename should be the path to a file in the upload directory.
					$attach_url = $image_paths['new_url'];
					$attach_path = $image_paths['new_path'];

					$attach_id = $this->get_image_id_by_url( $attach_url );

					if(!$attach_id):
						// The ID of the post this attachment is for.
						$parent_post_id = $post_id;

						// Check the type of file. We'll use this as the 'post_mime_type'.
						$filetype = wp_check_filetype( basename( $attach_path ), null );

						// Get the path to the upload directory.
						$wp_upload_dir = wp_upload_dir();

						// Prepare an array of post data for the attachment.
						$attachment = array(
							'guid'           => $attach_url, //$wp_upload_dir['url'] . '/' . basename( $attach_path ), 
							'post_mime_type' => $filetype['type'],
							'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $attach_path ) ),
							'post_content'   => '',
							'post_status'    => 'inherit'
						);

						// Insert the attachment.
						p($attachment);
						
						$attach_id = wp_insert_attachment( $attachment, $attach_path, $parent_post_id );

					// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
						require_once( ABSPATH . 'wp-admin/includes/image.php' );

					// Generate the metadata for the attachment, and update the database record.
						$attach_data = wp_generate_attachment_metadata( $attach_id, $attach_path );
						wp_update_attachment_metadata( $attach_id, $attach_data );

					endif;

					if($img_i == 0):
						$new_thumbnail_id = $attach_id;
					else: 
						$new_product_image_gallery[] = $attach_id;
					endif;
				endif;
	
				$this->add_i();

			endforeach;

			if(in_array('attach', $tasks)):


				if(!empty($new_product_image_gallery) && ($replace_images || empty($_product_image_gallery))):
					update_post_meta( $post_id, '_product_image_gallery', implode(',', $new_product_image_gallery) );
					var_dump($post_id . ' ADDED new_product_image_gallery', $new_product_image_gallery);
				endif;
				if(!empty($new_thumbnail_id) && ($replace_images || empty($_thumbnail_id))):
					set_post_thumbnail( $post_id, $new_thumbnail_id );
					var_dump($post_id . ' ADDED new_thumbnail_id', $new_thumbnail_id);
				endif;


			endif;


		endforeach;

		p($this->i);
		// p($results);
		die();
		/*	echo '<div style=""><h3>' . get_the_title($post_id) . '</h3>';
			
			foreach ($images as $img_i => $image):
				$image_url = $this->url_from_file_path( $image );
			// p($image_url);
				?>
				<img style="height: 100px; width: auto; " src="<?php echo $image_url; ?>" alt="">
				<?php 
				$this->add_i();
			endforeach;
			
			echo '</div>';

		die();*/
	}

	public function get_image_id_by_url($image_url) {
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s' LIMIT 1", $image_url )); 
		return (!empty($attachment) ? $attachment[0] : false); 
	}
	
	public function url_from_file_path( $file_path ) {
		return str_replace(TEMP_IMG_DIR, TEMP_IMG_DIR_URL, $file_path);
	}

	public function is_image_file( $file_path ) {

		if(is_dir($file_path)) return false;
		$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
		$detectedType = exif_imagetype( $file_path );
		return in_array($detectedType, $allowedTypes);

	}

	public function attach( $post_id, $results ) {

		$post_id = 1234;
		$images = array('filename1.png', 'filename2.png', 'filename10.png');

		// Get the path to the upload directory.

		$wp_upload_dir = wp_upload_dir();

		foreach($images as $name) {

		    $attachment = array(
		        'guid'=> $wp_upload_dir['url'] . '/' . basename( $name ), 
		        'post_mime_type' => 'image/png',
		        'post_title' => 'my description',
		        'post_content' => 'my description',
		        'post_status' => 'inherit'
		         );

		    $image_id = wp_insert_attachment($attachment, $name, $post_id);

		    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		    require_once( ABSPATH . 'wp-admin/includes/image.php' );
		 
		    // Generate the metadata for the attachment, and update the database record.
		    $attach_data = wp_generate_attachment_metadata( $image_id, $name );

		    wp_update_attachment_metadata( $image_id, $attach_data );

		}

	}

	public function display( $results ) {

		foreach ($results as $post_id => $files):
			$images = array_filter($files, array($this, 'is_image_file'));
			echo '<div style=""><h3>' . get_the_title($post_id) . '</h3>';
			foreach ($images as $img_i => $image):
				$image_url = $this->url_from_file_path( $image );
			// p($image_url);
				?>
				<img style="height: 100px; width: auto; " src="<?php echo $image_url; ?>" alt="">
				<?php 
				$this->add_i();
			endforeach;
			echo '</div>';
		endforeach;
		die();
	}

	public function build_results( $count_only = true ) {


		$session = (
			isset($_SESSION['found_stuff']) 
			? $_SESSION['found_stuff'] 
			: array('results' => array(), 'no_images' => array()));


		if($count_only) return $session;

		set_time_limit(600);

		$skus = $this->get_all_skus();

		$results = array();
		$no_images = array();

		foreach ($skus as $key => $value):

			if(
				isset($session['no_images'][$value['post_id']]) ||
				isset($session['results'][$value['post_id']])
				):
				// p('skipping: ' . $value['post_id']);
				 continue;
			endif;
			$search_result = $this->sku_search($value['meta_value']);
			
			if(empty($search_result)):
				$no_images[$value['post_id']] = $value['meta_value'];
			else:
				$results[$value['post_id']] = $search_result;
			endif;

			$session['results'] += $results;
			$session['no_images'] += $no_images;
			$_SESSION['found_stuff'] = $session;
			
		endforeach;

		// p('FOUND IMAGES FOR ' . count($session['results']) . ' OF ' . (count($session['results']) + count($session['no_images'])) . ' FROM ' . count($skus));

		return $session;
		// die();
	}

	public function get_all_skus() {

		global $wpdb;

		$sql = "SELECT post_id, meta_value FROM wp_postmeta WHERE meta_key = 'yxml_product_code'";// LIMIT 500";

		$results = $wpdb->get_results($sql, 'ARRAY_A');

		// p($results);
		return $results;
	}

	public function sku_search($sku) {

		// p(TEMP_IMG_DIR);

		$search_results = $this->search_directory( TEMP_IMG_DIR, $sku );
		// p($search_results);
		return $search_results;
		
	}

	public function search_directory( $dir_path, $search_string, $recursive = true ) {

		$results = array();

		$full_path = $dir_path . '/*' . $search_string . '*';
		$file_search = glob( $full_path );

		$results = $file_search;

		if($recursive):
			$dirs = glob( $dir_path . '/*', GLOB_ONLYDIR);
			foreach ($dirs as $key => $sub_dir):
				$results += $this->search_directory( $sub_dir, $search_string );
			endforeach;
		endif;

		return $results;

	}
}