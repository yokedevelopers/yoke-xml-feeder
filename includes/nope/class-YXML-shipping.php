<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class YXML_Shipping {

	/**
	 * @since    1.0.0
	 */

	
	public function __construct() {

		$this->init_hooks();

	}

	public function init_hooks() {
			
		// 'woocommerce_email_order_meta_fields'

		add_filter( 'woocommerce_shipping_methods', array($this, 'filter_shipping_methods'), 10, 1 );

		// define the woocommerce_available_shipping_methods callback 
		add_filter( 'woocommerce_shipping_packages', array($this, 'filter_woocommerce_available_shipping_packages'), 10, 1 ); 
		add_filter( 'woocommerce_available_shipping_methods', array($this, 'filter_woocommerce_available_shipping_methods'), 10, 1 ); 


	}

	public function shipping_methods() {

		return array(
			'click_and_collect' => 'YXML_Shipping_Click_And_Collect',
			'ap21_freight' => 'YXML_Shipping_API_Freight'
			);

	}

	public function get_shipping_method_instance_from_order() {

	}

	public function filter_shipping_methods( $methods ) { 

		$methods = array_merge(
			$methods,
			$this->shipping_methods()
			);
		
		return $methods;
	}

	public function filter_woocommerce_available_shipping_packages( $packages ) { 

	    // p($packages);
	    // die();
	    return $packages; 
	} 

	public function filter_woocommerce_available_shipping_methods( $methods ) { 

	    // p($methods);
	    // die();
	    return $methods; 
	}

	public function get_yxml_order_shipping( $wc_order ) {

		$shipping_data = array();

		$order_shipping_methods = $wc_order->get_shipping_methods();

		foreach ($this->shipping_methods() as $method_id => $method_class):
			if($wc_order->has_shipping_method($method_id)):
			endif;
		endforeach;

		if( $wc_order->has_shipping_method( 'click_and_collect' ) ):
			
			$shipping_data['DespatchType'] = 'ClickAndCollect';
			$shipping_data['PickupStoreId'] = $wc_order->get_meta( YXML_Shipping_Click_And_Collect::get_store_id_var_name() );

		elseif( $wc_order->has_shipping_method( 'ap21_freight' ) ):

			$shipping_tax = (float) $wc_order->get_shipping_tax();
			$shipping_total = (float) $wc_order->get_shipping_total();

			$class = null;

			foreach ($order_shipping_methods as $key => $shipping_method):

				$method_id = $shipping_method->get_method_id();
				$method_id_arr = explode(':', $method_id);

				if(!$method_id_arr[0] == 'ap21_freight') continue;

				$class = $this->shipping_methods()['ap21_freight'];

			endforeach;

			if(!$class) return $shipping_data;

			$instance = new $class( $method_id_arr[1] );

			$api_freight_option_id = $instance->get_api_freight_option_id();

			$shipping_data['SelectedFreightOption'] = array(
				'Id' => $api_freight_option_id,
				'Name' => $wc_order->get_shipping_method(),
				'Value' => $shipping_total + $shipping_tax,
				);
		endif;



		return apply_filters( 'yxml_order_shipping_data', $shipping_data, $wc_order ); 
	}


}

class YXML_Shipping_API_Freight extends WC_Shipping_Method {

	/** @var string cost passed to [fee] shortcode */
	// protected $fee_cost = '';

	/**
	 * Constructor.
	 */
	public function __construct( $instance_id = 0 ) {

		$this->id                    = 'ap21_freight';
		$this->instance_id 			 = absint( $instance_id );
		$this->method_title          = __( 'Yoke XML Freight Option', 'woocommerce' );
		$this->method_description    = __( 'Lets you use an Yoke XML calculation for shipping.', 'woocommerce' );
		$this->supports              = array(
			'shipping-zones',
			'instance-settings',
			'instance-settings-modal',
		);

		$this->init();

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		add_filter( 'woocommerce_shipping_' . $this->id . '_is_available', array($this, 'filter_is_available'), 20, 2);

		add_action( 'woocommerce_checkout_create_order', array($this, 'add_shipping_to_order_meta'), 20, 2 );
	}

	public function filter_is_available( $available, $package ) {
		return true;
		return YXML()->cart->has_freight_option( $this->get_api_freight_option_id() );
	}

	public function get_api_freight_option_id() {
		return $this->api_freight_option_id;
	}
	/**
	 * init user set variables.
	 */
	public function init() {

		// $this->instance_form_fields = include( 'includes/settings-flat-rate.php' );
		$this->init_form_fields();
		$this->init_settings();

		$this->title                = $this->get_option( 'title' );
		$this->tax_status           = 'None';
		$this->tax_percent           = $this->get_option( 'tax_percent' );
		$this->cost                 = $this->get_option( 'cost' );

		$this->api_freight_option_id    = $this->get_option( 'api_freight_option_id' );
	}
	
	public function init_form_fields() {

		$this->instance_form_fields = array(
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'yoke' ),
				'default'     => __( 'Standard Shipping', 'yoke' ),
				'desc_tip'    => true,
			),
			'api_freight_option_id' => array(
				'title' 		=> __( 'Freight Option Id', 'yoke' ),
				'type' 			=> 'text',
				'class'         => '',
				'default' 		=> '',
				'desc_tip'    	=> true,
				'required'		=> true,
				'description' 	=> __( 'Freight Id (Read from Yoke XML system GIT)', 'yoke' ),
			),					
			'cost' => array(
				'title' 		=> __( 'Cost', 'yoke' ),
				'type' 			=> 'text',
				'placeholder'	=> '',
				'description'	=> __( 'Amount to charge (ex. Tax)', 'yoke' ),
				'default'		=> '0',
				'desc_tip'		=> true,
			),
			'tax_percent' => array(
				'title' 		=> __( 'Tax Percent', 'yoke' ),
				'type' 			=> 'text',
				'placeholder'	=> '',
				'description'	=> __( 'Added to the total cost', 'yoke' ),
				'default'		=> '0',
				'desc_tip'		=> true,
			),
		);
	}

	/**
	 * calculate_shipping function.
	 *
	 * @param array $package (default: array())
	 */
	
	public function calculate_shipping( $package = array() ) {

		$freight_option = YXML()->cart->get_freight_option( $this->get_api_freight_option_id() );

		if(!$freight_option) return;

		$freight_option = (array) $freight_option;

		$freight_option['Value'] = $this->cost;
		$freight_option['TaxPercent'] = $this->tax_percent;

		// $freight_option_id = isset($freight_option['Id']) ? (string) $freight_option['Id'] : '';
		$freight_option_name = isset( $freight_option['Name'] ) ? (string) $freight_option['Name'] : '';
		$freight_option_value = (float) isset($freight_option['Value']) ? (string) $freight_option['Value'] : '';
		$freight_option_tax_percent = (float) isset($freight_option['TaxPercent']) ? (string) $freight_option['TaxPercent'] : 0;

		$freight_tax = $freight_option_tax_percent > 0 && $freight_option_value > 0 ? $freight_option_value/$freight_option_tax_percent : 0;

		$rate = array(
			'id'      => $this->get_rate_id(),
			'label'   => $this->title,
			'cost'    => $freight_option_value,
			'taxes'	=> $freight_tax,
			'package' => $package,
		);

		// Calculate the costs
		$has_costs = true; //$freight_option_value > 0; // True when a cost is set. False if all costs are blank strings.

		// Add the rate
		if ( $has_costs ) {
			$this->add_rate( $rate );
		}

		do_action( 'woocommerce_' . $this->id . '_shipping_add_rate', $this, $rate );
	}

	public static function get_api_freight_option_id_var_name(  ) {
		return 'api_freight_option_id';
	}

	public function add_shipping_to_order_meta( $order, $data ) {

		ob_start();

		// echo '<pre>';	

		if($order->has_shipping_method( $this->id )):
			$api_freight_option_id = $this->get_option( 'api_freight_option_id' );
			$var_key = self::get_api_freight_option_id_var_name();
			$order->add_meta_data( $var_key, $api_freight_option_id, true );
		endif;

		// print_r($order);
		// print_r($data);

		// echo '</pre>';
		$output = ob_get_clean();
		// YXML()->orders->throw_err( $output );
	}
}


class YXML_Shipping_Click_And_Collect extends WC_Shipping_Local_Pickup {

	public function __construct( $instance_id = 0 ) {

		$this->id                    = 'click_and_collect';
		$this->instance_id 			 = absint( $instance_id );
		$this->method_title          = __( 'Click & Collect', 'yoke' );
		$this->method_description    = __( 'Allow customers to pick up orders from Yoke XML Stores.', 'yoke' );
		$this->supports              = array(
			'shipping-zones',
			'instance-settings',
			'instance-settings-modal',
		);

		add_filter( 'woocommerce_shipping_' . $this->id . '_is_available', array($this, 'filter_is_available'), 20, 2);

		$this->tax_status	     = 'none';
		$this->cost	             = '';

		$this->init();

	}


	public function init() {

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();


		// Define user set variables
		$this->title     		= $this->get_option( 'title' );
		$this->description    	= $this->get_option( 'description' );

		// Actions
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

		add_action( 'woocommerce_checkout_update_order_review', array( $this, 'checkout_update_order_review' ), 10, 1 );
		// add_action( 'woocommerce_update_order_review_fragments', array( $this, 'update_order_review_fragments' ), 10, 1 );
		add_action( 'woocommerce_after_shipping_rate', array( $this, 'after_shipping_rate' ), 10, 2 );
		
		add_action('woocommerce_after_checkout_validation', array($this, 'validate_checkout'), 20, 2);
		// add_action('woocommerce_checkout_order_processed', array($this, 'validate_checkout_order'), 20, 3);
		// add_filter('woocommerce_checkout_fields', array($this, 'add_store_select_field'), 20, 3);
		add_action( 'woocommerce_checkout_create_order', array($this, 'add_shipping_to_order_meta'), 20, 2 );


	}


	public function add_store_select_field( $fields ) {

		$fields['click_collect_store'] = array(
					'type'         => 'select',
					// 'label'        => __( 'Account password', 'woocommerce' ),
					'options' 	=> $this->get_stores(),
					'required'     => true,
					'placeholder'  => esc_attr__( 'Password', 'woocommerce' ),
				);

		return $fields;
	}

	/**
	 * calculate_shipping function.
	 * Calculate local pickup shipping.
	 */
	public function calculate_shipping( $package = array() ) {
		$this->add_rate( array(
			'label' 	 => $this->title,
			'package'    => $package,
			'cost'       => $this->cost,
		) );
	}

	/**
	 * Init form fields.
	 */
	public function init_form_fields() {

		$this->instance_form_fields = array(
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Click & Collect', 'yoke' ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title' 		=> __( 'Description', 'woocommerce' ),
				'type' 			=> 'textarea',
				'class'         => '',
				'default' 		=> '',
				'desc_tip'    => true,
				'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
			)
		);
	}

	public function checkout_update_order_review( $post_args = array( )) {
		// var_dump($post_args);
		// die();

	}
	public function update_order_review_fragments( $fragments = array( )) {
		
		// Get checkout payment fragment
	    /*ob_start();
	    $this->click_and_collect_options_html();
	    $yxml_click_and_collect_options_html = ob_get_clean();
		$fragments['.yxml_click_collect_options_container'] = $yxml_click_and_collect_options_html;*/

		return $fragments;

	}

	public function filter_is_available( $available, $package ) {

		if(empty($this->get_stores())) $available = false;

		return $available;
	}

	public function after_shipping_rate( $method, $index ) {

		if($method->method_id == $this->id):
			if(!did_action('yxml_shipping_after_rat_' . $this->id)):
				do_action('yxml_shipping_after_rat_' . $this->id);
	    		$this->click_and_collect_options_html();
	    	endif;
	    endif;
	}
	

	public function is_chosen_method() {
		
		$is_chosen = false;

		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );

		foreach($chosen_methods as $key => $method):
		
			$id_in = strrpos($method, $this->id );

			if($id_in === 0):
				$is_chosen = true;
			endif;
		endforeach;
		
		return $is_chosen;
	}

	public function get_chosen_location() {

		if(!isset($_POST['post_data'])) return null;

		parse_str($_POST['post_data'], $post_data);

		return isset($post_data['yxml_click_collect_store_id']) ? $post_data['yxml_click_collect_store_id'] : null;
	}

	public function get_stores() {

		$stores = YXML()->cart->get_stores();
		return $stores;
	}

	public function click_and_collect_options_html() {

		if(!is_checkout()) return '';

		$is_chosen = $this->is_chosen_method();

		$chosen_location = $this->get_chosen_location();
		
		$output_html = '<br/>';

		$locations = $this->get_stores();

		$options_output_html = '';
		if($locations && is_array($locations)):
			foreach ($locations as $key => $value):
				$options_output_html .= sprintf(
					'<option value="%s" %s>%s</option>',
					$key,
					$key == $chosen_location ? 'selected="selected" ' : '',
					$value
					);
			endforeach;
		endif;

		$output_html = sprintf('
			<div class="shipping_method_options shipping_method_%1$s">
				<label for="%3$s_%2$s" class="">%4$s</label>
				<select id="%3$s_%2$s" name="%3$s" class="yxml_click_collect_options" data-placeholder="">%5$s</select>
			<div>',
			$this->id,
			$this->instance_id,
			self::get_store_id_var_name(),
			__('Click &amp; Collect Location', 'yoke'),
			$options_output_html
			);

		$output_html = apply_filters( 'yxml_click_and_collect_options_html', $output_html, $locations );

		echo $output_html;

	}

	public static function get_store_id_var_name() {
		return 'click_collect_store_id';
	}

	public function validate_checkout( $data, $errors ) {
		
		if($this->is_chosen_method()):
			
			if(empty($this->get_posted_pickup_store_id())):
				$errors->add( self::get_store_id_var_name(), __( 'You must select a <strong>Click & Collect location</strong>.', 'yoke' ) );
			endif;

		endif;

	}

	public function get_posted_pickup_store_id() {
		
		// if($this->is_chosen_method()) return false;

		$posted_data = $_POST;
		$var_key = self::get_store_id_var_name();
		// var_dump($posted_data);
		return isset($posted_data[$var_key]) ? $posted_data[$var_key] : null;

	}

	public function add_shipping_to_order_meta( $order, $data ) {

		if($order->has_shipping_method( $this->id )):
			$pickup_store_id = $this->get_posted_pickup_store_id();
			$var_key = self::get_store_id_var_name();
			$order->add_meta_data( $var_key, $pickup_store_id, true );
		endif;

	}
}




