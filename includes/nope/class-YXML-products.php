<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Products extends YXML_Post_Factory {

	protected $factory_id	 			= 'product';

	protected $single_class	 		 	= 'YXML_Product';

	protected $post_type	 		 	= 'product';

	protected $import_object_type	 	= 'product';

	public function __construct() {
		$this->init_hooks();
		parent::__construct();
	}
	
	private function init_hooks() {

		add_filter( 'posts_where', array($this, 'filter_posts_where' ), 10, 2);

		add_action( 'yxml/action/update_product_from_api', array( $this, 'update_product_from_api'), 10, 1 );

		add_filter( 'yxml_admin_settings', array( $this, 'add_settings_page') );
		// add_filter( 'yxml_post_args', array($this, 'draft_by_reference'), 10, 2 );
		add_action( 'manage_product_posts_custom_column', array($this, 'add_admin_col_data'), 10, 2 );

		add_filter( 'woocommerce_product_is_visible', array($this, 'filter_product_visibility'), 10, 2 );
		add_filter( 'woocommerce_product_query_tax_query', array($this, 'filter_woocommerce_product_query_tax_query'), 10, 2 );

	}

	public function get_hidden_references() {
		return $this->get_option('hide_by_reference');
	}

	public function get_active_references() {
		return $this->get_option('show_by_reference');
	}
	
	public function get_import_references() {
		return $this->get_option('import_references');
	}

	public function add_settings_page($option_settings) {
		$option_settings['product_settings'] = array(
					'label' 	=> 	__('Product Settings', 'yoke'),
					'desc'		=> 	__('', 'yoke'),
					'fields' 	=> 	array(
										'import_references' 		=> 	array(
																		'label' => __('Only Import', 'yoke'),
																		'type' 	=> 'select',
																		'options' => yxml_product_references_select_values(),
																		'field_args' => array('multiple'),
																		'desc'	=> __('Only import products which have <strong>ALL</strong> selected references.', 'yoke')
																	),
										'draft_by_reference' 		=> 	array(
																		'label' => __('Import as \'Draft\'', 'yoke'),
																		'type' 	=> 'select',
																		'options' => yxml_product_references_select_values(),
																		'field_args' => array('multiple'),
																		'desc'	=> __('Import products as \'draft\' where products have the selected references.', 'yoke')
																	),
										'show_by_reference'			=> 	array(
																		'label' => __('Show in Shop', 'yoke'),
																		'type' 	=> 'select',
																		'options' => yxml_product_references_select_values(),
																		'field_args' => array('multiple'),
																		'desc'	=> __('If set, show only products in the shop which have <strong>ALL</strong> selected references.', 'yoke')
																	),
										'hide_by_reference'			=> 	array(
																		'label' => __('Hide in Shop', 'yoke'),
																		'type' 	=> 'select',
																		'options' => yxml_product_references_select_values(),
																		'field_args' => array('multiple'),
																		'desc'	=> __('Hide products in the shop which have <strong>ANY</strong> selected references.', 'yoke')
																	)
									)
				);

		add_action( 'yxml_after_admin_options_form_fields_tab_product_settings', array($this, 'show_hidden_products_count') );


		return $option_settings;
	}

	public function get_hidden_terms() {

		$references = $this->get_hidden_references();
		if(empty($references)) return array();
		$terms = YXML()->references->get_terms_by_reference_ids( $references );
		return ( $terms ?: array() );

	}

	public function get_active_terms() {

		$references = $this->get_active_references();
		if(empty($references)) return array();
		$terms = YXML()->references->get_terms_by_reference_ids( $references );
		return ( $terms ?: array() );

	}

	public function filter_product_visibility( $visible, $post_id ) {

		if(!$visible) return $visible;
		return $this->is_visible_in_shop( $post_id );

	}

	public function is_visible_in_shop( $post_id ) {

		$active_terms = $this->get_active_terms();

		foreach ($active_terms as $key => $term):
			if( !has_term( (int) $term['term_id'], $term['taxonomy'], $post_id ) ) return false;
		endforeach;

		$hidden_terms = $this->get_hidden_terms();

		foreach ($hidden_terms as $key => $term):
			if( has_term( (int) $term['term_id'], $term['taxonomy'], $post_id ) ) return false;
		endforeach;

		return true;
	}


	public function filter_woocommerce_product_query_tax_query($tax_query, $WC_Query) {

		$hidden_tax_query = $this->get_hidden_tax_query(  );

		if($hidden_tax_query) $tax_query[] = $hidden_tax_query;

		return $tax_query;
	}

	public function get_hidden_tax_query( ) {

		$this_query = array('relation' => 'AND');
		$has_terms = false;
		
		$active_terms = $this->get_active_terms();
		
		if(!empty($active_terms)):
			
			$has_terms = true;
			$active_query = array('relation' => 'AND');

			$tax_arr = array();

			foreach ($active_terms as $ref_id => $term):
				if(!isset($tax_arr[$term['taxonomy']])) $tax_arr[$term['taxonomy']] = array();
				$tax_arr[$term['taxonomy']][] = $term['term_id'];
			endforeach;

			foreach ($tax_arr as $taxonomy => $term_ids):
				foreach ($term_ids as $term_id):
					$active_query[] = array(
							'taxonomy' => $taxonomy,
							'field' => 'term_id',
							'terms' => $term_id
						);
				endforeach;
			endforeach;

			$this_query[] = $active_query;
		endif;
		
		$hidden_terms = $this->get_hidden_terms();
		
		if(!empty($hidden_terms)):
			
			$has_terms = true;
			$hidden_query = array('relation' => 'AND');

			$tax_arr = array();

			foreach ($hidden_terms as $ref_id => $term):
				if(!isset($tax_arr[$term['taxonomy']])) $tax_arr[$term['taxonomy']] = array();
				$tax_arr[$term['taxonomy']][] = $term['term_id'];
			endforeach;

			foreach ($tax_arr as $taxonomy => $term_ids):
				$hidden_query[] = array(
						'taxonomy' => $taxonomy,
						'field' => 'term_id',
						'terms' => $term_ids,
						'operator' => 'NOT IN',
					);
			endforeach;

			$this_query[] = $hidden_query;
		endif;


		return ($has_terms ? $this_query : false);
		// die()
	}

	public function show_hidden_products_count(  ) {
		echo sprintf(__('<strong>There are currently %s products hidden by filters.</strong>', 'yoke'), $this->count_hidden_products());
	}

	public function count_hidden_products(  ) {
		
		$hidden_tax_query = $this->get_hidden_tax_query( );

		$query = new WP_Query( array(
			'post_type' => 'product',
			'posts_per_page' => -1,
			'post_status' => 'any',
			'tax_query' => $hidden_tax_query,
			'fields' => 'ids'
			) );

		$post_count = wp_count_posts('product');

		return ($post_count->publish - $query->found_posts);

	}

	public function filter_posts_where( $where, $query ) {

		// $start = microtime(true);
		$post_type = isset($query->query_vars['post_type']) ? $query->query_vars['post_type'] : false;

		if(!is_array($post_type)) $post_type = array($post_type);

		if(!in_array($this->get_post_type(), $post_type)) return $where;

		// return $where;
		// var_dump($where);
		// var_dump($query);
		global $wpdb;

		$hidden_tax_query = $this->get_hidden_tax_query( );

		$exclude_query = new WP_Query( array(
			'suppress_filters' => true,
			'post_type' => 'product',
			'posts_per_page' => -1,
			'post_status' => 'any',
			'tax_query' => $hidden_tax_query,
			'fields' => 'ids'
			) );
// var_dump($query);
		// var_dump($exclude_query->posts);
		// die();
		if(!empty($exclude_query->posts)):
			$not_in_str = implode(',', $exclude_query->posts);
			$wp_prefix = $wpdb->prefix;
			$where .= " AND ${wp_prefix}posts.ID IN ($not_in_str)";
		endif;
		// $finish = $start - microtime(true);
		// die($finish);
		// var_dump($exclude_query);
		// die();
		// var_dump($where);
		// die();
		return $where;

	}


	public function draft_by_reference( $args, $item ) {

		$ref_ids = $this->get_option('draft_by_reference');

		if(!empty($ref_ids) && count(array_intersect($ref_ids, $this->get_item_references( $item )))):
			$args['post_status'] = 'draft';
		endif;

		return $args;
	}

	public function get_item_references( $item ) {

		$references = $item->References->Reference;
		$reference_ids = array();
		
		if(method_exists($references, 'count') && $references->count()):
			foreach ($references as $key => $reference) $reference_ids[] = $reference->Id;
		endif;

		return $reference_ids;

	}

	public function add_admin_col_data( $column, $post_id ) {

	    if ( $column == 'name' ):
			$is_visible = YXML()->products->is_visible_in_shop($post_id);
			if(!$is_visible ) _e('Hidden by API settings', 'yoke');
		endif;
	}

	public function update_product_from_api( $request = array(), $redirect = true ) {

		if(is_numeric($request)) $request = array('product_id' => $request );
		$request = wp_parse_args( $request, array(
			'api_call_args' => array()
		) );

		$updated = false;

		$product_id = isset($request['product_id']) ? $request['product_id'] : null;
// var_dump('updating: ' . $product_id);
// return;
// die();
		$product = $this->get_post_by_id( $product_id );
		$api_id = $product->get_api_id();

		$api_call_args = $request['api_call_args'];
		// var_dump(__CLASS__ . '->' . __FUNCTION__ . '()');
		// var_dump("api_id: $api_id");

		if(!$api_id):
			
			YXML()->notices->err( sprintf(__('Update of <strong>\'%s\'</strong> failed! No product id found.', 'yoke'), get_the_title( $product_id )));
		else:

			add_filter('yxml_use_local_data', '__return_false');
			// var_dump($api_call_args);
			// $api_result =  YXML()->con->get('Products/' . $api_id, array('countryCode' => 'AU') );

			$api_result =  YXML()->con->get('Products/' . $api_id, $api_call_args );
			// $freestock_result = YXML()->con->get('Freestock/Style/' . $api_id, $api_call_args );
			// $api_result_simple =  YXML()->con->get('ProductsSimple/' . $api_id );

			// $api_product_notes = YXML()->con->get('ProductNotes/' . $api_id );

			// p($api_result_simple);
			// p($api_product_notes);
			// p($freestock_result);
			// p($api_result);
			// die();

			if((!$api_result || !$api_result->has_data()) && !YXML()->notices->has_errors()):
				YXML()->notices->err( sprintf(__('Update of <strong>\'%s\'</strong> failed! No API data found.', 'yoke')
						, get_the_title( $product_id )));
			else:
				$updated = $this->insert( $api_result );

				if(!$updated):
					YXML()->notices->err( sprintf(__('Update of <strong>\'%s\'</strong> failed! An error occured. ', 'yoke'), get_the_title( $product_id )));	
				else:
					YXML()->notices->msg( sprintf(__('Update of <strong>\'%s\'</strong> successful!', 'yoke'), get_the_title( $updated )));	
				endif;

			endif;

		endif;

		if($redirect):
			$ref = wp_get_referer();

			if($ref && (!defined('DOING_AJAX') || !DOING_AJAX )):
				wp_redirect( $ref );
				exit;
			endif;
		endif;
		
		return $updated;
	}
}

add_filter( 'manage_edit-product_columns', 'yxml_custom_product_column', 11);

function yxml_custom_product_column($columns) {

    $columns['yxml-actions'] = __( 'Actions','yoke');
    return $columns;
}

add_action( 'manage_product_posts_custom_column' , 'yxml_custom_orders_list_column_content', 10, 2 );

function yxml_custom_orders_list_column_content( $column, $post_id )  {

    switch ( $column ):
        case 'yxml-actions':
            echo sprintf('<a class="button" href="%s">%s</a>', yxml_update_product_from_api_link($post_id), __('API Update'));
            break;
    endswitch;
}

add_action( 'edit_form_after_title', 'yxml_add_after_post_title_api_update_button', 10, 1 );

function yxml_add_after_post_title_api_update_button( $post ) {
	if($post->post_type == 'product'):
		$post_id = $post->ID;
	    echo sprintf('<p>%s</p><p><a  class="button button-primary button-large" href="%s">%s</a></p>',
	    	__('Click the button below to update all information for this product from the Yoke XML database.', 'yoke'),
	    	yxml_update_product_from_api_link($post_id),
	    	__('API Update', 'yoke'));
	endif;
}

function yxml_update_product_from_api_link( $product_id ) {

    $current_uri = urlencode(home_url( add_query_arg( NULL, NULL ) ));    
    $update_url = admin_url( '?action=yxml_action&yxml_action=update_product_from_api&product_id=' . $product_id . '&_wp_http_referer=' . $current_uri);
    return $update_url;

}