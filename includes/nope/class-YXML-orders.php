<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Builds and sends an order to API
 *
 * @since      1.0.0
 * @package    YXML
 * @subpackage YXML/includes
 */

class YXML_Orders {

	/**
	 * @since    1.0.0
	 */


	public function __construct() {
		$this->init_hooks();
	}

	public function init_hooks() {
		
		add_action( 'yxml/action/submit_wc_order_to_api', array( $this, 'manual_submit_wc_order_to_api'), 10, 1 );
		// add_action( 'yxml/action/cron_minute', array($this, 'scheduled_order_tasks'), 10, 2 );
		add_action( 'yxml/action/cron_five_minutes', array($this, 'scheduled_order_tasks'), 10, 2 );
		// add_action( 'woocommerce_after_register_post_type', array($this, 'scheduled_order_tasks'), 90 );
		// if(isset($_GET['orders_test'])) add_action( 'init', array($this, 'scheduled_order_tasks'), 10 );

		// add_action('woocommerce_payment_complete', array( $this, 'submit_wc_order_to_api' ), 20, 1);

		add_action( 'manage_shop_order_posts_custom_column', array($this, 'add_admin_col_data'), 10, 2 );
		add_filter( 'manage_edit-shop_order_columns', array($this, 'custom_admin_cols'), 11, 1);			

	}

	public function get_api_order_id_meta_key( $include_prefix = true ) {
		$key = 'api_order_id';
		if($include_prefix && $key) $key =  YXML_DB_PREFIX . $key;
		return $key;
	}

	public function get_api_order_id( $wc_order_id ) {
		$key = $this->get_api_order_id_meta_key();
		return get_post_meta( $wc_order_id, $key, true );
	}	

	public function get_api_person_id_meta_key( $include_prefix = true ) {
		$key = 'api_person_id';
		if($include_prefix && $key) $key =  YXML_DB_PREFIX . $key;
		return $key;
	}

	public function get_api_person_id( $wc_order_id ) {
		$key = $this->get_api_person_id_meta_key();
		return get_post_meta( $wc_order_id, $key, true );
	}

	public function get_api_order( $wc_order_id ) {		

		$wc_order = wc_get_order( $wc_order_id );

		if(!$wc_order) return false;

		$api_order_id_meta_key = $this->get_api_order_id_meta_key();
		$api_person_id_meta_key = $this->get_api_person_id_meta_key();
		
		$order_id = get_post_meta( $wc_order->get_id(), $api_order_id_meta_key, true );
		$person_id = get_post_meta( $wc_order->get_id(), $api_person_id_meta_key, true );

		if($order_id && $person_id):
			return YXML()->con->get('Persons/' . $person_id . '/Orders/' . $order_id);
		else:
			return false;
		endif;

	}

	public function get_person_orders( $person_id ) {
		$orders = YXML()->con->get('Persons/' . $person_id . '/Orders/');
		return $orders;
	}

	public function manual_submit_wc_order_to_api( $request = array() ) {
		
		$updated = false;
		$wc_order_id = isset($request['order_id']) ? $request['order_id'] : null;

		$updated = $this->submit_wc_order_to_api( $wc_order_id, true );

		$edit_order_link = get_edit_post_link($wc_order_id);

		if($updated):
			YXML()->notices->msg( sprintf(__('Creation of Yoke XML Order from order <strong>#%s</strong> successful! Check <a href="%s">Order page</a> for details.', 'yoke'), $wc_order_id, $edit_order_link ));
		else:
			YXML()->notices->err( sprintf(__('Creation of Yoke XML Order from order <strong>#%s</strong> failed! Check <a href="%s">Order page</a> for errors.', 'yoke'), $wc_order_id, $edit_order_link ));
		endif;

		$ref = wp_get_referer();

		if($ref && (!defined('DOING_AJAX') || !DOING_AJAX )):
			wp_redirect( $ref );
			exit;
		endif;

		return $updated;
	}

	public function submit_wc_order_to_api( $order_id = null, $manual = false ) {

		$wc_order_updated = false;

		$wc_order = wc_get_order( $order_id );

		if( !$wc_order || is_wp_error($wc_order) ) return false;

		$api_order = $this->get_api_order( $wc_order ); // check for existing order

		$call = false;

		if($api_order && $api_order->has_data()):

			$api_order_id = property_exists($api_order, 'Id') ? (string) $api_order->Id : 'unknown';
			$error_note = sprintf(__("Order already exists. Id: %s", 'yoke'), $api_order_id);

		elseif($wc_order->has_status( apply_filters( 'yxml_invalid_order_statuses_for_api_sumbission', array( 'on-hold', 'pending', 'failed', 'cancelled' ), $this ) ) ):


			$error_note = sprintf(__("Orders with the status '%s' cannot be submitted to Yoke XML.", 'yoke'), $wc_order->get_status());

		else:

			$yxml_order = new YXML_Order_Package( );
			$yxml_order->create_from_wc_order( $order_id );

			$call = $yxml_order->get_call_object();

			if(is_object($call) && method_exists($call, 'get_return_location')):
			
				$location = $call->get_return_location();

				$order_api_meta = self::get_order_api_meta_from_location( $location );

				if($order_api_meta):

					$api_person_id = $order_api_meta['api_person_id'];
					$api_order_id = $order_api_meta['api_order_id'];

					$wc_order_updated = $this->set_api_order_meta( $wc_order, $api_order_id, $api_person_id );

				endif;
			endif;

		endif;

		if($wc_order_updated):

			$order_note = sprintf(__("Yoke XML Order created. <br/>OrderID: %s<br/>PersonId: %s", 'yoke'), 
				$api_order_id,
				$api_person_id
				);

			$this->update_order_products( $order_id );

		else:

			if( $call ):

				$call_result = $call->get_result();
				$error_id = $call_result->is_AP21_error();
				if($error_id) $error_note = $call_result->get_error() . ' (#'. $error_id .')';

			endif;

			$error_note = !empty($error_note) ? $error_note : __('Unknown error.', 'yoke');
			$order_note = sprintf(__("An attempt to create an Yoke XML Order failed: <strong>%s</strong>", 'yoke'), $error_note);
		endif;
		
		$wc_order->add_order_note( $order_note, false );

		return $wc_order_updated ? true : false;

	}

	public function update_order_products( $order_id ) {
		$wc_order = wc_get_order( $order_id );
		$order_items = $wc_order->get_items();
		$order_products = array();

		foreach ($order_items as $item_id => $order_item):
			// p($order_item);

			$product_id = wc_get_order_item_meta($item_id, '_product_id', true);
			$order_products[] = $product_id;
			// $variation_id = wc_get_order_item_meta($item_id, '_variation_id', true);
	// var_dump($product_id,
	// $variation_id);
		endforeach;

		$order_products = array_unique($order_products);
		// var_dump($order_products);

		foreach ($order_products as $key => $product_id):
			set_time_limit(15);
			YXML()->products->update_product_from_api(array('product_id' => $product_id), false);
		endforeach;

	}

	public function set_api_order_meta( $wc_order, $api_order_id, $api_person_id ) {

		$api_order_id_meta_key = $this->get_api_order_id_meta_key();
		$api_person_id_meta_key = $this->get_api_person_id_meta_key();

		$api_order_id_updated = update_post_meta( $wc_order->get_id(), $api_order_id_meta_key, $api_order_id);
		$api_person_id_updated = update_post_meta( $wc_order->get_id(), $api_person_id_meta_key, $api_person_id);

		return $api_order_id_updated;
			
	}

	public static function get_order_api_meta_from_location( $location = '' ) {

		if(preg_match('/Persons\/(?<api_person_id>[0-9]+)\/Orders\/(?<api_order_id>[0-9]+)/', $location, $matches)):
			
			return array(
				'api_person_id' => $matches['api_person_id'],
				'api_order_id' => $matches['api_order_id']
				);
		endif;

		return false;
		
	}

	public function get_cart_id() {
		$session = WC()->session;
		$cart_id = $session->generate_customer_id();
		return $cart_id;
	}

	public function get_cart() {
		$session = WC()->session;
		$cart_id = $session->generate_customer_id();
		return $cart_id;
	}

	public function custom_admin_cols( $columns ) {

		$position = count($columns) -1;

		$columns = array_merge(
				array_slice($columns, 0, $position, true),
	    		array('yxml-api-id' => __( 'AP21 OrderId', 'yoke')),
	    		array_slice($columns, $position, count($columns)-$position, true)
    		);
		
    	// $columns['yxml-api-id'] = __( 'AP21 OrderId', 'yoke');
    	return $columns;
	}

	public function add_admin_col_data( $column, $post_id ) {

		switch ($column):
			case 'yxml-api-id':
		    
		    	$id = $this->get_api_order_id( $post_id );
		    
		    	if($id):
					echo $id;
				else:
            		echo sprintf('<a class="button" href="%s">%s</a>', yxml_submit_wc_order_to_api_link($post_id), __('Send'));
            	endif;

				break;
			case 'order_actions':

				break;
			default:
				# code...
				break;
		endswitch;
	}

	public function get_new_orders() {
// return;
		$order_ids = array();

		$order_args = array(
			'posts_per_page' => -1,
			'date_after' => date('Y-m-d', (current_time( 'timestamp' ) - DAY_IN_SECONDS) ),
			'post_status' => apply_filters( 'yxml_valid_order_statuses_for_api_sumbission', array('processing') ),

		);

		$orders = wc_get_orders($order_args);
		// var_dump($order_args);
		// var_dump($orders);
		if(is_array($orders) && !empty($orders)):

			foreach ($orders as $key => $order):
				if( !$order->is_paid() || $this->get_api_order_id( $order->get_id() ) ) continue;
				$order_ids[] = $order->get_id();
			endforeach;
			
			// YXML()->log->log( __CLASS__ .'->'. __FUNCTION__ . '( \'' . $original_hook . '\' )');

		endif;

		if(!empty($order_ids)):
			ob_start();
			var_dump($order_ids);
			$log = ob_get_clean();
			YXML()->log->log( $log );
		endif;


		return $order_ids;
	}
	
	public function scheduled_order_tasks( $schedule = null, $original_hook = null ) {

		foreach ($this->get_new_orders() as $wc_order_id):

			if(get_post_meta( $wc_order_id, '_yxml_api_order_submitted', true )) continue;
			$updated = $this->submit_wc_order_to_api( $wc_order_id, true );
			update_post_meta( $wc_order_id, '_yxml_api_order_submitted', 1 );

		endforeach;

	}
   
}

function yxml_submit_wc_order_to_api_link( $order_id ) {

    $current_uri = urlencode(home_url( add_query_arg( NULL, NULL ) ));    
    $update_url = admin_url( '?action=yxml_action&yxml_action=submit_wc_order_to_api&order_id=' . $order_id . '&_wp_http_referer=' . $current_uri);
    return $update_url;
    
}


