<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class YXML_Cart_Package extends YXML_API_PutLoad {

	private $wc_order = null;
	private $wc_cart = null;

	protected $defaults = array();

	public function get_xml_object_name() {
		return 'Cart';
	}

	public function get_endpoint() {
		return 'Carts/1234';
	}

	public function create_hooks() {
		 add_filter('yxml_xml_output_item_name_' . $this->get_xml_object_name(), array($this, 'filter_item_name'), 10, 2);
	}
/*
	public function create_from_wc_order( $order ) {

		$this->create_hooks();

		if(!is_a($order, 'WC_Order') && is_numeric($order)) $order = wc_get_order( $order );

		$this->wc_order = $order;

		$config = $this->get_config();

		foreach ($config as $key => $field):
		
			$value = $this->process_field_callbacks( $field, $key );
			$this->set_output_field(array($key), $value);

		endforeach;

		$this->build_order_items();

	}

	public function create_from_wc_cart( $cart ) {

		$this->create_hooks();

		$this->wc_cart = $cart;

		$config = $this->get_config();

		foreach ($config as $key => $field):
		
			$value = $this->process_field_callbacks( $field, $key );
			$this->set_output_field(array($key), $value);

		endforeach;

		$this->build_order_items();

	}*/

	public function get_root_xml_node() {
		$node = $this->output;
		return $node;
	}
/*
	private function build_order_items() {

		// $cart = $this->get_root_xml_node();

		$order_items = $this->wc_order->get_items();

		// $cart_details = $cart->addChild('CartDetails');
		
		// $total_qty = 0;

		foreach ($order_items as $item_id => $order_item):
			

			$variation_id = $this->wc_order->get_item_meta($item_id, '_variation_id', true);
			$product_id = $this->wc_order->get_item_meta($item_id, '_product_id', true);

			$yxml_product = yxml_product($product_id);
			$yxml_product_variation = yxml_product($variation_id);
			// p($yxml_product);
			$qty = $this->wc_order->get_item_meta($item_id, '_qty', true);
			// $total_qty += $qty;

			$cart_item_data = array(
					'ProductId'	=> $yxml_product->get_api_id(),
					'SkuId'		=> $yxml_product_variation->get_sku_id(),
					'Quantity'	=> $qty,
					// 'Price'		=> $this->wc_order->get_item_meta($item_id, '_line_total', true),
					// 'Value'		=> $this->wc_order->get_item_meta($item_id, '_line_total', true),
					// 'TaxPercent'	=> 0
				);
			
			$this->add_product($cart_item_data);

			// foreach ($cart_item_data as $cart_item_data_key => $value):
				// $cart_details_item->addChild("$cart_item_data_key", $value);
			// endforeach;
		endforeach;

		// $order_details = $cart->addChild('TotalQuantity', $total_qty);

	}

	public function set_person() {
		
	}
	*/
	public function add_product( $cart_item_data ) {
		
		$cart = $this->get_root_xml_node();
		$cart_details = isset($cart->CartDetails) ? $cart->CartDetails : $cart->addChild('CartDetails');
		
		$cart_details_item = $cart_details->addChild('CartDetail');

		foreach ($cart_item_data as $cart_item_data_key => $value):
			$cart_details_item->addChild("$cart_item_data_key", $value);
		endforeach;

		$total_qty = 0;

		foreach($cart_details->children() as $cart_detail):
			$cart_detail = (array) $cart_detail;
			$total_qty += (int) $cart_detail['Quantity'];
		endforeach;

		$cart->TotalQuantity = $total_qty;

	}
/*
	public function set_freight( $freight_id = null ) {
		
		$cart = $this->get_root_xml_node();
		$freight_option = isset($cart->SelectedFreightOption) ? $cart->SelectedFreightOption : $cart->addChild('SelectedFreightOption');
 $cart->addChild('PricesIncludeTax', 'true');
		if(!$freight_id):
			$freight_option->Id 	= null;			
			$freight_option->Name 	= null;			
			$freight_option->Valie 	= null;			
		endif;
	}*/

	private function get_config() {	
		return array();
	}
}
