<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
* 
*/
class YXML_Admin_Columns {

	public function __construct( ) {

		$this->init_hooks();

	}


	private function init_hooks() {
		
		add_action( 'restrict_manage_posts', array($this, 'add_custom_taxonomy_filters'), 20, 1 );
		add_action( 'restrict_manage_posts', array($this, 'add_visibility_filter'), 20, 1 );
		// global $wp_filter;
		// p($wp_filter);
		add_filter( 'parse_query', array($this,'add_visibility_tax_query' ));
	}
	/**
	 * if submitted filter by post meta
	 * 
	 * make sure to change META_KEY to the actual meta key
	 * and POST_TYPE to the name of your custom post type
	 * @author Ohad Raz
	 * @param  (wp_query object) $query
	 * 
	 * @return Void
	 */
	public function add_visibility_tax_query( $query ){

	    global $pagenow;

    	// return $query;
    	if(!is_admin() || $pagenow != 'edit.php') return $query;

	    $post_type = (isset($_GET['post_type']) ? $_GET['post_type'] : 'post' );

	    if($post_type != $query->query_vars['post_type']) return $query;
	    
	    $request = $_GET;

		if( $post_type == 'product' ):
			if(isset($request['yxml_visibility']) && $request['yxml_visibility'] == 'visible_in_shop'):

				$tax_query = (isset($query->query_vars['tax_query']) ? $query->query_vars['tax_query'] : array('relation' => 'AND'));
				$tax_query = apply_filters('woocommerce_product_query_tax_query', $tax_query, null);

				$query->query_vars['tax_query'] = $tax_query;
	        endif;
		endif;

		return $query;
	}

	public function add_visibility_filter( $post_type ) {

			if($post_type != 'product') return;

			$field_args = array( 
				'options' => array(
						'any' => __('Any Visibility', 'yoke'),
						'visible_in_shop' => __('Visible In Shop', 'yoke'),
					),
				'value' => (isset( $_GET['yxml_visibility']) ? $_GET['yxml_visibility'] : 'any'),
				);
			
			yxml_field('yxml_visibility', 'select', $field_args);
			echo '&nbsp;';

	}

	public function add_custom_taxonomy_filters( $post_type ) {
		
		$filter_taxonomies = YXML()->tax->admin_taxonomy_filters();

		if(!isset($filter_taxonomies[$post_type])) return;

		foreach ($filter_taxonomies[$post_type] as $key => $filter_taxonomy):

			$labels = get_taxonomy_labels( get_taxonomy($filter_taxonomy) );
			$terms = get_terms( array(
			    'taxonomy' => $filter_taxonomy,
			    'hide_empty' => false,
			) );

			$term_options = array( __('Show all', 'yoke') );
			
			if(!empty($terms)):
				foreach ($terms as $key => $term):
					$term_options[$term->slug] = sprintf( __('%s (%d)', 'yoke'), $term->name, $term->count );
				endforeach;
			endif;

			$field_args = array( 
				'options' => $term_options,
				'value' => (isset( $_GET[$filter_taxonomy]) ? $_GET[$filter_taxonomy] : null),
				'allow_null' => true,
				'placeholder' => __('Show all', 'yoke') //sprintf(__('Filter by %s', 'yoke'), $labels->name ) 
				);
			echo sprintf(__('<label>Filter by %s:</label> ', 'yoke'), $labels->name);
			yxml_field($filter_taxonomy, 'select', $field_args);
			echo '&nbsp;';

		endforeach;

	}

	
	public function add_filter( $options ) {

	}

}