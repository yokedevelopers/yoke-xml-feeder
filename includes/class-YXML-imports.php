<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Imports extends YXML_Post_Factory {

	protected $factory_id	 			= 'import';

	protected $single_class	 		 	= 'YXML_Import';

	protected $post_type	 		 	= 'yxml_import';

	public $importers = array();

	public function __construct() {
		$this->init_hooks();
		parent::__construct();
	}
	
	private function init_hooks() {

		$this->register_importers();

		
		add_action( 'yxml/action/cron_all', array($this, 'on_cron_all'), 10, 2 );
		add_action( 'yxml/action/import_trigger', array($this, 'import_trigger'), 10);	
	
		add_filter( 'yxml_js_init_actions', function( $actions = array() ) {
			$actions['admin_import_post_settings'] = array();
			return $actions;
		} );

		add_action( 'edit_form_after_title', array($this, 'add_settings_meta_box'), 10, 1 );
		add_action( 'save_post', array($this, 'save_post'), 10, 1 );

	}

	public function on_cron_all( $schedule, $original_hook ) {

		if($schedule):
			$posts_ids_for_schedule = $this->get_post_ids_by_schedule($schedule);
			if(!empty($posts_ids_for_schedule)):

				foreach ($posts_ids_for_schedule as $post_id):
					$this->import_trigger(array( 'import_id' => $post_id));
				endforeach;

			endif;
		endif;

		// YXML()->log->log( __CLASS__ .'->'. __FUNCTION__ . '( \'' . $original_hook . '\' )');
	}

	public function get_post_ids_by_schedule( $schedule ) {

		$ids = get_posts( array(
			'meta_key' 		=> '_scheduled',
			'meta_value'	=> $schedule,
			'post_type'		=> $this->get_post_type(),
			'post_status'	=> 'publish',
			'fields'		=> 'ids'

		));

		return $ids;
	}

	private function register_importers() {

		$this->importers = apply_filters('yxml_importers', array(
			'shows'					=>	'YXML_Import_Shows',
		));
	}


	public function get_importers() {
		return $this->importers;
	}

	public function get_importer_list() {

		$importers = array();

		foreach($this->get_importers() as $importer_id => $importer):
			$importers[$importer_id] = $importer::$label;
		endforeach;

		return $importers;

	}

	public function get_import_label( $importer_id ) {

		$importers = $this->get_importer_list();
		return isset($importers[$importer_id]) ? $importers[$importer_id] : null;
	}


	public function get_trigger_hook( $args, $redirect = true ) {

	}

	public function import_trigger( $args, $redirect = true ) {

		$import_trigger = false;

		if(isset($args['import_id'])):

		    $import = $this->get_post_by_id($args['import_id']);
		    
		    if($import):
		    	$import_trigger = $import->trigger();
			endif;

		endif;

		$ref = wp_get_referer();

		if($ref && (!defined('DOING_AJAX') || !DOING_AJAX )):
			wp_redirect( $ref );
			exit;
		endif;

		return $import_trigger;
	}

	public function get_import_trigger_link( $import_id ) {
		$current_uri = urlencode(home_url( add_query_arg( NULL, NULL ) ));    
	    $url = admin_url( '?action=yxml_action&yxml_action=import_trigger&import_id=' . $import_id . '&_wp_http_referer=' . $current_uri);
	    return $url;
	}

	/*public function import($id, $args = array()) {

		$the_importer = $this->get_importer($id);


		if(!$the_importer):

			YXML()->notices->err( sprintf(__('<code>%s</code> is not a registered importer', 'yoke'), $id));
		
		else:			

			do_action( 'yxml_before_import', $the_importer, $args);

			// $the_importer->run();

			do_action( 'yxml_after_import', $the_importer, $args);

		endif;

	}
*/
	public function get_importer( $id ) {
		return $id && isset($this->importers[$id]) ? $this->importers[$id] : false;
	}

	public function add_settings_page($option_settings) {

		// $option_settings['yxml_import_settings'] = array();

		return $option_settings;
	}


	public function add_admin_columns($columns) {

		unset($columns['date']);
	    $columns['_importer_id'] = __( 'Importer','yoke');
	    $columns['_instance_result'] = __( 'Latest Result','yoke');
	    $columns['actions'] = __( 'Actions','yoke');

	    return $columns;
	}

	public function admin_column_content( $column, $post_id )  {

	    switch ( $column ):
	        case '_importer_id':

		        $import = $this->get_post_by_id($post_id);		        
		        echo $this->get_import_label( $import->get_importer_id() );
	            
	            break;
	        case '_instance_result':
	        	
	        	
		        $import = $this->get_post_by_id($post_id);		        
		        
		        if($import && $import_instance = $import->get_latest_instance()):

					$so = get_post_status_object( get_post_status( $import_instance->get_id() ) );
					if($so && $so->label) echo $so->label;		        	
					
		        	$link = get_edit_post_link( $import_instance->get_id() );
		        	echo sprintf(' <a href="%s">View</a>', $link);

		        endif;

	            break;
	        case 'actions':
            	echo sprintf('<a class="button" href="%s">%s</a>', $this->get_import_trigger_link($post_id), __('Run'));

	            break;
	    endswitch;
	}

	public function add_settings_meta_box( $post ) {

		if($post->post_type == $this->get_post_type()):
			$post_id = $post->ID;
			add_meta_box( 
				'yxml_import_settings',
				__('Import Settings', 'yoke'),
				array($this, 'do_post_settings_box')
			);
		endif;

	}

	public function get_meta_box_fields( $post_id = null ) {

		$import = $post_id ? $this->get_post_by_id($post_id) : null;
		$importer = $import ? $import->get_importer() : null;
		
		$fields = array();

		$fields['note'] = array(
				'active' => !$importer ? true : false,
				'type' => 'message',
				'label' => __('<strong>Select an Importer and save this post before continuing.</strong>', 'yoke')
			);

		$fields['_importer_id'] = array(
				'active' => true,
				'label'	=> __('Importer', 'yoke'),
				'type' => $importer ? 'hidden' : 'select', 
				'desc' => $importer ? $importer::$label : null, 
				'options' => $importer ? array() : $this->get_importer_list(),
				'allow_null' => false,
			);


		$fields['_scheduled'] = array(
				'active' => $importer ? true : false,
				'label'	=> __('Schedule', 'yoke'),
				'type' => 'select',
				'default' => 'none',
				'options' => array_merge(
					array('none' => __('Manual', 'yoke')),
					YXML()->cron->get_interval_options()
				),
				'desc' => __('Do you wish to schedule this import?')
			);

		$fields['_results_per_page'] = array(
				'active' => $importer && $importer::$paginate ? true : false,
				'label'	=> __('Results per Page', 'yoke'),
				'desc'	=> __('The number of results to request per API call.', 'yoke'),
				'type'	=> 'number',
				'default' => 40,
				'min' => 1,
				'max'	=> null
			);

		$fields['_time_limit'] = array(
				'active' => $importer ? true : false,
				'label'	=> __('Time Limit (seconds)', 'yoke'),
				'desc'	=> __('The time limit to assign to each iteration of the import. Increase this if you are experiencing timeouts.', 'yoke'),
				'type'	=> 'number',
				'default' => 30,
			);

		$fields['_changes_only'] = array(
				'active' => $importer ? true : false,
				'label'	=> __('Changes Only', 'yoke'),
				'desc'	=> __('Download only changes to the database, by including a timestamp in the request.', 'yoke'),
				'type'	=> 'bool',
				'default' => 1,
			);

		if($import):
			foreach ($fields as $name => &$field):
				$field['value'] = $import->get_post_meta( $name );
			endforeach;
		endif;

		return apply_filters( 'yxml_import_post_meta_box_fields', $fields);
	}

	public function do_post_settings_box( ) {

		global $post;

		$post_id = $post->ID;

		$settings_fields = $this->get_meta_box_fields( $post_id );		

		$scheduled_args = array('import_id' => $post_id);
		$trigger_action = 'yxml/action/import_trigger/' . $post_id;
		// var_dump(wp_next_scheduled($trigger_action));
		// var_dump(wp_get_schedule($trigger_action));

		echo '<table class="form-table"><tbody>';

		foreach ($settings_fields as $name => $field):
			$field = wp_parse_args( $field, array(
				'active' => true,
				'type' => null,
				'label' => '',
				'desc'	=> '',
				'required' => false
			) );

			if(!$field['active']) continue;

			$field_id = 'yxml_import_' . $name;
			$field['id'] = $field_id;

			$type = $field['type'];
			$label = $field['label'];
			$desc = $field['desc'];

			$required = $field['required'];
				
			unset($field['type']);
			unset($field['label']);
			unset($field['desc']);
			unset($field['required']);

			if($type == 'message'):

				echo sprintf(
					'<tr valign="top">
						<td colspan="2"><p>%s</p></td>
					</tr>', 
					$label
				);

			else:

				ob_start();
				yxml_field( $name, $type, $field );
				$field_output = ob_get_clean();

				echo sprintf(
					'<tr valign="top">
						<td><label for="%s">%s</label></td>
						<td>%s%s</td>
					</tr>', 
					$field_id,
					$label . ($required ? '*' : ''),
					$field_output,
					!empty($desc) ? '<p class="yxml-field-desc">' . $desc . '</p>' : ''
				);

			endif;
			
		endforeach;

		echo '</tbody></table>';

		$import = $post_id ? $this->get_post_by_id($post_id) : null;

		// $import->trigger();
	}


	public function save_post( $post_id ) {

		$args = $_POST;

		if(isset($args['post_type']) && $args['post_type'] == $this->get_post_type()):

			foreach ($this->get_meta_box_fields() as $name => $field):
				if($field['type'] == 'message')	continue;
				if(isset($args[$name])):
					update_post_meta( $post_id, $name, $args[$name] );
				elseif(isset( $field['on_null'] )):
					update_post_meta( $post_id, $name, $field['on_null'] );
				endif;
			endforeach;
						
			// $scheduled = get_post_meta( $post_id, '_scheduled', true );
			$scheduled_args = array('import_id' => $post_id);
			$trigger_action = 'yxml/action/import_trigger';
			// $trigger_action = 'yxml/action/import_trigger/' . $post_id;

			wp_clear_scheduled_hook( $trigger_action, array() );
			wp_clear_scheduled_hook( $trigger_action, $scheduled_args );

			if($scheduled == 'none'):
			else:
				// YXML()->cron->schedule_task( time(), $scheduled, $trigger_action, array() );
				// YXML()->cron->schedule_task( time(), $scheduled, $trigger_action, $scheduled_args );
			endif;

			do_action('yxml/action/save_post', $post_id);

		endif;
	}


	public function get_saved_results( $importer_id , $return_rows = array(), $where = array(), $limit = -1 ) {

		wp_die( 'Deprecated function: ' . __CLASS__ . '->' . __FUNCTION__ );
		
		return false;

		// $importer = $this->get_importer($importer_id);
		// if(!$importer || !$importer->has_db_table) return false;
		// return YXML()->db->get_results( $importer->get_db_table_name(), $return_rows, $where, $limit );

	}


}


