<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Import_Instances extends YXML_Post_Factory {

	protected $factory_id	 			= 'import_instance';

	protected $single_class	 		 	= 'YXML_Import_Instance';

	protected $post_type	 		 	= 'yxml_import_instance';

	protected $custom_statuses = array(
		'yxml-incomplete' 	=> array(
								'label' 					=> 'Incomplete',
								'internal' 					=> true,
								'public'					=> false,
								'show_in_admin_all_list' 	=> true,
								'show_in_admin_status_list' => true
							),
		'yxml-abandoned'	=> array(
								'label' 					=> 'Abandoned',
								'internal' 					=> true,
								'public'					=> false,
								'show_in_admin_all_list' 	=> true,
								'show_in_admin_status_list' => true
							),
		'yxml-success' 		=> array(
								'label' 					=> 'Successful',
								'internal' 					=> true,
								'public'					=> false,
								'show_in_admin_all_list' 	=> true,
								'show_in_admin_status_list' => true
							),
		'yxml-failed' 		=> array(
								'label' 					=> 'Failed',
								'internal' 					=> true,
								'public'					=> false,
								'show_in_admin_all_list' 	=> true,
								'show_in_admin_status_list' => true
							)
	);

	public function __construct() {
		$this->init_hooks();
		parent::__construct();
	}
	
	private function init_hooks() {
		add_action( 'edit_form_after_title', array($this, 'add_notes_meta_box'), 10, 1 );

	}

	public function add_admin_columns($columns) {

	    $columns['import_post'] = __( 'Import','yoke');
	    $columns['post_status'] = __( 'Status','yoke');

	    if(isset($columns['date'])):
			$date = $columns['date'];
			unset($columns['date']);
	    	$columns['date'] = $date;
		endif;

	    return $columns;
	}

	public function admin_column_content( $column, $post_id )  {

	    switch ( $column ):
	        case 'post_status':

				$so = get_post_status_object( get_post_status( $post_id ) );
				if($so && $so->label) echo $so->label;
	            break;
	        case 'import_post':
	        	$post_parent_id = wp_get_post_parent_id( $post_id );
	        	$edit_link = get_edit_post_link( $post_parent_id );
	        	echo sprintf(__( '<a href="%s" title="Manage the Import">#%s</a>', 'yoke' ), $edit_link, $post_parent_id);
	            break;
	    endswitch;
	}

	public function add_notes_meta_box( $post ) {

		if($post->post_type == $this->get_post_type()):
			$post_id = $post->ID;
			add_meta_box( 
				'yxml_import_instance_notes',
				__('Import Instance Notes', 'yoke'),
				array($this, 'do_post_notes_box')
			);
		endif;

	}
	public function do_post_notes_box( ) {

		global $post;

		$post_id = $post->ID;

		$instance = $this->get_post_by_id($post_id);

		$notes = $instance->get_instance_notes();

		$notes = array_reverse($notes);
		
		echo '<table class="form-table"><tbody>';

		foreach($instance->get_instance_data() as $key => $value):
				
				switch ($key):
					case 'data_complete':
					case 'import_complete':
						$value = $value ? 'Yes' : 'No';
						break;
					default:
						break;
				endswitch;

				echo sprintf(
					'<tr valign="top">
						<td>%s</td>
						<td>%s</td>
						<td></td>
					</tr>', 
					ucfirst(str_replace('_', ' ', $key)),
					$value
				);
			
		endforeach;

	
		foreach ($notes as $key => $note):
		
				echo sprintf(
					'<tr valign="top">
						<td><date>%s</date></td>
						<td>%s</td>
						<td><p class="yxml-note yxml-note-%s">%s</p></td>
					</tr>', 
					ucwords($note['type']),
					$note['time'],
					$note['type'],
					$note['note']

				);
			
		endforeach;

		echo '</tbody></table>';
	}
}