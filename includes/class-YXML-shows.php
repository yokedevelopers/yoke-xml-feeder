<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Shows extends YXML_Post_Factory {

	protected $factory_id	 			= 'show';

	protected $single_class	 		 	= 'YXML_Show';

	protected $post_type	 		 	= 'event';

	protected $import_object_type	 	= 'show';

	public function __construct() {
		$this->init_hooks();
		parent::__construct();
	}
	
	private function init_hooks() {
		// add_filter('yxml_post_types', array($this, 'register_post_type'));
	}

	public function register_post_type($post_types) {
		
		$post_types['event'] = array(
					'labels' => array(
						'name' => __('Events', 'yoke'),
						'singular_name' => __('Event', 'yoke')
					),								
					'public' => true,
					'show_ui' => true,
					'has_archive' => true,
					'supports' => array('title', 'editor', 'custom-fields'),
					'menu_icon'   => 'dashicons-location',
				);

		return $post_types;
	}

	
}
