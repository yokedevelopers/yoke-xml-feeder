<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *  Put Load - Abstract
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Permissions {

	
	public function __construct() {
		$this->init_hooks();
	}

	public function init_hooks() {		
	}

	public function current_user_can( $capability ) {

		if(!is_user_logged_in()) return false;

		return current_user_can( $capability );
	}

	public function current_user_can_manage_shop() {
		return $this->current_user_can( 'manage_woocommerce' );
	}

	public function current_user_can_import() {
		return $this->current_user_can( 'manage_woocommerce' );		
	}
}
