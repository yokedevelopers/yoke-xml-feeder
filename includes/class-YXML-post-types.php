<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Post_Types {

	public function __construct( ) {
		$this->init_hooks();
	}

	private function init_hooks() {

		add_action( 'yxml_before_init', array( $this, 'register_post_statuses' ), 1 );
		add_action( 'yxml_before_init', array( $this, 'register_post_types' ), 5 );
		
	}

	public function init() {}

	public function clean_post_status( $status ) {
		$status = trim($status);
		return strpos($status, 'yxml-') !== 0 ? 'yxml-' . $status : $status;
	}

	public function register_post_statuses() {

		foreach (apply_filters('yxml_custom_post_statuses', array()) as $status => $settings):
			
			$status = $this->clean_post_status($status);
			
			$settings = wp_parse_args( $settings, array(
				'label' => $status,
				'protected' => true
			) );

			if(!isset($settings['label_count'])) $settings['label_count'] =  _n_noop( $settings['label'] . ' <span class="count">(%s)</span>', $settings['label'] . ' <span class="count">(%s)</span>' );

			register_post_status( $status, $settings );
		endforeach;

	}

	public function register_post_types() {

		$post_types = apply_filters('yxml_post_types', array(

			'yxml_import' => array(
								'labels' => array(
									'name' => __('Imports', 'yoke'),
									'singular_name' => __('Import', 'yoke')
								),								
								'public' => false,
								'show_ui' => true,
								'has_archive' => false,
								'supports' => array('title'),
								'menu_position' => 57,
								'menu_icon'   => 'dashicons-download',
							),
			'yxml_import_instance' => array(
								'labels' => array(
									'name' => __('Import Instances', 'yoke'),
									'singular_name' => __('Import Instance', 'yoke')
								),								
								'public' => false,
								'show_ui' => true,
								'has_archive' => false,
								'supports' => array('title'),
								'menu_position' => 57,
								'menu_icon'   => 'dashicons-info',
							)
			));
		
		foreach ($post_types as $post_type => $post_type_args):

			if(post_type_exists( $post_type )) continue;

			$post_type_label = isset($post_type_args['labels']['name']) ? $post_type_args['labels']['name'] : $post_type;
			$post_type_label_singular = isset($post_type_args['labels']['singular_name']) ? $post_type_args['labels']['singular_name'] : $post_type;

			$post_type_args['labels'] = wp_parse_args( $post_type_args['labels'], array(
					'singular_name'     => $post_type_label_singular,
					'search_items'      => sprintf(__( 'Search %s', 'yoke' ), $post_type_label ),
					'all_items'         => sprintf(__( 'All %s', 'yoke' ), $post_type_label ),
					'edit_item'         => sprintf(__( 'Edit %s', 'yoke' ), $post_type_label_singular ),
					'update_item'       => sprintf(__( 'Update %s', 'yoke' ), $post_type_label_singular ),
					'add_new_item'      => sprintf(__( 'Add New %s', 'yoke' ), $post_type_label_singular ),
					'new_item_name'     => sprintf(__( 'New %s Name', 'yoke' ), $post_type_label_singular ),
					'menu_name'         => $post_type_label,
				) );

			register_post_type($post_type, $post_type_args);
		
		endforeach;

		do_action( 'yxml_post_types_registered' );

	}

}