<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
* 
*/



class YXML_Taxonomies {

	public		$post_type = null;
	private		$tax_reference = null;
	private		$term_reference = null;
	public		$admin_taxonomy_filters = array();

	public function __construct( ) {

		// $this->post_type = array('product');
		// $this->init_hooks();

	}


	private function init_hooks() {
		
		add_filter( 'yxml_database_config', array($this, 'add_db_config'), 5);
		add_filter( 'yxml_create_taxonomy_name', array($this, 'sanitize_taxonomy_name'), 10, 1);

		add_action( 'yxml_init', array( $this, 'register_taxonomies' ), 5 );
		add_action( 'pre_delete_term', array( $this, 'prevent_term_delete' ), 5, 2 );
		
	}
	
	public function term_is_api_reference( $term_id, $taxonomy = null ) {
		return $this->get_reference_id_by_term_id( $term_id, $taxonomy );
	}

	public function prevent_term_delete( $term_id, $taxonomy = null ) {

		if($this->term_is_api_reference( $term_id, $taxonomy )):
			$term = get_term( $term_id, $taxonomy );
			// var_dump($term);
			$msg = sprintf(__('The term \'%s\' cannot be deleted, as it is an Yoke XML reference.', 'yoke'), $term->name);
			
			YXML()->notices->err($msg, true);
			wp_die( $msg );

		endif;

	}

	private function init() { }

	public function add_db_config( $config ) {

		$tables = array(
				'terms_reference'
			);

		foreach ($tables as $key => $table):

			$config[$table] = array(
				'id'		=> array(
									'type' 			=> 'mediumint(9)',
									'flags'			=> array('NOT NULL'),
									'primary_key'	=> 1
								),
				'term_id'	=> array(
									'type' 			=> 'mediumint(9)',
									'flags'			=> array('NOT NULL'),
								)
			);
		endforeach;

		return $config;
	}

	public function register_taxonomies() {

		$reference_types = YXML()->references->get_reference_types(array(
			'is_tax' => 1,
			'order_by' => 'sequence',
			'order'	=> 'ASC'
			));

		if(!empty($reference_types)):

			foreach($reference_types as $reference_type):
			
				if(!$reference_type->is_tax) continue;

				if(!property_exists($reference_type, 'taxonomy') || empty($reference_type->taxonomy)):
					$taxonomy = apply_filters( 'yxml_create_taxonomy_name', $reference_type->code );
				else:
					$taxonomy = $reference_type->taxonomy;
				endif;
				
				$name = $reference_type->name;
				$post_types = $reference_type->post_types;

				if(empty($post_types)) $post_types = $this->post_type;
				if(!is_array($post_types)) $post_types = array($post_types);
				
				$saved_tax_options = YXML()->references->filter_tax_options( (is_array($reference_type->tax_options) ? $reference_type->tax_options : array()) );

				$labels = array(
					'name' => $name
				);



				$tax_options = array_merge(array(
					'hierarchical' => true,
					'labels' => $labels
					// 'rewrite' => array('slug' => 'schools', 'with_front' => true),
				), $saved_tax_options );

				if(isset($tax_options['rewrite'])):
					$rewrite = array(
						'slug' => $tax_options['rewrite']
						);
					if(isset($tax_options['with_front'])):
						$rewrite['with_front'] = $tax_options['with_front'];
						unset($tax_options['with_front']);
					endif;

					$tax_options['rewrite'] = $rewrite;

					$this->add_taxonomy_archive( $taxonomy, $rewrite['slug'] );
					
				endif;

				if( isset($reference_type->tax_options['add_admin_filter']) && $reference_type->tax_options['add_admin_filter'] ):
					foreach ($post_types as $post_type):
						$this->admin_taxonomy_filters[$post_type][] = $taxonomy;
					endforeach;

					unset($reference_type->tax_options['add_admin_filter']);

				endif;

				register_taxonomy($taxonomy, $post_types, $tax_options);

				add_filter( 'manage_edit-' . $taxonomy . '_columns', array($this, 'filter_taxonomy_edit_columns') );
				add_filter( 'manage_' . $taxonomy . '_custom_column', array($this, 'filter_taxonomy_custom_columns'), 10, 3 );

			endforeach;

		endif;

	}

	public function filter_taxonomy_edit_columns( $columns ) {

	    $columns['yxml_term'] = 'Yoke XML';
	    return $columns;
	}   

	public function filter_taxonomy_custom_columns( $value, $name, $term_id ) {

	    if( $name == 'yxml_term'):
	    	if(	$this->term_is_api_reference( $term_id ) ):
	    		$note = __('This term is an Yoke XML Reference.', 'yoke');
	    		$icon = 'dashicons-yes';
	    	else:

	    		$note = __('This term is not an Yoke XML Reference.', 'yoke');
	    		$icon = 'dashicons-no';

	    	endif;

	    	$value = sprintf('<span class="num" title="%s"><span class="dashicons %s"></span></span>', $note, $icon);
	    
	    endif;

	    return $value;
	}

	public function admin_taxonomy_filters() {
		return apply_filters('yxml_admin_taxonomy_filters', $this->admin_taxonomy_filters);
	}

	public function tax_reference( ) {
		
		if(is_null($this->tax_reference)):

			$tax_reference = array();
			
			$saved_types = YXML()->references->get_reference_types();

			foreach ($saved_types as $key => $reference_type):
			
				if(!$reference_type->is_tax) continue;

				$taxonomy = ( property_exists($reference_type, 'taxonomy') && !empty($reference_type->taxonomy) ? $reference_type->taxonomy : apply_filters( 'yxml_create_taxonomy_name', $reference_type->code ) );
			
				$tax_reference[$reference_type->id] = $taxonomy;

			endforeach;

			$this->tax_reference = $tax_reference;

		endif;

		return  $this->tax_reference;

	}

	public function term_reference( $type = 'reference' ) {
		
		if(is_null($this->term_reference) || !isset($this->term_reference[$type])):

			// $id_results = YXML()->db->get_results( 'terms_' . $type );
			$id_results = YXML()->db->get_results( 'references', array('id', 'term_id'), array('RELATION' => 'NOT IN', 'term_id' => '0'));

			$term_reference = array();

			if(is_array($id_results) && !empty($id_results)):
				foreach ($id_results as $key => $term_row):
					$term_reference[$term_row->id] = $term_row->term_id;
				endforeach;
			endif;
			$this->term_reference[$type] = $term_reference;
		endif;
		
		return (isset($this->term_reference[$type]) ? $this->term_reference[$type] : array());
	}

	public function get_term_pair( $ref_type_id, $ref_id ) {

		$taxonomy = $this->get_taxonomy_by_reference_type_id( $ref_type_id );

		$term_id = $this->get_term_id_by_reference_id( $ref_id );

		return ($taxonomy && $term_id ? array('taxonomy' => $taxonomy, 'term_id' => $term_id) : null);
		
	}

	public function get_taxonomy_by_reference_type_id( $ref_type_id = null ) {

		$ref_type_id = (int) $ref_type_id;
		$tax_reference = $this->tax_reference();

		return (isset($tax_reference[$ref_type_id]) ? $tax_reference[$ref_type_id] : null);

	}

	public function get_term_id_by_reference_id( $ref_id ) {
		
		$ref_id = (int) $ref_id;
		$term_reference = $this->term_reference();
		return (isset($term_reference[$ref_id]) ? $term_reference[$ref_id] : null);
		
	}

	public function get_reference_id_by_term_id( $term_id, $taxonomy = null ) {
		
		$term_id = absint($term_id);
		$term_reference = $this->term_reference();
		$term_reference = array_flip($term_reference);

		return (isset($term_reference[$term_id]) ? $term_reference[$term_id] : null);
		
	}

	public function sanitize_taxonomy_name( $str ) {
		return	'_' . sanitize_title($str);
	}

	/**
	* Insert the term as wp_term, and update database term references
	* @var $item (object) Imported reference
	* @var $type (string) Type of reference
	* @return term|WP_Error
	*/
	public function insert_term( $item ) { //, $type = 'reference' ) {
						
		$taxonomy = $this->get_taxonomy_by_reference_type_id( $item['ReferenceTypeId'] );

		if(!$taxonomy) return false; 

		$term_name = $item['Name'];
	
		$o_term_id = $this->get_term_id_by_reference_id( $item['Id'] );


		if($o_term_id):// && term_exists( $o_term_id, $taxonomy )):
			$term = wp_update_term( $o_term_id, $taxonomy, array('name' => $term_name) );
		else:
			$term = wp_insert_term( $term_name, $taxonomy, $args = array() );
		endif;

		if(!$o_term_id):

			$term_id = (
				is_wp_error( $term ) && isset($term->error_data['term_exists'])
				? $term->error_data['term_exists']
				: $term['term_id']
				);

			return $term_id;

		elseif(is_wp_error( $term )):
			$term = false;
			// $term = get_term_by('name', $term_name, $taxonomy );
		// var_dump( $o_term_id, $taxonomy, array('name' => $term_name) );
			// var_dump($term);
		endif;

		return $term;

	}

	public function add_taxonomy_archive( $taxonomy, $slug ) {
		
	}
}


