<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Form_Field {

	public $name			= null;
	public $id				= null;
	public $type			= null;
	public $options			= null;
	public $value			= null;
	public $placeholder		= null;
	public $default			= null;
	public $attrs			= array();
	public $classes			= array();
	public $label_wrap		= null;
	public $option_wrap		= 'p';
	public $null_default	= false;
	public $allow_null		= true;

	public function __construct( $name, $type = 'text', $args = array() ) {

		$this->filter_settings( $type, $args );

		$this->set( 'name', $name );
		$this->set( 'type', $type );

		if(!empty($args)):
			foreach ($args as $key => $value):
				if(property_exists($this, $key)):
					$this->set( $key, $value );
				else:
					$this->attrs[$key] = $value;
				endif;
			endforeach;
		endif;

	}

	public function filter_settings( &$type, &$args ) {

		switch ($type):
			case 'yes_no':
				$type = 'select';
				$args['options'] = array('1' => 'Yes', '0' => 'No');
				break;

			case 'bool':
				$type = 'checkbox';
				$args['null_default'] = true;
				$args['options'] = array('1' => '');
				$args['value'] = (isset($args['value']) && $args['value'] ? 1 : 0);
				break;

			case 'post_types':
				$type = 'select';
				$args['options'] = get_post_types();
				break;
			
			default:
				break;
		endswitch;

		if($type == 'select' && !isset($args['placeholder'])) $args['placeholder'] = __('Please select...', 'yoke');

	}

	public function set( $key, $value = null ) {
		$this->$key = apply_filters('yxml_field_' . $key, $value );
	}

	public function get( $key ) {
		return ( property_exists($this, $key) ? $this->$key : null );
	}

	public function get_type() {
		return ($this->type ?: 'text');
	}

	public function get_id() {
		return esc_attr(($this->id ?: $this->name));
	}

	public function get_name() {

		$name = $this->name;

		if($this->get_type() == 'select' && $this->has_attr('multiple')):
			$name .= (substr($name, -2) != '[]' ? '[]' : '');
		endif;

		return $name;
	}

	public function get_options() {

		$options = array();

		if(!empty($this->options)):
			$options = $this->options;
			if(!is_array($this->options)) $options = array( (string) $this->options => (string) $this->options );
		endif;

		return $options;
	}

	public function get_value() {
		return (!is_null($this->value) ? $this->value : (!is_null($this->default) ? $this->default : null));
	}

	public function is_current_value( $value ) {
		
		$current_value = $this->get_value();
		return ($current_value == $value || is_array($current_value) && in_array($value, $current_value));

	}

	public function get_classes() {

		$classes = (!empty($this->classes) ? (is_array($this->classes) ? $this->classes : explode(' ', $this->classes)) : array());

		$classes[] = 'yxml-input';
		$classes[] = 'yxml-input-' . $this->get_type();

		return array_unique($classes);
	}

	public function get_classes_str() {
		return implode(' ', $this->get_classes());
	}

	public function has_attr( $attr ) {
		$attrs = $this->get('attrs');
		return ($attrs && ( (isset($attrs[$attr]) && $attrs[$attr]) || in_array($attr, $attrs, true)));
	}

	public function get_attrs_str() {

		if(empty($this->attrs)) return '';

		$attrs_str = '';

		foreach ($this->attrs as $name => $value):
			if(is_string($name)):
				$attr_name = $name;
				$attr_value = $value;
			else:
				$attr_name = $value;
				$attr_value = $value;
			endif;
			$attrs_str .= sprintf(' %s="%s" ', $attr_name, $attr_value);
		endforeach;

		return $attrs_str;
	}

	public function get_options_output( $options = array(), $depth = 0 ) {

		if(empty($options)) return null;

		$output = '';

		if($depth == 0 && !$this->has_attr('multiple')): 
			$placeholder = $this->get('placeholder');
			if($placeholder):
				$output .= sprintf('<option value="" %s %s >%s</option>', 
					($this->is_current_value(null) ? ' selected="selected" ' : ''),
					(!$this->get('allow_null') ? 'disabled hidden' : ''),
					$this->get('placeholder')
				);
			elseif($this->get('allow_null')):
				$output .= '<option value=""></option>';
			endif;
		endif;

		foreach ($options as $option_name => $option_data):

			$is_optgroup = is_array($option_data);

			if($is_optgroup):

				$optgroup_options_arrays = array_filter($option_data, 'is_array');
				
				$optgroup_option_output = '';
				
				foreach ($optgroup_options_arrays as $optgroup_options_key => $optgroup_options):
					$optgroup_option_output .= $this->get_options_output( $optgroup_options, $depth+1 );
				endforeach;

				$optgroup_label = (isset($option_data['label']) ? $option_data['label'] : $option_name);

				if(!$optgroup_option_output && !$optgroup_label) continue;

				$output .= sprintf('<optgroup label="%s">%s</optgroup>', $optgroup_label, $optgroup_option_output);

			else:
				
				$is_selected = $this->is_current_value( $option_name );

				$output .= sprintf('<option value="%s" %s>%s</option>', $option_name, ($is_selected ? ' selected="selected" ' : ''), $option_data);

			endif;

		endforeach;

		return $output;
	}
	
	public function output() { 

		$output = '';

		$field_var = $field_name = $this->get_name();

		if($this->null_default):
			$output = sprintf('<input id="%s" type="hidden" name="%s" value="0">',
					$this->get_id() . '_hidden',
					$this->get_name()
				);
		endif;

		switch ($this->get_type()):
			case 'select':

				$output .= sprintf(
					'<select id="%s" name="%s" class="%s" %s placeholder="%s">%s</select>',
					$this->get_id(),
					$this->get_name(),
					$this->get_classes_str(),
					$this->get_attrs_str(),
					$this->get('placeholder'),
					$this->get_options_output( $this->get_options() )
					);

				break;
			case 'radio':
			case 'checkbox':
				$check_n = 0;
				foreach($this->get_options() as $option_key => $option_label):
					
					$is_selected = $this->is_current_value( $option_key );

					$item_output = sprintf(
						'<input type="%s" id="%s" name="%s" value="%s" class="%s" %s %s placeholder="%s">%s</',
						$this->get_type(),
						$this->get_id() . '_' . $check_n,
						$this->get_name(),
						$option_key,
						$this->get_classes_str(),
						($is_selected ? ' checked="checked" ' : ''),
						$this->get_attrs_str(),
						$this->get('placeholder'),
						$option_label
					);

					$option_wrap = $this->get('option_wrap');
					if($option_wrap !== false ):
						$option_wrap = ($option_wrap ?: 'p');
						
						$item_output = sprintf('<%s>%s</%s>', $option_wrap, $item_output, $option_wrap);

					endif;
					
					$output .= $item_output;

					$check_n++;
				endforeach;
				break;

			case 'textarea':

				$output .= sprintf(
					'<textarea id="%s" name="%s" class="%s" %s placeholder="%s">%s</textarea>',
					$this->get_id(),
					$this->get_name(),
					$this->get_classes_str(),
					$this->get_attrs_str(),
					$this->get('placeholder'),
					$this->get_value()
					);

				break;
			
			case 'text':
			default:
				
				if(!isset($this->attrs['style'])):
					$this->attrs['style'] = 'min-width: 300px;';
				endif;

				$output .= sprintf(
					'<input type="%s" id="%s" name="%s" value="%s" class="%s" placeholder="%s" %s>',
					$this->get_type(),
					$this->get_id(),
					$this->get_name(),
					htmlspecialchars( $this->get_value() ),
					$this->get_classes_str(),
					$this->get('placeholder'),
					$this->get_attrs_str()
					);

				break;
		endswitch;


		return $output;

	}

}

function yxml_field() {

	$args = func_get_args();

	$ref = new ReflectionClass('YXML_Form_Field');

  	$field = $ref->newInstanceArgs($args);

	echo $field->output();
}