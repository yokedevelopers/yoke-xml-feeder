<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *
 * This class creates and maintains the plugin database
 *
 * @since      	1.0.0
 * @package    	YXML
 * @subpackage 	YXML/includes
 * @link 		https://codex.wordpress.org/Creating_Tables_with_Plugins
 */

class YXML_DB_Manager {

	/**
	 * Version of database structure
	 *
	 * @since    1.0.0
	 */

	public $db_version = 1.0;

	/**
	 * Prefix for all tables created by plugin
	 *
	 * @since    1.0.0
	 */
	public $prefix;

	/**
	 * Initialize the connection settings.
	 *
	 * @since    1.0.0
	 */
	public function __construct( ) {

		$this->init_hooks();

	}

	
	private function init_hooks() {
		// $this->init();
		add_action( 'yxml_before_init', array($this, 'init'), 2);
	}

	public function init() {

		do_action( 'yxml_before_db_init' );

		// delete_option( YXML_OPTION_PREFIX . 'db_version');

		global $wpdb;
		$this->prefix = $wpdb->prefix . YXML_DB_PREFIX;

		$db_config = $this->get_db_config();
		// p($db_config);

		$current_db_version = get_option( YXML_OPTION_PREFIX . 'db_version');
		
		if(empty($current_db_version)):
			if(is_admin() && !defined('DOING_AJAX')):
				$this->create_db();
			endif;
		elseif( $current_db_version < $this->db_version):
	 		YXML()->notices->err( sprintf(__( 'Your <strong>%s</strong> database needs to be updated.', 'yoke' ), YXML_PLUGIN_TITLE));
			if(is_admin() && !defined('DOING_AJAX')):
				$this->create_db();
			endif;
		elseif($current_db_version > $this->db_version):
	 		YXML()->notices->err( sprintf(__( 'You seem to have a newer version of the <strong>%s</strong> database.', 'yoke' ), YXML_PLUGIN_TITLE), true );
		else:
	 		// YXML()->notices->msg( sprintf(__( 'Your <strong>%s</strong> database is up to date.', 'yoke' ), YXML_PLUGIN_TITLE), true );
		endif;

		do_action( 'yxml_db_init' );
	}


	private function create_db() {
	
		// delete_option( YXML_OPTION_PREFIX . 'db_version');
		
		$db_config = $this->get_db_config();

		foreach ($db_config as $table_name => $table_settings):

			$table_created = $this->create_table($table_name, $table_settings);

		endforeach;

		add_option( YXML_OPTION_PREFIX . 'db_version', $this->db_version );

	 	YXML()->notices->msg( sprintf(__( 'Your <strong>%s</strong> database was successfully updated', 'yoke' ), YXML_PLUGIN_TITLE), true );
	}

	private function create_table($table_name, $table_settings) {

		$table_cols_sql = $this->build_table_sql($table_settings);

		global $wpdb;

		$table_name = $this->get_table_name($table_name); 

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			$table_cols_sql
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		return dbDelta( $sql );
	}

	public function get_db_config() {

		$config = array();
			
		foreach (apply_filters('yxml_database_config', array()) as $table_name => $table_cols):

			$primary_key = 'id';

			foreach ($table_cols as $table_col_name => $table_col):
					
				$type = (isset($table_col['type']) ? $table_col['type'] : 'text');

				$is_primary = (isset($table_col['primary_key']) && $table_col['primary_key']);

				if($is_primary):
					unset($table_col['primary_key']);
					$primary_key = $table_col_name;
				endif;

				if(isset($table_col['serialize']) && $table_col['serialize']):
					add_filter( 'yxml_db_field_data_in_' . $table_name . '_' . $table_col_name, 'maybe_serialize' );
					add_filter( 'yxml_db_field_data_out_' . $table_name . '_' . $table_col_name, 'maybe_unserialize' );
				endif;

				if(isset($table_col['filter_in'])) add_filter( 'yxml_db_field_data_in_' . $table_name . '_' . $table_col_name, $table_col['filter_in'] );
				if(isset($table_col['filter_out'])) add_filter( 'yxml_db_field_data_out_' . $table_name . '_' . $table_col_name, $table_col['filter_out'] );

			endforeach;

			if(!isset($table_cols[$primary_key])):
				$table_cols = array_merge(
					array(
						$primary_key	=> array(
											'type' 			=> 'mediumint(9)',
											'flags'			=> array('NOT NULL', 'AUTO_INCREMENT')
										)
						),
					$cols
				);
			endif;		

			$config[$table_name] = array(
				'columns' => $table_cols,
				'primary_key' => $primary_key
				);
			
		endforeach;

		return $config;
	}


	public function build_table_sql( $table_settings = array() ) {

		$table_sql_arr = array();		

		$primary_key = $table_settings['primary_key'];

		foreach ($table_settings['columns'] as $table_col_name => $table_col):
				
			$type = (isset($table_col['type']) ? $table_col['type'] : 'text');
			
			$col_sql = $table_col_name . ' ' . $type;

			if(isset($table_col['default'])):
				$col_sql .= ' DEFAULT ' . '\'' . $table_col['default'] . '\'';
			endif;

			if(isset($table_col['flags'])):

				$flags = (is_array($table_col['flags']) ? implode(' ', $table_col['flags']) : $table_col['flags']);
				$col_sql .= ' ' . $flags;

			endif;

			$table_sql_arr[] = $col_sql;

		endforeach;
		
		$table_sql_arr[] = 'PRIMARY KEY  (' . $primary_key . ')';

		$table_sql = implode(',' . PHP_EOL, $table_sql_arr);

		return $table_sql;

	}

	public function get_table_primary_key( $table_name ) {

		$db_config = $this->get_db_config();
		return (isset($db_config[$table_name]) ? $db_config[$table_name]['primary_key'] : null);

	}

	/**
	* Insert (or Update on duplicate key ) a record into to database
	* @var $table_name (string) unprefixed name of the db table to modify
	* @var $data (mixed array|object) 
	*/

	public function insert($table_name, $data = array()) {

		global $wpdb;

		$config = $this->get_db_config();
		
		if(!isset($config[$table_name])) return false;

		$table_config = $config[$table_name];

		$table =  $this->get_table_name($table_name);

		$data = array_change_key_case((array) $data, CASE_LOWER);

		$filtered_data = array();

		foreach ($table_config['columns'] as $col_name => $col):

			$data_key = (isset($col['import_alias']) ? $col['import_alias'] : $col_name);
			$data_key = apply_filters('yxml_db_table_col_key', $data_key, $table_name );
			
			$col_val = null;
			
			if(isset($data[$data_key])):
				
				$col_val = $data[$data_key];
				$col_val = apply_filters('yxml_db_field_data_in_' . $table_name . '_' . $col_name, $col_val );

				$filtered_data[$col_name] = addslashes($col_val);

			endif;


		endforeach;
		
		$filtered_data = apply_filters( 'yxml_db_row_data_in_' . $table_name, $filtered_data );

		$primary_key = $this->get_table_primary_key($table_name);

		if(isset($filtered_data[$primary_key])):
			$where = array( $primary_key => $filtered_data[$primary_key]);
		endif;
		
		$insert_keys = array_keys($filtered_data);
		$insert_values = array_values($filtered_data);

		$update_arr = array();

		foreach ($filtered_data as $key => $value):
			
			if($key == $primary_key) continue;
			
			$update_arr[] = sprintf("$key = '%s'", $value);
			
		endforeach;

		$insert_keys_string = implode( ', ', array_keys($filtered_data));
		$insert_values_placeholders = implode( ', ', array_fill( 0, count( $filtered_data ), '\'%s\'' ) );

		$insert_values_string = vsprintf($insert_values_placeholders, array_values($filtered_data));

		$update_string = implode( ', ', $update_arr );

		$sql = "INSERT INTO $table ($insert_keys_string) VALUES ($insert_values_string) ON DUPLICATE KEY UPDATE $update_string";

		$result = $wpdb->query($sql);

		return $result;

	}

	public function build_where_str( $where = array() ) {

		$where_str = '1=1';

		$relation = 'AND';

		if(!empty($where)):
			
			$where_arr = array();
			
			if(isset( $where['RELATION'] )):
				$relation = $where['RELATION'];
				unset($where['RELATION']);
			endif;

			foreach($where as $field => $value):

				if($relation == 'IN' || $relation == 'NOT IN'):
					
					if(is_array($value)):
						$in_placeholders = implode(', ', array_fill('0', count($value), '%s') );
						$value_str = vsprintf(" $in_placeholders ", $value);
					else:
						$value_str = $value;
					endif;
					
					$where_str = vsprintf(" $field $relation ($value_str)", $value);
					// var_dump($where_str);
					// die();

				else:
					if( is_array($value) ):
						$where_arr[] = "(" . $this->build_where_str($value) . ")";
					else:
						$where_arr[] = "$field = '$value'";
					endif;
				
					$where_str = implode(' ' . $relation . ' ', $where_arr);

				endif;
			endforeach;

		endif;

		return $where_str;
	}

	public function get_results($table_name, $return_cols = array(), $where = array(), $limit = -1, $order_by = null, $order = 'ASC') {

		global $wpdb;

		$table = $this->get_table_name($table_name);
		
		
		$return_cols_str = (is_string($return_cols) ? $return_cols : (!empty($return_cols) ? implode(', ', $return_cols) : '*'));
		
		$where_str = $this->build_where_str( $where );
		// var_dump($where_str);
		$limit_str = '';

		if($limit > 0):
			$limit_str = sprintf('LIMIT %d', $limit);
		endif;
		
		$order_by_str = '';

		if($order_by):
			$order_by_str = sprintf("ORDER BY %s $order", $order_by);
		endif;
		
		$sql = "SELECT $return_cols_str FROM $table WHERE $where_str $limit_str $order_by_str";

        $results = $wpdb->get_results( $sql );

        $return = array();

        foreach ($results as $row_n => &$db_result_row):

			foreach ($db_result_row as $col_name => &$value):
				$value = apply_filters('yxml_db_field_data_out_' . $table_name . '_' . $col_name, $value );
			endforeach;
			
			$db_result_row = apply_filters( 'yxml_db_row_data_out_' . $table_name, $db_result_row );

        endforeach;


		return $results;
	}


	public function prepare_db_result( $db_result_row ) {

		$db_result_row = (array) $db_result_row;


		return $item;
	}
	
	public function get_table_name($table_name) {
		return $this->prefix . $table_name;
	}
}

		