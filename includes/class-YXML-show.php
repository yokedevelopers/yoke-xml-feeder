<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



class YXML_Show extends YXML_Post {

	protected $object_type	 			= 'show';

	protected $import_object_type	 	= 'Show';
	
	protected $post_type	 		 	= 'event';

	protected $unique_meta_key 		 	= 'show_id';

	protected $unique_import_item_key 	= '@ShoID';

	protected $default_post_status	 	= 'publish';

	protected $variations 				= array();

	public function get_import_field_map() {
		return array(
			'@ShoFullName' 					=> array('post' => 'post_title'),
			'Season|@SeaDescription' 		=> array('post' => 'post_content'),
			// 'Season|@VenName'				=> array('meta' => 'event_venue'),
			// 'Season|Performance|@PerTime'	=> array('meta' => 'perf_time'),
		);
	}

	public function before_save() {

		$this->set_post_meta('event_venue', $this->get_api_item_value(array('Season', '@VenName')));
		$this->set_post_meta('event_sub_title', $this->get_api_item_value(array('Season', '@SeaName')));
		$this->set_post_meta('enta_show_id', $this->get_api_item_value(array('@ShoID')));
		$this->set_post_meta('enta_venue_id', $this->get_api_item_value(array('Season', '@VenID')));

	}

	public function after_save() {

		$post_id = $this->get_id();


		if($post_id):
		endif;
	}



	public function get_api_id() {
		return $this->get_unique_meta_value();
	}

}

function yxml_show( $id = null ) {

	$post_type = get_post_type($id);
	switch ($post_type):
		case 'show':
			return new YXML_Show( $id );			
			break;
	endswitch;
	
	return false;
}
