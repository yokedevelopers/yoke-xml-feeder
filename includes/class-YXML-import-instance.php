<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class YXML_Import_Instance extends YXML_Post {

	protected $object_type	 			= 'import_instance';
	
	protected $post_type	 		 	= 'yxml_import_instance';

	protected $supports 				= array();

	protected $import_id 				= null;

	protected $default_post_status 		= 'yxml-incomplete';

	protected $raw_args 				= array();
	protected $args 	 				= array();

	protected $importer 	 			= null;

	public function __construct( $id = null, $parent_id = null, $args = array() ) {

		parent::__construct( $id );
		
		if(!$this->get_id()):

			$this->set_post_status();
			$this->set_post_parent( $parent_id );
			$this->set_post_title();
			$this->set_args( $args );

		endif;

	}

	public function set_args( $args ) {
		
		$this->set_args_hash( $args );

		$this->raw_args = $args; 

		$args = wp_parse_args($args, array(
			'results_per_page' => null,
			'time_limit' => null,
			'changes_only' => null,
			'importer_class' => null
		));

		$this->args = $args;
	
	}

	public function get_args( ) {
		return $this->args ?: array();
	}

	public function get_arg( $key, $default = null ) {
		return isset($this->get_args()[$key]) ? $this->get_args()[$key] : $default;
	}

	public function set_post_parent( $parent_id ) {
		if(is_numeric($parent_id) && $parent_id):
			$this->set_post_data('post_parent', absint($parent_id));
		endif;
	}

	public function get_post_parent() {
		return $this->get_post_data('post_parent');
	}

	public function set_post_title() {
		
		$parent_id = $this->get_post_parent();

		if(!$parent_id) return false;

		$post_title = sprintf('%s %s', get_the_title($parent_id), $this->get_timestamp( 'Y/m/d H:i:sa' ));
		$this->set_post_data('post_title', $post_title );

		return $post_title;
	}

	public function get_time_started( $format = null ) {
		$post_date = $this->get_post_data('post_date');
		return is_null($format) ? $post_date : date($format, strtotime($post_date));
	}

	public function get_saved_data_path() {
		
		$path = YXML_IMPORT_DATA_PATH . $this->get_post_parent() . DIRECTORY_SEPARATOR .  $this->get_id() . DIRECTORY_SEPARATOR;
		
		if(!file_exists($path)):
		    mkdir($path, 0777, true);
		endif;

		return $path;
	}

	public function get_timestamp( $format = '' ) {

		$timestamp = current_time( 'timestamp' );
		return !empty($format) ? date($format, $timestamp) : $timestamp;
	}

	public function before_save() {
		
	}

	public function hash_args( $args = array() ) {
		return wp_hash( serialize($args) );
	}

	public function get_args_hash() {
		return $this->get_post_meta('_args_hash');
	}

	public function set_args_hash( $args = array()) {
		return $this->set_post_meta('_args_hash', $this->hash_args($args));
	}

	public function check_args( $args = array() ) {
		return $this->hash_args( $args ) == $this->get_args_hash();
	}

	public function is_complete() {
		return in_array( $this->get_post_status(), apply_filters( 'yxml_import_instance_complete_post_statuses', 
			array('yxml-success', 'yxml-failed', 'yxml-abandoned')
		 ) );
	}

	public function is_failed() {
		return in_array( $this->get_post_status(), apply_filters( 'yxml_import_instance_failed_post_statuses', 
			array('yxml-abandoned', 'yxml-failed')
		 ) );
	}

	public function is_successful() {
		return in_array( $this->get_post_status(), apply_filters( 'yxml_import_instance_successful_post_statuses', 
			array('yxml-success')
		 ) );
	}
/*
	public function get_importer_class() {
	}

	public function get_importer() {

		if(!is_null($this->importer)) return $this->importer;

		$importer_class = $this->get_arg('importer_class');

		if(!$importer_class) return false;

		$the_importer = new $importer_class;

		$this->importer = $the_importer;

		return $this->importer;

	}
*/
	public function run() {

		// add_filter('yxml_use_local_data', '__return_true');
		// $this->clear_instance_data();

		$import_attempts = $this->get_instance_data('import_attempts', 0);

		if(!$import_attempts):
			$import_attempts = 0;
			$this->add_note(__('Import initialised', 'yoke'));
		else:
			$this->add_note(sprintf(__('Import resumed. Attempt #%s', 'yoke'), $import_attempts + 1));
		endif;

		$import_attempts++;

		$this->save_instance_data( 'import_attempts', $import_attempts );

		$importer_class = $this->get_arg('importer_class');

		if(!$importer_class) return false;
				
		$importer_vars = $this->get_args();

		$the_importer = new $importer_class( $importer_vars );

		$completed_pages = absint($this->get_instance_data('completed_pages', 0));
		
		$the_importer->set_current_page( $completed_pages + 1 );

		$the_importer->set_data_complete( $this->get_instance_data('data_complete', false) );
		$the_importer->set_import_complete( $this->get_instance_data('import_complete', false) );

		$time_limit = absint($this->get_arg('time_limit', 0));

		if($time_limit) set_time_limit($time_limit);

		while($the_importer->getting_data()):
			
			// if($time_limit) set_time_limit($time_limit);

			$this->save_instance_data('current_page', $the_importer->get_current_page() - 1 );
			$this->save_instance_data('completed_pages', $the_importer->get_completed_pages() );
			$this->save_result( $the_importer->get_api_result(), $the_importer->get_completed_pages() );

		endwhile;
		
		$this->save_instance_data('data_complete', $the_importer->get_data_complete() );

		if(!$the_importer->has_errors() && $the_importer->get_data_complete()):
			// die('data_complete');
			// if($time_limit) set_time_limit($time_limit);

			$the_importer->set_imported_pages( $this->get_instance_data('imported_pages', 0) );
			$the_importer->set_imported_rows( $this->get_instance_data('imported_rows', 0) );

			$the_importer->set_saved_data( $this->get_saved_results() );
			
			while($the_importer->importing_data()):
				$this->save_instance_data('imported_pages', $the_importer->get_imported_pages() );
				$this->save_instance_data('imported_rows', $the_importer->get_imported_rows() );
			endwhile;

		endif;

		$this->save_instance_data('import_complete',  $the_importer->get_import_complete() );

		if($the_importer->has_errors()):

			$this->fail($the_importer->get_errors());

		elseif($the_importer->get_data_complete() && $the_importer->get_import_complete()):

			$this->success();

		endif;

		return;

		// var_dump('DATA END:');
		// var_dump($this->get_instance_data());

		// var_dump( $the_importer->get_import_complete() );

	}

	public function clear_instance_data( ) {
		$instance_data = array();
		update_post_meta( $this->get_id(), '_instance_data', $instance_data );
	}

	public function save_instance_data( $key, $value ) {
		$instance_data = $this->get_instance_data();
		$instance_data[$key] = $value;
		update_post_meta( $this->get_id(), '_instance_data', $instance_data );
	}

	public function get_instance_data( $key = null, $default = null ) {
		
		$instance_data = get_post_meta( $this->get_id(), '_instance_data', true );

		if(!$instance_data) $instance_data = array();

		if($key):
			return isset($instance_data[$key]) ? $instance_data[$key] : $default;
		endif;

		return $instance_data;
	}

	public function save_result( $data, $page_n, $prefix = '' ) {

		if(is_array($data)):
			
			foreach ($data as $part_n => $result):

				if(!method_exists($result, 'asXML')) continue;

				$prefix = str_pad($page_n, 3, 0, STR_PAD_LEFT) . '_';
				$this->save_result( $result, $part_n, $prefix );
				
			endforeach;

			return; 

		endif;

		if(!method_exists($data, 'asXML')) return;
		
		$dir_path = $this->get_saved_data_path();
		$file_path = $dir_path . $prefix . str_pad($page_n, 5, 0, STR_PAD_LEFT) . '.xml';

		$file = fopen($file_path, 'w') or die('Can\'t create local file:' . $file_path);
		fwrite($file, $data->asXML());
		fclose($file);
		
	}

	public function get_saved_results() {
		
		$dir_path = $this->get_saved_data_path();

		$results_arr = array();

		if(file_exists($dir_path) && is_dir($dir_path)):
			
			$file_arr = scandir($dir_path);

			foreach ($file_arr as $file_name):

				$file_path = $dir_path . $file_name;
				if(!is_file($file_path) || strtolower(pathinfo($file_path, PATHINFO_EXTENSION)) != 'xml') continue;

				// ob_start();
				// include( $file_path );
				// $result_raw = ob_get_clean();
				$result_raw = file_get_contents($file_path);
				$results_arr[] = YXML()->con->handle_response( $result_raw );

			endforeach;
		endif;

		return $results_arr;

	}

	public function abandon() {

		if($this->is_complete()) return false;

		$this->add_note( __('Import abandoned') );
		$this->set_post_status('yxml-abandoned');
		
		$this->save();
		$this->read();
	}

	public function fail( $reasons ) {

		if(empty($reasons)):
			$reasons = array(
				__('Import failed')
			);
		elseif(!is_array($reasons)):
			$reasons = array($reasons);
		endif;

		foreach ($reasons as $key => $reason):
			$this->add_note( $reason, 'error' );
		endforeach;
		
		$this->save_instance_data('errors', $reasons);
		$this->set_post_status('yxml-failed');
		$this->save();
		// $this->read();
	}
	public function success() {
		
		$this->add_note( __('Import completed') );
		$this->set_post_status('yxml-success');
		$this->save();
		// $this->read();
	}

	public function add_note( $note, $type = 'message' ) {

		$instance_notes = $this->get_instance_notes();
		
		$instance_notes[] = array(
			'time' => $this->get_timestamp( 'Y/m/d g:i:sa' ),
			'note' => $note,
			'type' => $type
		);

		update_post_meta( $this->get_id(), '_instance_notes', $instance_notes );
	}

	public function get_instance_notes() {
		
		$instance_notes = get_post_meta( $this->get_id(), '_instance_notes', true );

		if(!$instance_notes) $instance_notes = array();

		return $instance_notes;
	}


	public function before_delete() {

		$this->delete_files();
				
	}

	private function delete_files($target = '') {

		if(empty($target)) $target = $this->get_saved_data_path();

	    if(is_dir($target)):

	        $files = glob( $target . '*', GLOB_MARK );
	        
	        foreach( $files as $file ):
	            $this->delete_files( $file );      
	        endforeach;
	      
	        rmdir( $target );

	    elseif(is_file($target)):
	        unlink( $target );
	    endif;

	}
}

