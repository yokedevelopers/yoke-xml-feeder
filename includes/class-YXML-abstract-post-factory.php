<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



abstract class YXML_Post_Factory extends YXML_Data {

	protected $factory_id	 		= null;

	protected $single_class	 		= null;

	protected $post_type	 		= null;

	protected $import_object_type	= null;

	protected $custom_statuses 		= array();

	public function __construct() {

		parent::__construct();

		add_action( 'before_delete_post', array($this, 'before_delete_post'), 10, 1 );
		add_filter('yxml_custom_post_statuses', array($this, 'register_post_statuses'));

		add_filter( 'manage_edit-' . $this->get_post_type() . '_columns', array( $this, 'add_admin_columns'), 10, 1);
		add_action( 'manage_' . $this->get_post_type() . '_posts_custom_column' , array( $this, 'admin_column_content'), 10, 2 );

		$this->init_hooks();

	}

	public function get_custom_statuses( ) {		
		$statuses = is_array($this->custom_statuses) ? $this->custom_statuses : array();
		$cleaned_statuses = array();
		foreach ($statuses as $key => $value):
			$cleaned_statuses[ YXML()->post_types->clean_post_status($key) ] = $value;
		endforeach;
		return $cleaned_statuses;
	}

	public function add_admin_columns($cols) {
		return $cols;
	}

	public function admin_column_content( $column, $post_id ) {}

	public function register_post_statuses( $statuses ) {

		foreach ($this->get_custom_statuses() as $key => $value):
			$statuses[$key] = $value;
		endforeach;

		return $statuses;
	}

	private function init_hooks() {}

	public function get_posts( $args = array() ) {
		
		$posts = array();

		$post_ids = get_posts(wp_parse_args( $args, array(
				'post_type' => $this->get_post_type(),				
				'posts_per_page' => -1,
				'fields' => 'ids',
				'post_status' => array_merge( array_keys($this->get_custom_statuses()), array('any') )
			)
		));

		foreach ($post_ids as $key => $post_id):
			$posts[] = $this->get_post_by_id( $post_id );
		endforeach;

		return $posts;
	}

	public function get_post_from_api_item( $item ) {

		$single_class = $this->get_single_class();
		$post = new $single_class( $item );

		return $post ?: false;

	}

	public function get_post_by_id( $id ) {

		$single_class = $this->get_single_class();
		$post = new $single_class( $id );

		return $post ?: false;

	}

	public function post_exists( $store_id ) {

	}

	public function insert( $item, $update_existing = true ) {
		
		// var_dump(__CLASS__ . '->' . __FUNCTION__ . '()');
		$single = $this->get_post_from_api_item( $item );
		
		if(!$single->get_api_item()) return 0;

		$single->save();

		do_action('yxml_after_' . $this->get_post_type() . '_post_insert', $single );
		
		return $single->get_id();

	}

	public function get_import_object_type() {
		return $this->import_object_type;
	}

	public function get_post_type() {
		return apply_filters( 'yxml_import_' . $this->get_factory_id() . '_post_type',  $this->post_type );
	}

	protected function set_config() {}

	public function get_option_group_name() {
		return $this->get_factory_id() . '_settings';
	}

	public function get_option( $option_name = null ) {
		return YXML()->get_option($this->get_option_group_name(), $option_name);
	}

	/*************************************************************************************************/


	public function get_factory_id() {
		return $this->factory_id;
	}

	public function get_config() {
		return $this->config;
	}

	public function add_db_config( $config = array() ) {
		return $config;
	}

	public function get_single_class() {
		return $this->single_class;
	}

	public function get_db_table_name() {
		return $this->db_table_name;
	}
	
	/*************************************************************************************************/



	public function before_delete_post( $post_id ) {

		global $post_type;
	    if ( $post_type != $this->get_post_type() ) return;
		
		$post = $this->get_post_by_id( $post_id );

		if($post) $post->before_delete();
	}
}