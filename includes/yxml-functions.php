<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function yxml_current_user_can_manage_shop() {
	return YXML()->permissions->current_user_can_manage_shop( );
}

/*add_action('init', function(){
	$a = get_posts(array('post_type'=> 'location', 'fields' => 'ids',
'posts_per_page' => -1));
	foreach ($a as $key => $value) {
		var_dump($value);
		echo get_the_title($value) . ': ' . get_post_meta($value, 'yxml_code', true) . '<br>';
	}
	die();
});*/
function yxml_maybe_get_body_text( $note = '', $allow_tags = '<p>' ) {
	// echo $note;
	// var_dump( $note );
	// die();
	if(preg_match('/(?<body_content><body>[\s|\S]{0,}<\/body>)/', $note, $matches)):
	// if(preg_match('/<body>(?<body_content>(.|\s)+)<\/body>/', $note, $matches)):
// var_dump($matches);
	// die();
		$body_content = $matches['body_content'];
		$note = strip_tags($body_content, $allow_tags);

	endif;

	$note = trim($note);

	return $note;
}

function yxml_product_references_select_values() {

	$product_references = YXML()->references->get_product_references();

	$field_options = array();
	
	if(!empty($product_references)): 
		foreach ($product_references as $ref_type):

			if(!empty($ref_type['references'])): 
				$group_options = array();
				foreach ($ref_type['references'] as $ref):
					$group_options[$ref['id']] = $ref['name'];
				endforeach; 
				$field_options[] = array(
					'label' => $ref_type['name'],
					$group_options
					);
			endif;
		endforeach; 
	endif; 

	return $field_options;
}

add_filter('woocommerce_dropdown_variation_attribute_options_args' ,'filter_woocommerce_dropdown_variation_attribute_options_args', 900, 1);
add_filter('woocommerce_dropdown_variation_attribute_options_html' ,'filter_woocommerce_dropdown_variation_attribute_options_html', 900, 2);



function filter_woocommerce_dropdown_variation_attribute_options_html( $html, $args ) {
	return $html;
}

function filter_woocommerce_dropdown_variation_attribute_options_args( $args ) {

	$options 			= $args['options'];
	$attribute_name 	= $args['attribute'];
	$product 			= $args['product'];
	$selected 			= $args['selected'];
	if(empty($selected) && count($options) == 1):
		$selected = array_values($options)[0];
	endif;
	// var_dump($options);
	// var_dump($selected);

	$args['options']  		= $options;
	$args['attribute']  	= $attribute_name;
	$args['product']  		= $product;
	$args['selected']  		= $selected;

	return $args;
}
// add_filter('woocommerce_product_default_attributes' ,'filter_woocommerce_dropdown_variation_attribute_options_args');
// get_variation_default_attribute



function yxml_csv_import() {
	// require_once( YXML_ABSPATH . 'includes/imports/class-YXML-import-products.php');
	$file = fopen(YXML_ABSPATH . 'includes/docs_csvs/orders.csv', 'r');

	$headers = fgetcsv($file);

	foreach ($headers as $key => &$value) $value = str_replace(' ', '_', strtolower($value));

	$settings = array();
	$row = 0;

	while(!feof($file)):
		$values = fgetcsv($file);
		if($row && is_array($values)):
			$settings[] = array_combine($headers, $values);
		endif;
		$row++;
	endwhile;

	fclose($file);

	$settings = array_filter($settings, function($v, $k) {
		return (isset($v['post_request']) && strtolower($v['post_request']) == 'no' ? false: true );
	}, ARRAY_FILTER_USE_BOTH );

	echo '<pre>';
	foreach ($settings as $key => $value):
		if(empty($value['field'])):
		// echo PHP_EOL . PHP_EOL . " /* " . trim( implode("\t", $value) ) . " */ " . PHP_EOL;
		else:
		echo '\'' . $value['field'] . "' \t\t\t\t\t\t\t\t => null, /* " . $value['post_request'] . " \t\t " . $value['example'] . " \t\t " . $value['comment'] . " */" . PHP_EOL;
		// echo '$' . $value['field'] . " \t\t\t\t = null; /* " . $value['example'] . " \t\t " . $value['comment'] . " */" . PHP_EOL;
		endif;
	endforeach;
	echo '</pre>';
	
	p($settings);


	die();
} 

