<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}




class YXML_API_Call {

	protected $ch;

	protected $url;
	protected $method;
	protected $data;
	
	protected $headers = array();
	
	protected $verbose_data = '';
	protected $return_headers = array();
	protected $result;
	protected $exec_time;

	public function __construct( $url, $method, $data = null ) {

		$this->set_default_headers();

		$this->set_url( $url );
		$this->set_method( $method );
		$this->set_data( $data );

	}

	private function set_url( $url ) {
		$this->url = $url;
	}

	public function get_url() {
		return $this->url;
	}

	private function set_method( $method = null ) {
		$method = in_array(strtoupper($method), array('GET', 'POST', 'PUT')) ? strtoupper($method) : 'GET';
		$this->method = $method;
	}

	public function get_method() {
		return $this->method;
	}

	private function set_data( $data ) {
		$this->data = $data;
	}

	public function get_data() {
		return $this->data;
	}

	public function set_default_headers() {

		$this->headers = array(
			'Content-Type: text/xml',
	    	);
	}

	public function add_header( $value ) {
		$this->headers[] = (string) $value;
	}

	public function get_headers( ) {
		return $this->headers;
	}

	public function handle_return_header( $curl, $header_line ) {

		$head_arr = explode(':', $header_line, 2);

		if(count($head_arr) > 1):
			$this->return_headers[$head_arr[0]] = trim($head_arr[1]);
		elseif(strlen(trim($header_line))):
			$this->return_headers[] = $header_line;
		endif;

	    return strlen($header_line);
	}

	public function get_header( $key ) {
		return isset( $this->return_headers[ $key ] ) ? $this->return_headers[ $key ] : null;
	}

	public function get_return_location() {
		return $this->get_header( 'Location' ) ?: false;
	}

	public function ch( $init = false ) {

		if(is_null($this->ch) && $init) $this->ch = curl_init();

		return $this->ch;
	}

	private function set_result( $result ) {
		$this->result = $result;
	}

	public function get_result( $raw = false ) {

		$result_raw = $this->result;

		if($raw):
			$result = $result_raw;
		else:
			$result = self::parse_xml( $result_raw );
		endif;

		return $result;
	}

	public function get_exec_time() {
		return $this->exec_time;
	}

	public static function parse_xml( $xmlstr = '' ) {

		libxml_use_internal_errors(true);

		if(!is_string($xmlstr)) $xmlstr = '';

		$doc = simplexml_load_string($xmlstr, 'API_Result');

		if (!$doc):

			$return = new API_Error(libxml_get_errors());
			libxml_clear_errors();

		else:

			$return = $doc;

		endif;

		return $return;
	}

	private function add_error() {
		$this->errors[curl_errno($this->ch())] = htmlspecialchars(curl_error($this->ch())); 
	}

	private function set_verbose( $data ) {
		$this->verbose_data = $data;
	}

	public function print_verbose() {
		echo "Verbose information:\n<pre>", htmlspecialchars($this->verbose_data), "</pre>\n";
	}

	public function go() {
		
		if(!$this->ch( true )) return false;

	    switch ($this->get_method()):
	        case 'POST':
	            
	            curl_setopt($this->ch(), CURLOPT_POST, 1);
	    		curl_setopt($this->ch(), CURLOPT_POSTFIELDS, $this->get_data());

	    		curl_setopt($this->ch(), CURLOPT_RETURNTRANSFER, 1);

	    		break;

	        case 'PUT':

	            curl_setopt($this->ch(), CURLOPT_PUT, 1);

	    		$putData = fopen('php://temp/maxmemory:256000', 'w');  
			    
			    if (!$putData):
			        die('could not open temp memory data');  
			    endif;

			    fwrite($putData, $this->get_data());  
			    fseek($putData, 0);

			    curl_setopt($this->ch(), CURLOPT_INFILE, $putData);  
			    curl_setopt($this->ch(), CURLOPT_INFILESIZE, strlen($this->get_data())); 
	    		curl_setopt($this->ch(), CURLOPT_RETURNTRANSFER, 1);

	            break;

	        case 'GET':

	    		curl_setopt($this->ch(), CURLOPT_RETURNTRANSFER, 1);
	        
	            break;
	        default:
	    endswitch;
 
// $user = "MAPAXML";
// // $user = "yoke";
// $pass = "V6p;&\"{EG#ta:d?'121";
// curl_setopt($this->ch(), CURLOPT_HTTPHEADER, array(
// 	// 'Content-Type: text/xml',
// 	'Authorization: Basic '. base64_encode($user. ":" . $pass) // <---
// ));
// curl_setopt($curl, CURLOPT_USERPWD, $user. ":" . $pass);
	    curl_setopt($this->ch(), CURLOPT_HTTPHEADER, $this->get_headers());
		curl_setopt($this->ch(), CURLOPT_HEADERFUNCTION, array($this, 'handle_return_header'));
		curl_setopt($this->ch(), CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($this->ch(), CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($this->ch(), CURLOPT_URL, $this->get_url());
		
	    curl_setopt($this->ch(), CURLOPT_VERBOSE, true);
		$verbose_data = fopen('php://temp', 'w+');
		curl_setopt($this->ch(), CURLOPT_STDERR, $verbose_data);

		$start = microtime(true);
	    $result = curl_exec($this->ch());
		$this->exec_time = number_format( microtime(true) - $start, 2 );
		var_dump($result);
	    $this->set_result( $result );

	    // $info = curl_getinfo( $this->ch(), CURLINFO_EFFECTIVE_URL );
	    // var_dump($info);
	    // var_dump($info);
		
		if(!$result):
			$this->add_error();
		endif;

		rewind($verbose_data);
		$verboseLog = stream_get_contents($verbose_data);

		$this->set_verbose($verboseLog);

	    curl_close($this->ch());

	    return $result;
	}

}





class API_Result extends SimpleXMLElement {

	private $call_data = array();

	public function has_data() {
		return $this->is_AP21_error() ? false : true;
	}

	public function is_AP21_error() {
		return property_exists($this, 'ErrorCode') ? (string) $this->ErrorCode : false;
	}

	public function get_error() {
		return $this->is_AP21_error() && property_exists($this, 'Description') ? (string) $this->Description : false;
	}

	public function add_call_data($key, $data) {
		$this->call_data[$key] = $data;
	}

	public function get_call_data( $key = null ) {
		if($key):
			return isset($this->call_data[$key]) ? $this->call_data[$key][0] : null;
		else:
			return (array) $this->call_data;
		endif;
	}

    public function replace($element) {
        $dom     = dom_import_simplexml($this);
        $import  = $dom->ownerDocument->importNode(
            dom_import_simplexml($element),
            TRUE
        );
        $dom->parentNode->replaceChild($import, $dom);
    }
}

class API_Error {

	public $errors = array();
	
	public function __construct( $errors = array()) {

		$this->errors = $errors;

	}

	public function show() {

		if(!empty($this->errors)):
			foreach ($this->errors as $key => $error):
				ob_start();
				print_r($error);
				$output = ob_get_clean();
				echo sprintf('<div class="notice notice-error"><p><code>%s</code></p></div>', $output);
			endforeach;
		endif;
	}
}
