<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


abstract class YXML_Post extends YXML_Data {

	protected $import_object_type	 	= null;

	protected $supports 			 	= array( 'api_import' );

	protected $post_type	 		 	= null;

	protected $default_post_status	 	= 'draft';

	protected $mode					 	= 'read';

	protected $update_existing		 	= true;

	protected $api_item 			 	= null;

	protected $import_field_map			= array();

	protected $extra_data				= array(
		'post_data'		=> array(),
		'post_meta'		=> array(),
		'post_terms' 	=> array()
		);

	public function __construct( $item = null, $update = true ) {

		parent::__construct();

		$this->set_prop('update_existing', $update);

		if( !post_type_exists( $this->get_post_type() ) ):
			return false;
		endif;


		if($this->supports('api_import') && $this->set_api_item( $item )):
			
			$this->create_from_api();

		elseif(is_numeric($item)):
		
			$post_id = absint($item);

			if(get_post_type($post_id) == $this->get_post_type()):
				
				$this->set_id( $post_id );
				$this->set_mode('read');

				$this->read();

			endif;
		else:
			$this->set_mode('create');
		endif;
		
	}

	public function get_mode() {
		return $this->get_prop( 'mode' );
	}


	public function set_mode( $mode ) {
		$allowed_modes = array('read', 'create', 'update');
		if(in_array($mode, $allowed_modes)):
			$this->set_prop( 'mode', $mode );
		endif;
	}

	public function get_import_object_type() {
		return $this->import_object_type;
	}

	public function get_post_type() {
		return apply_filters( 'yxml_import_' . $this->get_object_type() . '_post_type', $this->post_type );
	}

	public function get_post_status() {
		return $this->get_post_data( 'post_status' );
	}

	public function set_post_status( $status = null ) {
		if(!$status) $status = $this->get_default_post_status();
		$this->set_post_data( 'post_status', $status );
	}

	public function get_default_post_status() {
		return $this->default_post_status;
	}

	public function after_insert() {}

	public function get_unique_meta_key( $include_prefix = true ) {

		$key = $this->get_prop('unique_meta_key');
		if($include_prefix && $key) $key =  YXML_DB_PREFIX . $key;
		return $key;
	}

	public function get_unique_meta_value() {
		
		$value = null;

		if( in_array($this->get_mode(), array('update', 'read')) ):

			$post_id = $this->get_id();
			$key = $this->get_unique_meta_key( );

			$value = $post_id && $key ? get_post_meta( $post_id, $key, true ) : false;

		endif;

		return $value;
	}

	public function get_unique_import_item_key() {
		return $this->get_prop('unique_import_item_key');
	}

	public function get_import_item_unique_value() {

		$item = $this->get_api_item( true );
		$unique_key = $this->get_unique_import_item_key();

		if(!$item || !$unique_key ) return null;

		$value = null;

		return $this->get_api_item_value($unique_key);
		
		return $value;

	}

	public function set_api_item( $item ) {

		apply_filters( 'yxml_set_api_item_' . $this->get_post_type(), $item, $this );

		if(is_a($item, 'API_Result')):
			
			$this->set_prop( 'api_item', $item );

			$post_id = $this->get_saved_post_id();

			if($post_id):
				$this->set_id( $post_id );
			endif;

			return true;
		endif;

		return false;
	}

	public function get_api_item( $as_array = false ) {

		$item = $this->get_prop( 'api_item' );
		return $as_array && $item ? (array) $item : $item;

	}

	public function get_api_item_value( $key, $default = null, $item = null ) {

		$item = $item ?: $this->get_api_item( true );

		if(is_array($key)):
			if(count($key) < 2):
				$key = reset($key);
			elseif(isset($item[$key[0]])):
				$new_item = (array) $item[$key[0]];
				$new_key = array_shift($key);
				return $this->get_api_item_value($key, $default, $new_item);
			endif;
		endif;

		if(stripos($key, '@') === 0):
			$key = ltrim($key, '@');
			return $this->get_api_item_attribute( $key, $default, $item );
		endif;


		$val = isset($item[$key]) ? $item[$key] : $default;

		if(empty($val) && is_a($val, 'API_Result')) $val = $default;

		return $val;
	}

	public function get_api_item_attribute( $key, $default = null, $item = null ) {

		$item = $item ?: $this->get_api_item( true );

		if(is_array($item)):
			$val = isset($item['@attributes'][$key]) ? (string) $item['@attributes'][$key] : $default;
		else:
			$val = isset($item[$key]) ? (string) $item[$key] : $default;
		endif;

		return $val;
	}

	public function get_import_field_map() {
		$val = $this->get_prop('import_field_map');
		return $val ?: array();
	}

	public function before_save() {}
	public function after_save() {}
	public function before_delete() {}

	public function save() {
		
		$this->before_save();

		do_action( 'yxml_before_' . $this->get_post_type() . '_post_save', $this );

		$this->set_post_data('post_type', $this->get_post_type());

		$changes = $this->get_changes();

		$post_data = isset($changes['post_data']) ? $changes['post_data'] : array();
		$post_meta = isset($changes['post_meta']) ? $changes['post_meta'] : array();
		$post_terms = isset($changes['post_terms']) ? $changes['post_terms'] : array();

		$updated_post_id = 0;

		$post_data_req = array(
			'post_title',
			'post_status',
			'post_parent'
			);

		foreach ($post_data_req as $key):
			if(!isset($post_data[$key])) $post_data[$key] = $this->get_post_data($key);
		endforeach;

		if( !apply_filters('yxml_' . $this->get_post_type() . '_post_valid_save', true,  $this) ) return;

		if(!empty($post_data)):

			$post_data['post_type'] = $this->get_post_type();
			
			if($this->get_id()):
				$post_data['ID'] = $this->get_id();
			elseif(isset($post_data['ID'])):
				unset($post_data['ID']);
			endif;
			
			if(isset($post_data['ID'])):
				$updated_post_id = wp_update_post( $post_data, false );
			else:
				$updated_post_id = wp_insert_post( $post_data, false );
			endif;
			
			if(is_wp_error( $updated_post_id )):
				
			elseif($updated_post_id): 
				$this->set_id( $updated_post_id );			
			endif;

		endif;
		
		if($this->get_id()):

			foreach ($post_meta as $key => $value):
				update_post_meta( $this->get_id(), $key, $value );
			endforeach;
			
			foreach ($post_terms as $taxonomy => $term_ids):
				wp_set_object_terms( $this->get_id(), $term_ids, $taxonomy );
			endforeach;

		endif;

		$this->after_save();

		do_action( 'yxml_after_' . $this->get_post_type() . '_post_save', $this->get_id(), $this );

		return $this->get_id();
	}

	public function create_from_api( $item = null ) {

		if(!is_null($item) && !$this->set_api_item($item)) return false;

		$post_id = $this->get_id();

		if($post_id):

			$this->set_mode('update');
			
			$this->read();

		else:
			$this->set_mode('create');
		endif;
	
		$unique_meta_key = $this->get_unique_meta_key( ); 
		$item_unique_value = $this->get_import_item_unique_value();

		$this->set_post_meta( $unique_meta_key, $item_unique_value);
		

		$post_args = $this->prepare_post_args();

		return $post_id;

	}
		
	/**
	* @var $item (mixed) string|int|object
	* @return (int) post_id of existing post or 0 if none exists
	*/
	public function get_saved_post_id( $item = null ) {
		
		$post_id = 0;

		$item = is_null($item) ? $this->get_api_item() : $item;

		if( !$item ) return $post_id;

		$post_unique_meta_key = $this->get_unique_meta_key();

		$import_item_key_value = $this->get_import_item_unique_value( $item );


		if(!$post_unique_meta_key || !$import_item_key_value) return 0;

		
		global $wpdb;

		$sql = $wpdb->prepare(
				"SELECT ID FROM $wpdb->posts WHERE post_type = %s AND ID IN (SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s) LIMIT 1",
				$this->get_post_type(),
				$post_unique_meta_key,
				$import_item_key_value
			);

		$results = $wpdb->get_results( $sql );

		if(!empty($results)):
			$post_id = (int) $results[0]->ID;
		endif;

		return $post_id;

	}

	public function prepare_post_args() {

		$post_type = $this->get_post_type();

		$post_id = $this->get_id() ?: null;

		$post_data = array(
			'ID' => $this->get_id(),
			'post_type' => $this->get_post_type()
			);
		
		if( $this->get_mode() == 'create' || !$post_id ):
			$post_data['post_status'] = $this->get_default_post_status();
		else:
			$post_data['post_status'] = get_post_status( $post_id );
		endif;
		
		$post_meta = array();
		$post_terms = array();

		$import_field_map = $this->get_import_field_map();

		foreach ($import_field_map as $import_obj_key => $import_obj_value):
		
			$default = isset($import_obj_value['default']) ? $import_obj_value['default'] : null;

			$import_obj_key = explode('|', $import_obj_key);

			$api_item_value = $this->get_api_item_value( $import_obj_key, $default );

			$api_meta_prefix = YXML()->get_api_meta_prefix();

			if(isset($import_obj_value['post'])):
				$post_data[$import_obj_value['post']] = $api_item_value;
			elseif(isset($import_obj_value['meta'])):
				$post_meta[ $api_meta_prefix . $import_obj_value['meta']] = $api_item_value;
			endif;

		endforeach;

		foreach ($post_meta as $key => $value):
			$this->set_post_meta($key, $value);
		endforeach;

		foreach ($post_data as $key => $value):
			$this->set_post_data($key, $value);
		endforeach;
	}

	public function filter_post_args( $args ) {
		return $args;
	}

	public function read_post_data() {

		if(!$this->get_id()) return;

		$post = get_post( $this->get_id() );
		$this->set_prop( 'post_data', (array) $post );

	}

	public function read_post_meta() {

		if(!$this->get_id()) return;

		$meta = get_post_meta( $this->get_id() );

		foreach ($meta as $key => &$value):
			if(is_array($value) && count($value) == 1)	$value = $value[0];
			$value = maybe_unserialize( $value );
		endforeach;

		$this->set_prop( 'post_meta', $meta );

	}

	public function read_post_terms() {

		if(!$this->get_id()) return;

		$terms = array();

		$taxonomies = get_object_taxonomies( $this->get_post_type() );
		
		foreach ($taxonomies as $key => $taxonomy):

			$terms[$taxonomy] = wp_get_object_terms( $this->get_id(), $taxonomy, array('fields' => 'ids'));
			
		endforeach;
		
		$this->set_prop( 'post_terms', $terms );

	}

	public function read() {

		if($this->get_id()):

			$this->read_post_data();
			$this->read_post_meta();
			$this->read_post_terms();
			$this->set_object_read( true );

		else:
			return false;
		endif;

	}

	public function set_post_meta( $key, $value ) {
		if(!isset($this->data['post_meta'][$key]) || $this->data['post_meta'][$key] !== $value):
			$this->changes['post_meta'][$key] = $value;
		endif;
	}

	public function set_post_data( $key, $value ) {
		if(!isset($this->data['post_data'][$key]) || $this->data['post_data'][$key] !== $value):
			$this->changes['post_data'][$key] = $value;
		endif;
	}

	public function get_post_meta( $key, $single = true ) {
		
		$value = null;

		if($this->get_mode() != 'read' && isset($this->changes['post_meta'][$key]) ):
			$value = $this->changes['post_meta'][$key];
		elseif(isset($this->data['post_meta'][$key])):
			$value = $this->data['post_meta'][$key];
			if($single && is_array($value) && count($value) == 1 && isset($value[0])) $value = $value[0];
		endif;

		return $value;
	}

	public function get_post_data( $key ) {
		
		$value = null;

		if($this->get_mode() != 'read' && isset($this->changes['post_data'][$key]) ):
			$value = $this->changes['post_data'][$key];
		elseif(isset($this->data['post_data'][$key])):
			$value = $this->data['post_data'][$key];
		endif;

		return $value;
	}

	public function set_post_term( $taxonomy, $term, $append = false ) {

		$taxonomy_arr = array();

		$term_id = 0;

		if(is_numeric($term)):
			$term_id = absint($term);
		elseif(is_object($term) || is_array($term)):
			$term = (array) $term;
			$term_id = isset($term['term_id']) ? $term['term_id'] : 0;
		endif;

		if($append):
			if(!$this->get_object_read()) $this->read();
			$taxonomy_arr =  isset($this->changes['post_terms'][$taxonomy]) ? $this->changes['post_terms'][$taxonomy] : $this->get_post_terms( $taxonomy );
		endif;

		if($term_id):
			$taxonomy_arr[] = $term_id;
		endif;

		$this->changes['post_terms'][$taxonomy] = array_values(array_unique($taxonomy_arr));

	}

	public function get_post_terms( $taxonomy = false ) {
		
		$value = null;
		$terms = array();

		if($this->get_mode() != 'read' && isset($this->changes['post_terms']) ):
			$terms = $this->changes['post_terms'];
		elseif(isset($this->data['post_terms'])):
			$terms = $this->data['post_terms'];
		endif;

		if($taxonomy):
			return isset( $terms[$taxonomy] ) ? $terms[$taxonomy] : null;
		else:
			return $terms;
		endif;

	}

}
