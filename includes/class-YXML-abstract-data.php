<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


abstract class YXML_Data {
	
	protected $id = 0;

	protected $object_type = 'data';

	protected $data = array();

	protected $changes = array();

	protected $object_read = false;

	protected $extra_data = array();

	protected $default_data = array();

	protected $supports = array();

	public function __construct() {
		$this->data         = array_merge( $this->data, $this->extra_data );
		$this->default_data = $this->data;
	}

	public function set_defaults() {
		$this->data        = $this->default_data;
		$this->changes     = array();
		$this->set_object_read( false );
 	}

	/**
	 * Set object read property.
	 *
	 * @since 3.0.0
	 * @param boolean $read
	 */
	public function set_object_read( $read = true ) {
		$this->object_read = (bool) $read;
	}

	/**
	 * Get object read property.
	 *
	 * @since  3.0.0
	 * @return boolean
	 */
	public function get_object_read() {
		return (bool) $this->object_read;
	}

	public function get_changes() {
		return $this->changes;
	}

	/**
	 * Merge changes with data and clear.
	 *
	 * @since 3.0.0
	 */
	public function apply_changes() {
		$this->data    = array_replace_recursive( $this->data, $this->changes );
		$this->changes = array();
	}

	public function get_supports( ) {
		return $this->supports;
	}

	public function supports( $feature ) {
		$supports = $this->get_supports();
		return in_array($feature, $supports);
	}

	protected function set_prop( $prop, $value ) {

		if( property_exists($this, $prop) ):
			$this->$prop = $value;
		else:
			$this->data[ $prop ] = $value;
		endif;
	}

	protected function get_hook_prefix() {
		return 'yxml_' . $this->object_type . '_get_';
	}

	public function get_prop( $prop ) {

		$value = null;

		if( property_exists($this, $prop) ):
			$value = $this->$prop;

		elseif( array_key_exists( $prop, $this->data ) ):

			$value = $this->data[ $prop ];

		endif;
		
		$value = apply_filters( $this->get_hook_prefix() . $prop, $value, $this );

		return $value;
	}

	public function get_id() {
		return $this->get_prop('id');
	}


	public function set_id( $id = 0 ) {
		$this->set_prop('id', absint( $id ));
	}

	public function get_object_type( ) {
		return $this->get_prop('object_type');
	}
}
