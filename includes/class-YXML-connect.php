<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * API Connection 
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Connect {

	protected $plugin_name;

	/**
	 * URL base for all connections.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string 	$url_base 	From admin settings screen
	 */
	
	protected $url_base;

	/**
	 * Authorisation string.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string 	$auth 	From admin settings screen
	 */

	protected $auth;

	/**
	 * Default Country Code.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string 	$default_country 	From admin settings screen, 2 char country code
	 */
	
	protected $default_country;

	protected $do_verbose = false;
	public $endpoint_file_name;
	public $use_local_data;


	/**
	 * Initialize the connection settings.
	 *
	 * @since    1.0.0
	 */
	public function __construct( ) {

		$this->init_hooks();

	}

	private function init_hooks() {

	}

	/**
	 * Initialise the API Call.
	 *
	 * @since    1.0.0
	 * @return object 	processed xml object of API results
	 */
	public function get($endpoint, $args = array()) {

		
		$endpoint = apply_filters('yxml_endpoint_name', $endpoint);
		$endpoint_file_name_ext = '.xml';

		$paging_str = '';

		if(is_array($endpoint)):
			$endpoint_file_name = implode( DIRECTORY_SEPARATOR, $endpoint ) . $paging_str . $endpoint_file_name_ext;
			$endpoint = implode('/', $endpoint);
		else:
			$endpoint_file_name = $endpoint . $paging_str . $endpoint_file_name_ext;
		endif; 
		
		if(!$endpoint) return;

		$this->use_local_data = apply_filters('yxml_use_local_data', YXML_USE_LOCAL_DATA );
		$this->endpoint_file_name = $endpoint_file_name;

		$call = false;

		if( $this->use_local_data ):
			$file_path = YXML_LOCAL_DATA_PATH . $endpoint_file_name;
			if(file_exists($file_path)):
				ob_start();
				include( $file_path );
				$result_raw = ob_get_clean();
			else:
				$result_raw = '';
			endif;
		else:
			$call = $this->call_api($endpoint, $args);
			$result_raw = $call ? $call->get_result( true ) : false;
		endif;

		$result = $this->handle_response( $result_raw );

		if(is_a($result, 'API_Result') && $call):
			$result->add_call_data('url', $call->get_url());
		endif;

		return $result;

	}

	public function handle_response( $result_raw, $request_type = 'GET' ) {

		$result = YXML_API_Call::parse_xml($result_raw);

		if(is_a($result, 'API_Error')):

			if( YXML_SHOW_IMPORT_XML_ERRORS ) $result->show();
			return false;

		elseif( $request_type == 'GET' && !$this->use_local_data && $this->endpoint_file_name ):

			$file_path = YXML_LOCAL_DATA_PATH . $this->endpoint_file_name;
			$file_dir = dirname($file_path);

			if(!is_dir($file_dir)):
				mkdir($file_dir, 0777, true);
			endif;

			$file = fopen($file_path, 'w') or die('Can\'t create local file:' . $file_path);
			fwrite($file, $result_raw);
			fclose($file);

		endif;

		return $result;
	}

	public function put($endpoint, $xml_data, $args = array()) {

		$endpoint = apply_filters('yxml_endpoint_name', $endpoint);
		$call = $this->call_api($endpoint, $args, 'PUT', $xml_data);
		
		return $call;

		$result_raw = $call->get_result( true );
		
		return $this->handle_response( $result_raw, 'PUT' );

	}

	public function post($endpoint, $xml_data, $args = array()) {

		$endpoint = apply_filters('yxml_endpoint_name', $endpoint);

		$headers_only = isset($args['headers_only']) && $args['headers_only'] ? true : false;

		$call = $this->call_api($endpoint, $args, 'POST', $xml_data, $headers_only);
		
		return $call;

		$result_raw = $call->get_result( true );

		$result = $call->get_result( true );

		$location = $call->get_return_location();

		return $this->handle_response( $result_raw, 'POST' );
	}

	/**
	 * Call the API
	 *
	 * @since    1.0.0
	 * @return 	xml string
	 */
	public function call_api($endpoint, $args = array(), $method = 'GET', $xml_data = null, $headers_only = false) {
		
		
		$result = false;

		$settings = YXML()->get_option('api_settings');
// var_dump($settings);
// die();
		$url_base = isset($settings['live_url_base']) ? $settings['live_url_base'] : null;
		$user = isset($settings['live_user']) ? $settings['live_user'] : null;
		$pass = isset($settings['live_pass']) ? $settings['live_pass'] : null;
		// $auth = isset($settings['live_auth']) ? $settings['live_auth'] : null;
// var_dump($pass);
// die();
		if(!$url_base || !$user || !$pass ):

			YXML()->notices->err( __('Connection to Yoke XML failed. Check your settings.', 'yoke') );
			return $result;
			
		endif;

		$url_base = trim(trim($url_base), '/') . '/';

		$this->url_base = $url_base;
		// $this->auth = $auth;
		$this->auth = array(
			'user' => $user,
			'pass' => $pass
		);

		if(!is_array($args)) $args = array();


		$args = wp_parse_args( apply_filters('yxml_endpoint_args', $args, $endpoint, $method), array() );

		
		$url = $this->url_base . $endpoint;

        if ($args):
            $url = sprintf("%s?%s", $url, urldecode(http_build_query($args)));
        endif;

		$caller = new YXML_API_Call( $url, $method, $xml_data );

		if(is_array($this->auth)):
			$auth = 'Basic '. base64_encode($this->auth['user']. ":" . $this->auth['pass']); // <---
			$caller->add_header('Authorization: Basic '. base64_encode($user. ":" . $pass) );
		else:
			$caller->add_header( 'Authorization: ' . $auth );

// var_dump($user);
// var_dump($pass);
// $user = "MAPAXML";
// $pass = "V6p;&"{EG#ta:d?'121";

		endif;


	    // p($caller);
	    // die();

		$caller->go();

	    return $caller;
	}


}


