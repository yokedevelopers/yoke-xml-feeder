<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 * Import Posts
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Import extends YXML_Post {

	protected $object_type	 			= 'import';
	
	protected $post_type	 		 	= 'yxml_import';

	protected $supports = array();

	public function get_importer_id() {
		return $this->get_post_meta('_importer_id');
	}

	public function get_importer() {

		$id = $this->get_importer_id();

		if($id):
			$import_class = YXML()->imports->get_importer($id);
			if(class_exists($import_class)) return $import_class;
		endif;

		return false;
	}


	public function get_instance_args() {

		$args = array();

		$importer_class = $this->get_importer();
		
		if(!$importer_class) return false;

		$args['importer_class'] = $importer_class;

		$_results_per_page = $this->get_post_meta('_results_per_page');
		if($_results_per_page) $args['results_per_page'] = $_results_per_page;

		$_time_limit = $this->get_post_meta('_time_limit');
		if($_time_limit) $args['time_limit'] = $_time_limit;

		$_changes_only = $this->get_post_meta('_changes_only');
		$args['changes_only'] = $_changes_only ? true : false;

		$latest = $this->get_lastest_successful_instance();
		$args['last_updated'] = $latest ? $latest->get_time_started('Y-m-d\TH:m:s') : null;

		return $args;
	}

	public function get_instances( $args = array() ) {

		$args['post_parent'] = $this->get_id();
		return YXML()->import_instances->get_posts( $args );
	}

	public function get_latest_instance( ) {

		$instances = $this->get_instances(array(
			'posts_per_page' => 1
		));

		return !empty($instances) ? $instances[0] : false;

	}

	public function get_lastest_successful_instance( $args = array() ) {
		
		$instances = $this->get_instances(array(
			'posts_per_page' => 1,
			'post_status' => 'yxml-success'
		));

		return !empty($instances) ? $instances[0] : false;
		
	}

	public function get_saved_data_path() {
		
		$path = YXML_IMPORT_DATA_PATH . $this->get_id() . DIRECTORY_SEPARATOR;
		
		if(!file_exists($path)):
		    mkdir($path, 0777, true);
		endif;

		return $path;
	}

	public function before_delete() {

		$this->delete_files();

		foreach ($this->get_instances() as $instance):
			wp_delete_post( $instance->get_id(), true );
		endforeach;
		
	}

	private function delete_files($target = '') {

		if(empty($target)) $target = $this->get_saved_data_path();

	    if(is_dir($target)):

	        $files = glob( $target . '*', GLOB_MARK );
	        
	        foreach( $files as $file ):
	            $this->delete_files( $file );      
	        endforeach;
	      
	        rmdir( $target );

	    elseif(is_file($target)):
	        unlink( $target );
	    endif;

	}

	public function new_import_instance( $args = array() ) {
		return new YXML_Import_Instance( null, $this->get_id(), $args );
	}

	public function trigger( $resume = true ) {

		$import_instance = false;
		
		$args = $this->get_instance_args();

		if(!$args) return false;

		if( $resume && $latest_import_instance = $this->get_latest_instance()):
			
			if( !$latest_import_instance->check_args( $args ) ):
				$latest_import_instance->abandon();
			elseif( !$latest_import_instance->is_complete() ):
				$latest_import_instance->set_args( $args );
				$import_instance = $latest_import_instance;
			endif;
		endif;

		if(!$import_instance):
			$import_instance = $this->new_import_instance( $args );
			$import_instance->save();
		endif;
		
		error_reporting(0);
		register_shutdown_function(array($this, 'on_import_error'), $import_instance );

		if($import_instance) $import_instance->run();		

	}

	
	public function on_import_error( $import_instance ) {
// wp_redirect( 'http://dobsons.test/wp/wp-admin/?action=yxml_action&yxml_action=import_trigger&import_id=56353&_wp_http_referer=http%3A%2F%2Fdobsons.test%2Fwp%2Fwp-admin%2Fedit.php%3Fpost_type%3Dyxml_import' );
// exit;
		$err = error_get_last();

		if(!empty($err) && $err['type'] == 1):

	     	$err_message = (!empty($err) ? $err['message'] : __('Unknown error', 'yoke'));
	     
	     	$import_instance->add_note( sprintf(__('The import was stopped due to a fatal error: <code><strong>%s</strong></code>', 'yoke'),  $err_message ), 'error' );

   			$ref = wp_get_referer();

			if($ref && (!defined('DOING_AJAX') || !DOING_AJAX )):
				wp_redirect( $ref );
				exit;
			endif;

		endif;
		
	}
}



