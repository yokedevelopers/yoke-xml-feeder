<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 *  Put Load - Abstract
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */


class YXML_Log {

	public function __construct() {

		$this->init_hooks();
	}

	public function init_hooks() {}

	public function log( $detail = '', $file = 'log' ) {

		$dir = YXML_LOGS_PATH;

		if(!file_exists($dir)):
		    mkdir($dir, 0777, true);
		endif;

		$file_path = $dir . $file . '.txt';

		file_put_contents($file_path, "\n" . date('Y/m/d H:i:s', current_time( 'timestamp' )) . "\n", FILE_APPEND);

		$detail = is_array($detail) ? $detail : array($detail);

		foreach ($detail as $key => &$line) $line = is_array($line) ? serialize($line) : $line;

		$output = '';
		foreach ($detail as $key => $value) $output .= "$key:\t $value\n";
		// $detail = implode("\n", $detail);

		file_put_contents($file_path, $output . "\n", FILE_APPEND);
	}

	public function get_log( $file = 'log' ) {

		$file_path = YXML_LOGS_PATH . $file . '.txt';
		
		$output = '';
		
		if(file_exists($file_path)):
			$output = file_get_contents($file_path);
		endif;

		return $output;
	}

	public function print_log( $file = 'log' ) {
		echo '<pre>';
		echo $this->get_log( $file );
		echo '</pre>';
	}
}
