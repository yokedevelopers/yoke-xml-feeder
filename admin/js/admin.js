
(function( $ ) {
	'use strict';

	var A21 = {
		settings : $.extend({
			ajax_url : 		'/wp/wp-admin/admin-ajax.php'
			},
			yxml_script_settings || {}
		),
		actions: [],
		init: function(){
			A21.common();

			var called_init = ['common'];

			if(typeof this.settings.init_actions !== 'undefined') {
				$.each(this.settings.init_actions, function(func, args){
					if(typeof A21[func] === 'function' && called_init.indexOf(func) < 0) {
						A21[func].call(A21, ...args);
						called_init.push(func);
					}
				});
			}
		},
		common: function() {
			A21.ajax_handler.init();

			var $admin_page = $('#yxml-admin-wrap');

			$('.yxml-input-select').each(function(){

				var $field = $(this);

				$field.select2({
						minimumResultsForSearch : 20,
						placeholder : $field.attr('placeholder')
						// closeOnSelect : false
					});
			});


		},
		screen_imports: {
			init : function() {}
		},
		screen_references: {
			init : function() {

			}
		},
		admin_import_post_settings: function() {

			var $meta_box = $('#yxml_import_settings');

			$meta_box.find(':input[name="_import_type"]').change(function(){
				var $field = $(this);
				console.log($field.val());
				
			});

		},
		ajax_handler: {
			init: function() {
				
				this.forms();
				this.handle_response();
			},
			forms: function() {

				var $ajax_forms = $('form.yxml-ajax-submit');

				$ajax_forms.each(function(){
					
					var $form = $(this);

					$form.submit(function(event) {

						event.preventDefault();

						var form_data = $form.serializeObject();
						console.log(form_data);
						return;
						if(typeof $form.data.xhr !== 'undefined') {
							$form.data.xhr.abort();
						}

						$form.data.xhr = $.ajax({
							url: A21.settings.ajax_url,
							type: 'POST',
							dataType: 'json',
							data: form_data,
						})
						.done(function() {
							console.log("success");
						})
						.fail(function() {
							console.log("error");
						})
						.always(function() {
							console.log("complete");
						});

					});

					if($form.hasClass('auto-submit')) {
						var $inputs = $(':input');

						$form.on('clear', function(){
							$form.submit();
						});

						$inputs.change(function() {
							$form.submit();
						});
					}

				});

			},
			handle_response: function() {

				$( document ).ajaxSuccess(function( event, xhr, settings ) {

					var response = xhr.responseText;

					var output = null;

					if(response) {
					    try {
							output = $.parseJSON(response);
					    } catch(e) {
							output = response;
					    }
					}

					if(typeof settings.data !== 'undefined') {

						var settings_data = parseQueryString(settings.data);
						
						if(settings_data && typeof settings_data.action === 'string') {
							A21.do_action('ajax_return_' + settings_data.action, output);
						}

					} else {
						A21.do_action('ajax_return', output, xhr, settings);
					}

				});

			}
		},
		add_action(the_action, the_callback, priority) {
			
			priority = priority || 10;

			if(typeof the_callback !== 'function') {
				console.log('The callback "' + the_callback + '" was not found.');
				return;
			}
			
			if(typeof A21.actions[the_action] === 'undefined') {
				A21.actions[the_action] = [];
			}
			
			if(A21.actions[the_action].length < priority) {
				for (var i = A21.actions[the_action].length; i < priority; i++) {
					A21.actions[the_action][i] = null;
				}
			}

			if(typeof A21.actions[the_action][priority] === 'undefined') {
				A21.actions[the_action][priority] = the_callback;
			} else {
				A21.actions[the_action].splice(priority, 0, the_callback);	
			}

		},
		do_action() {

			var args = arguments,
				the_action = A21.actions[args[0]];
				
			if(typeof the_action === 'undefined' || !the_action.length) {
				return;
			}

			$.each(the_action, function(priority, callback) {
				if(typeof callback === 'function') {
					return callback.apply(this, Array.prototype.slice.call(args, 1));				
				}
			});

		}
	}

	function parseQueryString(query) {

		if(typeof query === 'undefined' || !query.length ) {
			return;
		}

	    var vars = query.split('&');
	    var return_obj = {};

	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split('=');
	       	return_obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	    }
	    return return_obj;
	}


	$.fn.serializeObject = function() {
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};


	$( window ).load(function() {
		
		A21.init();

		if( typeof A21.settings.admin_page !== 'undefined' && A21.settings.admin_page ) {
			if(
				typeof A21['screen_' + A21.settings.admin_page] !== 'undefined' &&
				typeof A21['screen_' + A21.settings.admin_page].init === 'function'
				) {
				A21['screen_' + A21.settings.admin_page].init.call();
			}
		}
	});


})( jQuery );

