<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}



/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    YXML
 * @subpackage YXML/admin
 * @author     Your Name <email@example.com>
 */

class YXML_Admin_Options {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( ) {

		$this->init_hooks();


	}

	private function init_hooks() {
        
        add_action( 'yxml_init', array($this, 'do_config'));

		add_action( 'admin_menu', array($this, 'add_plugin_pages') );
		add_action( 'admin_init', array($this, 'page_init') );	


        /* Load the JavaScript needed for the settings screen. */
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'admin_footer-' . $this->settings_page_id(), array($this, 'footer_scripts' ) );

        /* Set number of column available. */
        add_filter( 'screen_layout_columns', array( $this, 'layout_columns' ), 10, 2 );


	}

    public function enqueue_scripts( $hook_suffix ){
        // die($hook_suffix);
        if ( $hook_suffix == $this->settings_page_id() ){
            wp_enqueue_script( 'common' );
            wp_enqueue_script( 'wp-lists' );
            wp_enqueue_script( 'postbox' );
        }
    }

    public function layout_columns( $columns, $screen ){
        // p($screen);
        // p($columns);
        // die();
        // $page_hook_id = fx_smb_setings_page_id();
        // if ( $screen == $this->settings_page_id() ){
        //     $columns[$this->settings_page_id()] = 2;
        // }
        return $columns;
    }


    public function settings_page_id() {
        return 'apparel21_page_yxml-import'; //strtolower(YXML_PLUGIN_NAME) . '-settings';
    }

    public function get_config() {
        
        $option_settings = apply_filters('yxml_admin_settings', array(

            'api_settings' => array(
                    'label'     =>  __('API Settings', 'yoke'),
                    'desc'      =>  __('', 'yoke'),
                    'fields'    =>  array (
                                        'live_url_base'         =>  array(
                                                                            'label' => __('URL Base', 'yoke'),
                                                                            'desc' => __('', 'yoke'),
                                                                            
                                                                        ),
                                        'live_user'             =>  array(
                                                                            'label' => __('Username', 'yoke'),
                                                                            'desc' => __('', 'yoke'),
                                                                        ),
                                        'live_pass'         =>  array(
                                                                            'label' => __('Password', 'yoke'),
                                                                            // 'type' => 'password'
                                                                        ),
                                        'show_import_errors'    =>  array(
                                                                            'label' => __('Show import errors', 'yoke'),
                                                                            'type' => 'yes_no',
                                                                        ),
                                        'use_local_data'        =>  array(
                                                                            'label' => __('Use Local Data', 'yoke'),
                                                                            'type' => 'yes_no',
                                                                        ),
                                        'save_local_data'       =>  array(
                                                                            'label' => __('Save Local Data', 'yoke'),
                                                                            'type' => 'yes_no',
                                                                            'desc' => sprintf( __('Local data directory: <code>%s</code>', 'yoke'), YXML_LOCAL_DATA_PATH )
                                                                        ),
                                    )
                ),
            /*'update_schedule' => array(
                    'label'     =>  __('Update Schedule', 'yoke'),
                    'fields'    =>  array (
                                        'auto_update'           =>  array(
                                                                        'label' => __('Auto Update', 'yoke'),
                                                                        'type' => 'yes_no',
                                                                    ),
                                    )
                )*/
        ));

        return array(
            'options_cap'                   =>      apply_filters('yxml_manage_plugin_cap', 'manage_options'),
            // 'options_menu_title'            =>      __('Yoke XML', 'yoke'),
            // 'options_page_title'            =>      __('Yoke XML', 'yoke'),
            'options_page_slug'             =>      strtolower(YXML_PLUGIN_NAME) . '-settings',
            // 'options_page_icon'             =>      'dashicons-chart-pie',
            // 'options_page_menu_order'       =>      '56',
            // 'options_group'                 =>      strtolower(YXML_PLUGIN_NAME) . '-options',
            // 'option_prefix'                 =>      YXML_OPTION_PREFIX,
            'option_settings'               =>      $option_settings
        );

    }

    public function do_config() {

        $config_defaults = $this->get_config();

        if(!empty($config_defaults) && is_array($config_defaults)):
            foreach ($config_defaults as $param_name => $value):
                $value = apply_filters( 'yxml_admin_settings_config_value_'. $param_name , $value );
                $this->$param_name = $value;
            endforeach;
        endif;

    }
    /**
     * Add options page
     */
    public function add_plugin_pages() {

        add_menu_page(
			__('Yoke XML', 'yoke'), //$this->options_page_title,
			__('Yoke XML', 'yoke'), //$this->options_menu_title,
            $this->options_cap,
            $this->options_page_slug, 
            array( $this, 'create_admin_page' ),
            'dashicons-chart-pie', //$this->options_page_icon,
            '56' //$this->options_page_menu_order
        );
        
        add_submenu_page(
            $this->options_page_slug, 
            __('Yoke XML Settings', 'yoke'),//$page_title, 
            __('Settings', 'yoke'),//$menu_title, 
            $this->options_cap,
            $this->options_page_slug
        );
        
       
    }

    /**
     * Options page callback
     */
    public function create_import_page() {

        if ( !current_user_can( $this->options_cap ) ):
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        endif;

        include plugin_dir_path( __FILE__ ) . 'partials/YXML-import-page.php';
    }

    public function create_references_page() {

        if ( !current_user_can( $this->options_cap ) ):
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        endif;

        include plugin_dir_path( __FILE__ ) . 'partials/YXML-references-page.php';
    }

    public function set_all_options() {
    	
    	$options = array();
    	
    	$option_settings = $this->option_settings;

    	if(empty($option_settings)) return;

    	// $option_prefix = $this->option_prefix;

    	foreach ($option_settings as $setting_name => $setting_group):
    		
    		$option_name = YXML_OPTION_PREFIX . $setting_name;

    		$options[$setting_name] = get_option($option_name);

    	endforeach;

    	$this->options = $options;

    }
    public function create_admin_page() {
     
        $this->set_all_options();
       	
       	if ( !current_user_can( $this->options_cap ) ):
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		endif;

		include plugin_dir_path( __FILE__ ) . 'partials/YXML-admin-display.php';

    }

    /**
     * Register and add settings
     */
    public function page_init() { 
    	
    	$option_settings = $this->option_settings;
    	if(empty($option_settings)) return;

    	// $option_prefix = $this->option_prefix;

    	foreach ($option_settings as $setting_name => $setting_group):

    		$group_id = $setting_name; // ID
    		$option_name = YXML_OPTION_PREFIX . $setting_name;

	        register_setting(
                $option_name, // Option group
	            $option_name, // Option name
	            array( $this, 'sanitize' ) // Sanitize
	        );

	        add_settings_section(
	            $group_id,
	            $setting_group['label'],
	            array( $this, 'print_section_info' ), // Callback
	            $option_name // 
	        );

    		if(isset($setting_group['fields'])):
    			foreach ($setting_group['fields'] as $field_key => $field):

			        add_settings_field(
			            $field_key,
			            $field['label'], // Title 
			            (!isset($field['callback']) ? array( $this, 'setting_field_cb' ) : $field['callback']), // Callback
			            $option_name, 
			            $group_id, // Section  
		            	array_merge(
				            array(
									'field_name' => $field_key,
									'option_name' => $setting_name

			            		), 
				            $field
				 		)
			        );    

    			endforeach;
    		endif;
    	endforeach;
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input ) {

        $option_page = $_POST['option_page'];

       	if ( !current_user_can( $this->options_cap ) ):
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		endif;
	 	
    	// Create our array for storing the validated options
        $output = array();


        // Loop through each of the incoming options
        foreach( $input as $key => $value ):
            if( isset( $input[$key] ) ):
                // var_dump($input[$key]);
                $output[$key] = $this->sanitize_value( $input[ $key ] );

                // var_dump($output[$key]);
                 
            endif;
        endforeach;
        
        // var_dump($input);
        // var_dump($output);
        // die();
        add_settings_error( 'yoke_messages', 'yoke_message', __( 'Settings Saved', 'yoke' ), 'updated' );
	    return apply_filters( 'yxml_validate_option_input', $output, $input, $option_page );

    }

    public function sanitize_value( $value ) {
        
        if(is_array($value)):
            foreach ($value as $key => $value_el):
                $value[$key] = $this->sanitize_value( $value_el );
            endforeach;
        else:
            $value = sanitize_text_field($value);
            // $value = strip_tags( stripslashes( $value ) );
        endif;

        return $value;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info( $section_settings ) {

    	$option_settings = $this->option_settings;

    	if(isset($option_settings[$section_settings['id']]['desc'])):
        	echo $option_settings[$section_settings['id']]['desc'];
        endif;
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function setting_field_cb($args = array()) {

    	$field_name = esc_attr(YXML_OPTION_PREFIX . $args['option_name'] ) . '[' . esc_attr( $args['field_name'] ) . ']';

        $option_group_values = (isset( $this->options[$args['option_name']] ) ? $this->options[$args['option_name']] : null);

    	$setting_value = null;
        if(!empty($option_group_values) && isset($option_group_values[$args['field_name']])):
    		$setting_value = $option_group_values[$args['field_name']];
    	endif;

        yxml_field( $field_name, (!empty($args['type']) ? $args['type'] : 'text'), array_merge(array(
            'value' => $setting_value,
            'options' => (isset($args['options']) ? $args['options'] : null)
            ), (isset($args['field_args']) ? $args['field_args'] : array()) ) );

        if(isset($args['desc'])):
            printf( '<p class="yk-setting-desc">%s</p>', $args['desc'] );
        endif;
    }

    public function get_wc_countries() {

        $countries = array();

        if(class_exists('WC_Countries')):
            $countries = new WC_Countries();
            $countries = $countries->__get('countries'); 

        endif;

        return $countries;

    }
    
    public function footer_scripts() {
        ?>
        <script type="text/javascript">
            //<![CDATA[
            jQuery(document).ready( function($) {
                // toggle
                $('.if-js-closed').removeClass('if-js-closed').addClass('closed');
                postboxes.add_postbox_toggles( '<?php echo $this->settings_page_id(); ?>' );
                // display spinner
                /*$('#fx-smb-form').submit( function(){
                    $('#publishing-action .spinner').css('display','inline');
                });
                // confirm before reset
                $('#delete-action .submitdelete').on('click', function() {
                    return confirm('Are you sure want to do this?');
                });*/
            });
            //]]>
        </script>
        <?php
    }
}