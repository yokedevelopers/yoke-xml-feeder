<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/admin/partials
 */

$option_settings = YXML()->admin->options->option_settings;
$options_page_slug = YXML()->admin->options->options_page_slug;

$current_tab = (isset($_GET['tab']) ? $_GET['tab'] : array_keys($option_settings)[0]);

if(!empty(	$option_settings )):

$active_section = null;

?>


<div id="yxml-admin-wrap" class="wrap">
 	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <?php settings_errors( 'yoke_messages' ); ?>
 	<h2 class="nav-tab-wrapper">

		<?php $tab_n = 0; ?>
		<?php foreach ($option_settings as $setting_name => $setting_group): ?>

			<?php

			$tab_url = esc_url( add_query_arg( array( 'tab' => $setting_name ), admin_url( 'admin.php?page=' . $options_page_slug ) ) );
			$is_active = ($current_tab == $setting_name || is_null($current_tab) && $tab_n == 0);

			?>
			<a href="<?php echo $tab_url; ?>" class="nav-tab <?php if ( $is_active ) echo ' nav-tab-active'; ?>"><?php esc_html_e(  $setting_group['label'] ); ?></a>

			<?php $tab_n++; ?>
		<?php endforeach; ?>
	</h2>
	

	<form method="post" action="options.php">
        <?php

        if(!is_null($current_tab)):

    		$option_name = YXML_OPTION_PREFIX . $current_tab;
    	
            // This prints out all hidden setting fields
            settings_fields( $option_name );
            do_settings_sections( $option_name );
          
			do_action( 'yxml_after_admin_options_form_fields_tab_' . $current_tab );

			submit_button(__('Save all settings', 'yoke'));

		endif;

        ?>
	</form>

	<?php do_action( 'yxml_after_admin_options_form' ); ?>
	<?php do_action( 'yxml_after_admin_options_form_tab_' . $current_tab ); ?>
	
</div>

<?php endif; ?>