<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/admin/partials
 */

?>
<?php 

	/* global vars */
	global $hook_suffix;

	/* enable add_meta_boxes function in this page. */
	do_action( 'add_meta_boxes', $hook_suffix );

?>

<?php 
	$doing_import_options = false;
	if(isset($_GET['edit_options'])):
		$import_id = $_GET['edit_options'];
		$import = YXML()->imports->get_import( $import_id );
		$doing_import_options = true;
	endif;
?>

<div id="yxml-admin-wrap" class="wrap">

 	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

    <?php settings_errors( 'yoke_messages' ); ?>
	
	<div class="yxml-settings-meta-box-wrap">

		
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>">
					<div class="postbox-container">
						<div class="meta-box-sortables ui-sortable">
 	
						    <?php if($doing_import_options): ?>
								
								<form id="yxml-import-form" action="options.php" method="post">

									<?php $import->print_user_settings(); ?>
									<?php wp_referer_field(); ?>

									<input type="hidden" name="yxml_save_import_options" value="<?php echo $import->id; ?>">

									<input type="submit" name="">
								</form>
						    <?php else: ?>
								<form id="yxml-import-form" action="options.php">
								 	<?php $imports = YXML()->imports->imports; ?>
						
									<input type="hidden" name="action" value="yxml_action">
									<input type="hidden" name="yxml_action" value="do_import">
									<input type="hidden" name="page" value="yxml-import">

						  			<?php wp_nonce_field(); ?>

								 	<?php if(!empty($imports)): ?>

									 	<?php foreach($imports as $import_id => $import): ?>

										 	<?php

							  		 		$started = $import->get_option('started');
									 		$started_time = ($started ? new DateTime($started) : null);
									 		$started_str = $started ?: 'N/A';

									 		$completed = $import->get_option('completed');
									 		$completed_time = ($completed ? new DateTime($completed) : null);
									 		$completed_str = $completed ?: 'N/A';

									 		$success_completed = $import->get_option('success_completed');
									 		$success_completed_time = ($success_completed ? new DateTime($success_completed) : null);
									 		$success_completed_str = $success_completed ?: 'N/A';

									 		$success_started = $import->get_option('success_started');
									 		$success_started_time = ($success_started ? new DateTime($success_started) : null);
									 		$success_started_str = $success_started ?: 'N/A';

									 		$duration = null;
									 		$duration_str = 'N/A';

									 		if($success_started_time && $success_completed_time):
												$duration =	$success_completed_time->format('U') - $success_started_time->format('U');
									 			$duration_str = ( $duration < 1 ? __( '< 1 second', 'yoke' ) : sprintf(_n( '%s second', '%s seconds', $duration, 'yoke' ), number_format_i18n( $duration )) );
									 		endif;

									 		$success = $import->get_option('success');

										 	?>
											<div id="import-div-<?php echo $import_id; ?>" class="postbox">
						        				<button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Activity</span><span class="toggle-indicator" aria-hidden="true"></span></button>
						        				<h2 class="hndle ui-sortable-handle"><span><?php echo $import->get_label(); ?></span></h2>
						        				<?php 
						        				// echo $import->get_settings_page_link(); 
						        				//echo $import->get_import_link(); 
						        				?>

						        				
										 		<table class="widefat striped">
										 			<thead>
										 				<tr>
											 				<th >
											 					<code><?php echo $import->get_endpoint(); ?></code>
											 				</th>
											 				<th>
																<button name="import_id" value="<?php echo $import_id; ?>" class="button button-primary alignright"><?php _e('Run Import', 'yoke'); ?></button>
											 				</th>
										 				</tr>
										 			</thead>
										 			<tbody>
										 				<tr>
										 					<td><?php _e('Last import began:', 'yoke') ?></td>
										 					<td><?php echo $started_str; ?></td>
										 				</tr>
										 				<tr>
										 					<td><?php _e('Last import finished:', 'yoke') ?></td>
										 					<td><?php echo $completed_str; ?></td>
										 				</tr>
										 				<tr>
										 					<td><?php _e('Last successful import began:', 'yoke') ?></td>
										 					<td><?php echo $success_started_str; ?></td>
										 				</tr>
										 				<tr>
										 					<td><?php _e('Last successful import finished:', 'yoke') ?></td>
										 					<td><?php echo $success_completed_str; ?></td>
										 				</tr>
										 				<tr>
										 					<td><?php _e('Last import completed in:', 'yoke') ?></td>
										 					<td><?php echo $duration_str; ?></td>
										 				</tr>
										 				<tr>
										 					<td><?php _e('Last import successful:', 'yoke') ?></td>
										 					<td><?php echo ($success ? __('Yes', 'yoke') :  __('No', 'yoke')); ?></td>
										 				</tr>
										 			</tbody>
										 			<tfoot>
										 				<tr>
										 					<td></td>
										 					<td>
																<?php if($import->has_user_settings): ?>
																	<a href="<?php echo $import->get_settings_page_link(); ?>" class="button button-primary alignright"><?php _e('Manage Options', 'yoke'); ?></button>
																<?php endif; ?>
															</td>
										 				</tr>
										 			</tfoot>
										 		</table>

											</div>
										<?php endforeach; ?>

					 				<?php endif; ?>
								</form>
    						<?php endif; ?>
						</div><!-- meta-box -->

					</div><!-- #postbox-container-1 -->

					<div id="postbox-container-2" class="postbox-container">

						<?php //do_meta_boxes( $hook_suffix, 'normal', null ); ?>
						<!-- #normal-sortables -->

						<?php //do_meta_boxes( $hook_suffix, 'advanced', null ); ?>
						<!-- #advanced-sortables -->

					</div><!-- #postbox-container-2 -->
				</div><!-- #post-body -->
			</div><!-- #poststuff -->
				
			<br class="clear">

	</div>
</div>

<style type="text/css" media="screen">
	table td, table th {
		width: 50%;
	}	
</style>

