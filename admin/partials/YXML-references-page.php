<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/admin/partials
 */

?>

<div id="yxml-admin-wrap" class="wrap">
 	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

    <?php settings_errors( 'yoke_messages' ); ?>
 	
 	<?php 

 		$page_url = admin_url( 'admin.php?page=yxml-references' );

 		$is_edit_page = false;
 		
 		$references_type_args = array();
 		
 		if(isset($_GET['refid'])):
 			$references_type_args['id'] = $_GET['refid'];
 			$is_edit_page = true;
 			$tax_options_fields = YXML()->references->tax_options_fields(); 
 		endif;

 		$reference_types = YXML()->references->get_reference_types($references_type_args); 
		// $registered_post_types = get_post_types();
 	?>


	<div class="yxml-settings-meta-box-wrap">
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>">
				<div class="postbox-container">
					<div class="postbox">
        				<!-- <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Toggle panel: Activity</span><span class="toggle-indicator" aria-hidden="true"></span></button> -->
        				<!-- <h2 class="hndle ui-sortable-handle"><span><?php _e( 'Reference Type Import Options', 'yoke' ); ?></span></h2> -->
						
						<?php if(!empty($reference_types)): ?>

        					<h2 class="hndle ui-sortable-handle"><span><?php _e( 'Edit Reference Type Import Options', 'yoke' ); ?></span></h2>
							 			
							
							<?php if($is_edit_page): ?>
		        				
								
								<?php foreach($reference_types as $ref_type): ?>

									<form id="yxml-reference-types-form" class="" method="post" action="options.php">

				  						<?php wp_nonce_field(); ?>

										<input type="hidden" name="action" value="yxml_update_reference_types_options">
										<input type="hidden" name="_redirect" value="<?php echo $page_url; ?>">


										<table class="widefat striped">
								 			<thead>
								 				<tr>

									 				<th>
		        										<a href="<?php echo $page_url; ?>" class="button"><?php _e('Back', 'roots'); ?></a>									 					
									 				</th>
									 				<th>
		        										<button type="submit" class="button button-primary alignright"><?php _e('Save Options', 'roots'); ?></button>
									 				</th>
								 				</tr>
								 			</thead>
								 			<tbody>
									 			<?php 

									 			$is_product_reference = $ref_type->product_reference;

												$post_types_placeholder = ($is_product_reference ? 'product' : '');
												// if($is_product_reference) $ref_type->post_types = 'product';

									 			$taxonomy = $ref_type->taxonomy;
									 			$taxonomy_placeholder = apply_filters( 'yxml_create_taxonomy_name', $ref_type->code ); 
									 			$is_tax = ($ref_type->is_tax || (!$ref_type->taxonomy && taxonomy_exists( $taxonomy_placeholder )));
									 			$var_name_prefix = 'reference_types[' . $ref_type->id . ']';
									 			?>
								 				<tr>
								 					<td><strong><?php _e('ID', 'yoke'); ?></strong></td>
								 					<td>
								 						<?php echo $ref_type->id; ?>
								 					</td>
								 				</tr>

								 				<tr>
								 					<td><strong><?php _e('Name', 'yoke'); ?></strong></td>
								 					<td>
									 					<?php echo $ref_type->name; ?>
									 					<input type="hidden" name="<?php echo $var_name_prefix; ?>[name]" value="<?php echo $ref_type->name; ?>">
								 					</td>
								 				</tr>

								 				<tr>
								 					<td><strong><?php _e('Code', 'yoke'); ?></strong></td>
								 					<td>
									 					<?php echo $ref_type->code; ?>
									 					<input type="hidden" name="<?php echo $var_name_prefix; ?>[code]" value="<?php echo $ref_type->code; ?>">
								 					</td>
								 				</tr>

								 				<tr>
								 					<td><strong><?php _e('Is Tax', 'yoke'); ?></strong></td>
								 					<td>
								 						<?php yxml_field($var_name_prefix . '[is_tax]', 'bool', array('value' => $is_tax)); ?>
								 					</td>
								 				</tr>

								 				<tr>
								 					<td><strong><?php _e('Taxonomy', 'yoke'); ?></strong></td>
								 					<td>
								 						<?php yxml_field($var_name_prefix . '[taxonomy]', 'text', array('value' => $ref_type->taxonomy, 'placeholder' => $taxonomy_placeholder)); ?>
								 					</td>
								 				</tr>

								 				<tr>
								 					<td><strong><?php _e('Post Type(s)', 'yoke'); ?></strong></td>
								 					<td>
								 						<?php yxml_field($var_name_prefix . '[post_types]', 'post_types', array('value' => $ref_type->post_types)); ?>
								 					</td>
								 				</tr>
								 				<?php if(!empty($tax_options_fields)): ?>
									 				<?php foreach($tax_options_fields as $option_key => $option_settings): ?>
										 				<tr>
										 					<td><code><?php echo $option_key; ?></code></td>
										 					<td>
										 						<?php 
										 						$field_name = $var_name_prefix . '[tax_options][' . $option_key . ']';
										 						
										 						$option_value = (isset($ref_type->tax_options[$option_key]) ? $ref_type->tax_options[$option_key] : (isset($option_settings['default']) ? $option_settings['default'] : null));
																
								 								yxml_field( $field_name, $option_settings['type'], array('value' => $option_value));

										 						?>
											 				</td>
											 			</tr>
									 				<?php endforeach; ?>
								 				<?php endif; ?>
							 				</tbody>
							 			</table>
									</form>
								<?php endforeach; ?>	

							<?php else: ?>
		        				<table class="widefat striped">
						 			<thead>
						 				<tr>
							 				<th><?php _e('ID', 'yoke'); ?></th>
							 				<th><?php _e('Name', 'yoke'); ?></th>
							 				<th><?php _e('Code', 'yoke'); ?></th>
							 				<th><?php _e('Is Tax', 'yoke'); ?></th>
							 				<th><?php _e('Taxonomy', 'yoke'); ?></th>
							 				<th><?php _e('Post Type(s)', 'yoke'); ?></th>
							 				<th><?php _e('Options', 'yoke'); ?></th>
						 				</tr>
						 			</thead>
						 			<tbody>
								 	<?php foreach($reference_types as $ref_type): ?>
							 			<?php 

							 			$ref_type_edit_link = $page_url . '&refid=' . $ref_type->id;

							 			$is_product_reference = $ref_type->product_reference;

										$post_types_placeholder = ($is_product_reference ? 'product' : '');
										// if($is_product_reference) $ref_type->post_types = 'product';

							 			$taxonomy = $ref_type->taxonomy;
							 			$taxonomy_placeholder = apply_filters( 'yxml_create_taxonomy_name', $ref_type->code ); 
							 			$is_tax = ($ref_type->is_tax || (!$ref_type->taxonomy && taxonomy_exists( $taxonomy_placeholder )));
							 			$var_name_prefix = 'reference_types[' . $ref_type->id . ']';
							 			?>
							 			<tr>
							 				<th data-col="<?php _e('ID', 'yoke'); ?>">
							 					<?php echo $ref_type->id; ?>
							 				</th>

							 				<th data-col="<?php _e('Name', 'yoke'); ?>">
							 					<?php echo $ref_type->name; ?>
							 				</th>

							 				<th data-col="<?php _e('Code', 'yoke'); ?>">
							 					<?php echo $ref_type->code; ?>
							 				</th>

							 				<th data-col="<?php _e('Is Tax', 'yoke'); ?>">
							 					<?php if( $is_tax ): ?><i class="dashicons-before dashicons-yes"></i><?php endif; ?>
							 				</th>

							 				<th data-col="<?php _e('Taxonomy', 'yoke'); ?>">
								 				<?php //yxml_field( $field_name, $option_settings['type'], array('value' => $option_value)); ?>

							 					<input type="text" readonly name="<?php echo $var_name_prefix; ?>[taxonomy]" placeholder="<?php echo $taxonomy_placeholder; ?>" value="<?php echo $ref_type->taxonomy; ?>">
							 				</th>

							 				<th data-col="<?php _e('Post Type(s)', 'yoke'); ?>">
							 					<input type="text" readonly name="<?php echo $var_name_prefix; ?>[post_types]" placeholder="" value="<?php echo $ref_type->post_types; ?>">
							 				</th>

							 				<th data-col="<?php _e('Options', 'yoke'); ?>">
							 					<a class="button" href="<?php echo $ref_type_edit_link; ?>"><?php _e('Edit', 'yoke'); ?></a>
							 				</th>								 			
							 			</tr>

									<?php endforeach; ?>	
					 				</tbody>
					 			</table>
							<?php endif; ?>
						
						<?php endif; ?>
						
					</div>
				</div>

				<br class="clear">

			</div>		

		</div>
	</div>
</div>
