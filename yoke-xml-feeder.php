<?php

/**
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           YXML
 *
 * @wordpress-plugin
 * Plugin Name:       Yoke XML Feeder
 * Plugin URI:        http://yokedesign.com.au
 * Description:       Integrate an XML API
 * Version:           1.0.0
 * Author:            Yoke
 * Author URI:        http://yokedesign.com.au
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       yoke
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-YXML-activator.php
 */

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'class-YXML.php';
/*

function yxml_activate() {

	add_action('yxml_before_init', function(){ 
		do_action('yxml_activation');
	});
	// p(_get_cron_array());

	// die(__FUNCTION__);
}

function yxml_deactivate() {
	do_action('yxml_deactivation');
	// p(_get_cron_array());

	// die(__FUNCTION__);
}*/
register_activation_hook(__FILE__, array('YXML_Activator', 'activate'));
register_activation_hook(__FILE__, function(){
		// p(_get_cron_array());

	// die();
});
register_deactivation_hook(__FILE__, array('YXML_Deactivator', 'deactivate'));
// register_deactivation_hook(__FILE__, 'yxml_deactivate');