<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    YXML
 * @subpackage YXML/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    YXML
 * @subpackage YXML/includes
 */

final class YXML {


	/**
	 * The single instance of the class.
	 *
	 * @var YXML
	 * @since 1.0.0
	 */
	protected static $_instance = null;

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      YXML_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The API Connect
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      YXML_Connect	$con 
	 */
	public $con;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @var      string
	 */
	protected $version = '1.0.0';


	/**
	 * Main Instance.
	 *
	 * Ensures only one instance of the plugin is loaded or can be loaded.
	 *
	 * @since 2.1
	 * @static
	 * @see YXML()
	 * @return YXML - Main instance.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 * @since 1.0.0
	 */
	public function __clone() {
		wp_die( __( 'That&#8217s not gonna happen', 'yoke' ));
	}	

	/**
	 * Unserializing instances of this class is forbidden.
	 * @since 2.1
	 */
	public function __wakeup() {
		wp_die( __( 'Cheatin&#8217; huh?', 'yoke' ));
	}

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	public function __construct() {

		if(did_action( 'yxml_loaded' )) return;

		$this->define_constants();
		$this->load_dependencies();
		$this->init_hooks();

		do_action( 'yxml_loaded');

		return $this;

	}

	private function init_hooks() {

		register_activation_hook( __FILE__, array($this, 'activate_YXML') );
		register_deactivation_hook( __FILE__, array($this, 'deactivate_YXML') );
		
		add_action( 'init', array($this, 'init'), 1);

		$this->define_admin_hooks();
		$this->define_public_hooks();

		// add_action( 'init', array( $this, 'init' ), 0 );

	}

	private function pre_init() {}

	public function init() {
		
		$this->log 					= new YXML_Log( );
		$this->permissions			= new YXML_Permissions( );
		$this->notices				= new YXML_Notices( );
		$this->shows				= new YXML_Shows( );
		// $this->persons				= new YXML_Persons( );
		// $this->cart					= new YXML_Cart( );
		// $this->orders				= new YXML_Orders( );
		// $this->shipping				= new YXML_Shipping( );
		$this->actions 				= new YXML_Actions( );
		$this->post_types			= new YXML_Post_Types( );
		// $this->attributes			= new YXML_Attributes( );
		// $this->references			= new YXML_References( );
		$this->imports				= new YXML_Imports( );
		$this->import_instances		= new YXML_Import_Instances( );
		// $this->products				= new YXML_Products( );
		// $this->stores				= new YXML_Stores( );
		$this->tax					= new YXML_Taxonomies( );
		// $this->image_import			= new YXML_Image_Import( );		
		$this->admin_cols			= new YXML_Admin_Columns( );
		
		$this->db					= new YXML_DB_Manager( );

		$this->cron					= new YXML_Cron( );

		// $this->pre_init();

		do_action( 'yxml_before_init');

		$this->set_locale();

		// $this->importer			= new YXML_Importer( );
		$this->con 				= new YXML_Connect( );

		do_action( 'yxml_init');

	}


	private function define_constants() {

		$wp_uploads_dir = wp_upload_dir();
		
		$this->define( 'YXML_PLUGIN_NAME', 				'YXML' );
		$this->define( 'YXML_PLUGIN_TITLE', 			'Yoke XML Feeder' );

		$this->define( 'YXML_PLUGIN_FILE',				__FILE__ );
		$this->define( 'YXML_ABSPATH', 					dirname( __FILE__ ) . DIRECTORY_SEPARATOR );
		$this->define( 'YXML_SESSION_PREFIX',			strtolower(YXML_PLUGIN_NAME) . '_');
		$this->define( 'YXML_DB_PREFIX', 				strtolower(YXML_PLUGIN_NAME) . '_');
		$this->define( 'YXML_OPTION_PREFIX', 			strtolower(YXML_PLUGIN_NAME) . '_' );
		$this->define( 'YXML_UPLOAD_DIR',	 			strtolower($wp_uploads_dir['basedir']));
		$this->define( 'YXML_LOCAL_DATA_PATH', 			strtolower(YXML_UPLOAD_DIR) . DIRECTORY_SEPARATOR . 'yoke-xml-feeder' . DIRECTORY_SEPARATOR . 'local-data' . DIRECTORY_SEPARATOR );

		$this->define( 'YXML_IMPORT_DATA_PATH',			apply_filters( 'yxml_import_data_dir', $wp_uploads_dir['basedir'] . DIRECTORY_SEPARATOR . strtolower(YXML_PLUGIN_NAME) . '_import_data' . DIRECTORY_SEPARATOR ) );
		$this->define( 'YXML_LOGS_PATH',				$wp_uploads_dir['basedir'] . DIRECTORY_SEPARATOR . strtolower(YXML_PLUGIN_NAME) . '_logs' . DIRECTORY_SEPARATOR );

		$this->define( 'YXML_USE_LOCAL_DATA', 			$this->get_option('api_settings', 'use_local_data' ) );
		$this->define( 'YXML_SAVE_LOCAL_DATA', 			$this->get_option('api_settings', 'save_local_data' ) );

		$this->define( 'YXML_SHOW_IMPORT_XML_ERRORS', 	$this->get_option('api_settings', 'show_import_errors' ) );
	}


	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		} else {
		}
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - YXML_Loader. Orchestrates the hooks of the plugin.
	 * - YXML_i18n. Defines internationalization functionality.
	 * - YXML_Admin. Defines all hooks for the admin area.
	 * - YXML_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {


		require_once( YXML_ABSPATH . 'includes/class-YXML-log.php');
		
		require_once( YXML_ABSPATH . 'includes/class-YXML-activator.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-deactivator.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-data.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-post-factory.php');
		
		require_once( YXML_ABSPATH . 'includes/class-YXML-notices.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-db-manager.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-permissions.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-response.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-actions.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-cron.php');
		// require_once( YXML_ABSPATH . 'includes/class-YXML-config.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-i18n.php');

		/**
		 * The class responsible for defining all actions that occur in the admin/public areas.
		 */
		require_once( YXML_ABSPATH . 'admin/class-YXML-admin.php');
		require_once( YXML_ABSPATH . 'public/class-YXML-public.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-admin-columns.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-field.php');

		/**
		 * The class responsible for connections to the API
		 */

		require_once( YXML_ABSPATH . 'includes/class-YXML-connect.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-api-call.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-api-putload.php');
		
		// require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-post-handler.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-post-types.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-post.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-taxonomies.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-shows.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-show.php');

		require_once( YXML_ABSPATH . 'includes/class-YXML-imports.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-import.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-import-instances.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-import-instance.php');
		require_once( YXML_ABSPATH . 'includes/class-YXML-abstract-importer.php');
		
		require_once( YXML_ABSPATH . 'includes/importers/class-YXML-importer-shows.php');

		require_once( YXML_ABSPATH . 'includes/yxml-functions.php');

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the YXML_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new YXML_i18n();

		add_action( 'plugins_loaded', array($plugin_i18n, 'load_plugin_textdomain' ));

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$this->admin = new YXML_Admin( $this->get_plugin_name(), $this->get_version() );

		add_action( 'admin_enqueue_scripts', array($this->admin, 'enqueue_styles' ));
		add_action( 'admin_enqueue_scripts', array($this->admin, 'enqueue_scripts' ));

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$this->public = new YXML_Public( $this->get_plugin_name(), $this->get_version() );

		add_action( 'wp_enqueue_scripts', array($this->public, 'enqueue_styles' ));
		add_action( 'wp_enqueue_scripts', array($this->public, 'enqueue_scripts' ));

	}

    public function get_option($option_name, $field = null) {

        $option = get_option( YXML_OPTION_PREFIX . $option_name);        

        if(!$field):
        	return $option;
        elseif(isset($option[$field])):
        	return $option[$field];
        endif;

        return;
    }

    public function set_option($option, $value) {

    	$as_array = is_array($option);

    	$save_value = $value;

    	if($as_array):
    		reset($option);
			$option_name = key($option);
			$field = $option[$option_name];

    		$original_value = get_option( YXML_OPTION_PREFIX . $option_name);

    		if(!is_array($original_value)) $original_value = array();

    		$save_value = $original_value;
			$save_value[$field] = $value;

    	else:
			$option_name = $option;
    	endif;

		// var_dump( YXML_OPTION_PREFIX . $option_name, $save_value );
		// die();
		update_option( YXML_OPTION_PREFIX . $option_name, $save_value );

        return;
    }

    public function get_api_meta_prefix() {
    	return apply_filters('yxml_api_meta_prefix', YXML_DB_PREFIX);
    }

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return YXML_PLUGIN_NAME;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}


	/**
	 * deactivate plugin.
	 * @since     1.0.0
	 * This action is documented in includes/class-YXML-activator.php
	 */
	public function activate_YXML() {
		require_once( YXML_ABSPATH . 'includes/class-YXML-activator.php');
		YXML_Activator::activate();
	}

	/**
	 * deactivate plugin.
	 * @since     1.0.0
	 * This action is documented in includes/class-YXML-deactivator.php
	 */
	public function deactivate_YXML() {
		require_once( YXML_ABSPATH . 'includes/class-YXML-deactivator.php');
		YXML_Deactivator::deactivate();
	}
}

function YXML() {
	return YXML::instance();
}



/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
$GLOBALS['YXML'] = YXML();


// add_action( 'yxml_init', 'YXML_tests');
// add_action( 'yxml_after_admin_options_form', 'YXML_tests');

function YXML_tests() {
	require_once( YXML_ABSPATH . 'init-test.php');
}
